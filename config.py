import os
import redis
from datetime import timedelta

basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):

    SERVER_NAME = 'api.doctrin:3000'

    ROOT = os.path.join(basedir, 'api')
    UPLOAD_FOLDER = os.path.join(ROOT, 'static/uploads')
    TMP_FOLDER = os.path.join(UPLOAD_FOLDER, 'tmp')

    #Flask
    DEBUG = False
    TESTING = False
    SQLALCHEMY_ECHO = False
    FLASK_COVERAGE = True


    ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'pdf', 'doc', 'docx', 'xls', 'xlsx'])
    MAX_CONTENT_LENGTH = 20 * 1024 * 1024

    ERROR_404_HELP = False

    # BLEACH
    BLEACH_ALLOWED_TAGS = set(['a', 'b', 'em', 'i', 'li', 'ol', 'strong', 'ul', 'h1', 'h2', 'h3', 'h4', 'h5', 'p'])
    BLEACH_ALLOWED_ATTRIBUTES = ['href', 'title', 'style', 'src']
    BLEACH_ALLOWED_STYLES = ['font-family', 'font-weight']
    BLEACH_STRIP_TAGS = True
    
    SECRET_KEY = 'O\x0f\xc9\xfak[\x99#m\x17\x18\xac2\x00`8\xa7\x01\xa1/\xdf>~\xeadsu\xa3\xf6@\xb8\xbb\xd5-\xf2(\\\xfe\x14\r8\xa8\x7fh\xc6\x93\xc8\x87\r\xc6bc\xb1\xb7\xcea\xf1\x95\xa2b\xb0c\xc9\xf5\xdc%\x04\x9f\x9f\xbad\x82+\xca\x19\xc5\xcdY\x04\x87\x15\x194\xf8:|4\xea\xaf\xda\xe2N\x8aV\xcf!Z{6f\xdcJ\x16\xc9\xd9Ft\xfeP\xb0\x10\x8dR\xd5^\x03\xa5\x10z\xc2=\x9b\xb2\xc5*n\xa9\x0b'

    #CACHE
    CACHE_TYPE = 'redis'
    CACHE_DEFAULT_TIMEOUT = 60
    CACHE_REDIS_URL = 'redis://:root@localhost:6379/0'
    CACHE_KEY_PREFIX = 'doctrin_cache'

    # RATE LIMIT
    RATELIMIT_HEADERS_ENABLED = True
    RATELIMIT_STORAGE_URL = 'redis://:root@localhost:6379/0'
    RATELIMIT_KEY_PREFIX = 'doctrin_ratelimit_'

    # CELERY
    CELERY_BROKER_URL = 'redis://:root@localhost:6379/1'
    CELERY_RESULT_BACKEND = 'redis://:root@localhost:6379/1'
    CELERY_ACCEPT_CONTENT = ['json', 'pickle']

    # RESIZE MEDIA
    IMAGES_PATH = ['static']
    IMAGES_CACHE = os.path.join(ROOT, 'static/cache')

    # MAIL
    ADMIN_EMAIL = 'robot@missana.id'
    MAIL_SERVER = 'smtp.webfaction.com'
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USE_SSL = False
    MAIL_USERNAME = 'missana_robot'
    MAIL_PASSWORD = 'Pr0m0th3u51928'
    MAIL_DEFAULT_SENDER = 'robot@missana.id'
    MAIL_ASCII_ATTACHMENTS = True
    MAIL_DEBUG = True

    # NEXMO
    NEXMO_API_KEY = 'b824ced9'
    NEXMO_SECRET_KEY = '5e560391a01394e0'

    # JWT
    JWT_TOKEN_LOCATION = ['headers']
    JWT_BLACKLIST_ENABLED = True
    JWT_BLACKLIST_TOKEN_CHECKS = ['access', 'refresh']
    JWT_ACCESS_TOKEN_EXPIRES = timedelta(days=365)
    JWT_REFRESH_TOKEN_EXPIRES = timedelta(days=365)

    CORS_ENABLED = True
    CORS_ALLOW_HEADERS = ['Origin', 'Accept', 'Content-Type', 'Authorization', 'Content-Length', 'X-Requested-With']
    CORS_METHODS = ['GET', 'POST', 'DELETE', 'PUT', 'OPTIONS']
    CORS_SUPPORTS_CREDENTIALS = True

    # ONE SIGNAL
    ONESIGNAL_APPID = 'da733a06-23ca-4554-aaac-a032e9eb35ca'
    ONESIGNAL_APIKEY = 'ZWJjMDkwMzAtODYzMC00NmM3LTg0Y2UtNzU1YTlkMzM4MTk2'
    ONESIGNAL_APIURL = 'https://onesignal.com/api/v1/notifications'
    ONESIGNAL_CHANNELID = '598b79c9-6748-4cf9-99c2-4e5fd94f26d9'


    # WEBDAV
    MEDIA_STORAGE = 'webdav'
    WEBDAV_HOST = 'drivex.pusri.co.id'
    WEBDAV_PROTOCOL = 'https'
    WEBDAV_PATH = '/remote.php/webdav/'
    WEBDAV_PORT = 443
    WEBDAV_USERNAME = 'app.drivex'
    WEBDAV_PASSWORD = 'XmNE6-xHbjW-fWjjy-Kk9bi-HEEEr'


    # CITCALL
    CITCALL_URL = 'https://gateway.citcall.com/v3/call'
    CITCALL_USERNAME = 'pusri'
    CITCALL_APIKEY = 'QWxhZGRpbjo5YWY0ZDgzODE3ODFiYWNjYjBmOTE1ZTU1NGY4Nzk4ZA=='


    @staticmethod
    def init_app(app):
        pass


class ProductionConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'mysql://pmauser:password_here@localhost:3006/db_doctrin'


class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SQLALCHEMY_DATABASE_URI = 'mysql://pmauser:password_here@localhost:3006/db_doctrin'

class TestingConfig(Config):
    TESTING = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SQLALCHEMY_DATABASE_URI = 'mysql://pmauser:password_here@localhost:3006/db_doctrin_test'


config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig,
    'testing': TestingConfig,
    'default': DevelopmentConfig
}
