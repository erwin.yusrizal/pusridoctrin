import nexmo
import random

from flask import current_app, render_template
from flask_mail import Message

from api import mail, celery

from api.utils import (
    normalize_phone_number
)

def exponential_backoff(task_self):
    minutes = task_self.default_retry_delay / 60
    rand = random.uniform(minutes, minutes * 1.3)
    return int(rand ** task_self.request.retries) * 60


'''
Send Celery Email Task Handler
'''

@celery.task(name='doctrin.api.tasks.doctrin_email_queue', bind=True, max_retries=5, acks_late=True, default_retry_delay=10)
def doctrin_email_queue(self, *args, **kwargs):

    message = args if args else kwargs

    if type(message) == tuple:
        message = message[0]

    email = Message(message["subject"], sender=("Micro.id", "robot@missana.id"), recipients=[message["to"]])
    email.body = render_template(message["template"] + '.txt', message=message["variables"])
    email.html = render_template(message["template"] + '.html', message=message["variables"])

    if 'attachment' in message['variables']:
        if message['variables']['attachment']:
            email.attach(message['variables']['attachment'], 'application/octect-stream', open(message['variables']['attachment'], 'rb').read())

    try:
        mail.send(email)
    except Exception as e:
        self.retry(exc=e, countdown=exponential_backoff(self))



'''
Send SMS Task Handler
'''

@celery.task(name='doctrin.api.tasks.doctrin_sms_queue', bind=True, max_retries=5, acks_late=True, default_retry_delay=10)
def doctrin_sms_queue(self, *args, **kwargs):

    message = args if args else kwargs

    if type(message) == tuple:
        message = message[0]
    
    
    phone = normalize_phone_number(message["phonenumber"]) if message["phonenumber"] else None

    if phone:
        try:
            headers = {
                "Content-Type": "application/json",
                "Authorization": current_app.config.get('CITCALL_APIKEY')
            }
            
            payload = {
                "senderid": "citcall",
                "msisdn": phone,
                "text": message["content"]
            }

            req = requests.post(current_app.config.get('CITCALL_URL'), headers=headers, data=json.dumps(payload))

            return {
                'status': req.status_code,
                'reason': req.reason
            }
        except Exception as e:
            self.retry(exc=e, countdown=exponential_backoff(self))



@celery.task(name="doctrin.api.tasks.doctrin_delete_orphan_images")
def doctrin_delete_orphan_images():
    print('Hello guys....')


