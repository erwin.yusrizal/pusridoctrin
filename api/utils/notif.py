import json
import requests
from flask import current_app


class OneSignal(object):

    def __init__(self):

        self.APPKEY = current_app.config.get('ONESIGNAL_APIKEY')
        self.APPID = current_app.config.get('ONESIGNAL_APPID')
        self.URL = current_app.config.get('ONESIGNAL_APIURL')
        self.CHANNELID = current_app.config.get('ONESIGNAL_CHANNELID')

        self.header = {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": "Basic " + self.APPKEY
        }


    def send(self, title, body, player_ids=None, segments=None):

        payload = {
            "app_id": self.APPID,
            "headings": {
                "en": title
            },
            "content": {
                "en": body
            },
            "android_channel_id": self.CHANNELID,
            "small_icon": "icon",
            "large_icon": "icon",
            "android_sound": "doctrin_notif"
        }

        if player_ids:
            payload['include_player_ids'] = player_ids

        elif segments:
            payload['included_segments'] = segments


        req = requests.post(self.URL, headers=self.header, data=json.dumps(payload))

        return {
            'status': req.status_code,
            'reason': req.reason
        }

