import json
import requests
import random
import base64

from api.utils import (
    normalize_phone_number
)

class Miscall(object):

    def __init__(self):

        self.CITCALL_URL = 'https://gateway.citcall.com/v3/call'
        self.CITCALL_USERNAME = 'pusri'
        self.CITCALL_APIKEY = '4b90546814329b04a7cb4b22e0ab7baf'

        auth_string = self.CITCALL_USERNAME+':'+self.CITCALL_APIKEY.encode('ascii')

        print(auth_string)

        self.headers = {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": base64.b64encode(auth_string)
        }


    def send(self, phone):

        phone = normalize_phone_number(phone)

        payload = {
            "msisdn": phone,
            "gateway": random.randint(0,4)
        }
        
        print(self.headers, payload)

        req = requests.post(self.CITCALL_URL, headers=self.headers, data=json.dumps(payload))
        print(req.reason)
        return req
