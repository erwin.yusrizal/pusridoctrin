import arrow
import json
import random
import string
import base64
import re
import os

from flask import current_app, request

from math import ceil
from wheezy.core.feistel import make_feistel_number
from wheezy.core.feistel import sample_f

feistel_number = make_feistel_number(sample_f)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in current_app.config.get('ALLOWED_EXTENSIONS')

def get_clients_ip():
    headers_list = request.headers.getlist("X-Forwarded-For")
    user_ip = headers_list[0] if headers_list else request.remote_addr
    return user_ip

def generate_sms_token(phone='', n=6):
    return ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits + phone) for _ in range(n))

def generate_unique_id(n=16):
    return ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits + str(arrow.utcnow().timestamp)) for _ in range(n))


def parse_verification_token(token):
    try:
        payload = json.loads(base64.urlsafe_b64decode(str(token)))
        return payload
    except ValueError:
        return None

def normalize_phone_number(phone_number):

    phone = phone_number

    if phone_number.startswith('08'):
        phone = phone_number.replace(phone_number[:2], '628', 1)
    elif phone_number.startswith('+628'):
        phone = phone_number.replace(phone_number[:4], '628', 1)
    
    return phone


def denormalize_phone_number(phone_number):

    phone = phone_number
    
    if phone_number.startswith('628'):
        phone = phone_number.replace(phone_number[:3], '08', 1)
    elif phone_number.startswith('+628'):
        phone = phone_number.replace(phone_number[:4], '08', 1)       
    
    return phone


def is_file_exist(path):
    return os.path.exists(path)


def is_dir_exist(file):
    if not os.path.exists(os.path.dirname(file)):
        try:
            os.makedirs(os.path.dirname(file))
        except OSError as exc:
            if exc.errno != errno.EEXIST:
                raise


def reverse_id(id):
    return feistel_number(id)


def valid_url(url):

    regex = re.compile(
        r'^(?:http|ftp)s?://' # http:// or https://
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|' #domain...
        r'localhost|' #localhost...
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})' # ...or ip
        r'(?::\d+)?' # optional port
        r'(?:/?|[/?]\S+)$', re.IGNORECASE)

    return re.match(regex, url)

