from flask import current_app

def add_index(index, model):

    if not current_app.elasticsearch:
        return
    
    payload = {}
    
    for field in model.__searchable__:
        payload[field] = getattr(model, field)

    current_app.elasticsearch.index(index=index, id=model.id, body=payload)


def update_index(index, model):

    if not current_app.elasticsearch:
        return

    payload = {}

    for field in model.__searchable__:
        payload[field] = getattr(model, field)

    current_app.elasticsearch.update(index=index, id=model.id, body=payload)


def delete_index(index, model):

    if not current_app.elasticsearch:
        return

    current_app.elasticsearch.delete(index=index, id=model.id)


def query_index(index, query, page, perpage):

    if not current_app.elasticsearch:
        return [], 0

    search = current_app.elasticsearch.search(
        index=index, 
        body={
            'query': {
                'bool': {
                    'should': [
                        {
                            'wildcard': {
                                "tracking_number": "*"+query+"*"
                            }
                        },
                        {
                            'wildcard': {
                                "document_number": "*"+query+"*"
                            }
                        },
                        {
                            'wildcard': {
                                "title": "*"+query+"*"
                            }
                        },
                        {
                            'wildcard': {
                                "remarks": "*"+query+"*"
                            }
                        }

                    ]
                }
            },
            'from': (page - 1) * perpage, 
            'size': perpage
        }
    )
    
    ids = [int(hit['_id']) for hit in search['hits']['hits']]
    return ids, search['hits']['total']['value']
