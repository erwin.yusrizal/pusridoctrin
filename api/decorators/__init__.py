import datetime
import time

from functools import update_wrapper, wraps
from flask import request, jsonify, g
from flask_jwt_extended import get_jwt_identity
from flask_restful import abort

from api.v1.models.users import UserModel
from api.utils import reverse_id

def anonymous_required():
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            if 'Authorization' in request.headers:
                return jsonify({
                    'success': False,
                    'errors': {
                        'code': 403,
                        'messages': {
                            'anonymous_required': ['You are already logged in. You need to logout to continue.']
                        }
                    }
                }), 403
            return f(*args, **kwargs)
        return decorated_function
    return decorator


def current_user():
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            id = reverse_id(get_jwt_identity())
            user = UserModel.query.get(id)
            if user is None:
                return jsonify({
                    'success': False,
                    'errors': {
                        'code': 401,
                        'messages': {
                            'authorization': [ 'You are not authorized to access this resource.']
                        }
                    }
                }), 401
            g.current_user = user
            return f(* args, **kwargs)
        return decorated_function
    return decorator


def permission_required(permission, action):
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            id = reverse_id(get_jwt_identity())
            user = UserModel.query.get(id)
            if user and not user.check_permission(permission, action):
                return jsonify({
                    'success': False,
                    'errors': {
                        'code': 403,
                        'messages': {
                            'permission_required': ['You don\'t have the permission to access the requested resource. It is either read-protected or not readable by the server']
                        }
                    }
                }), 403
            return f(*args, **kwargs)
        return decorated_function
    return decorator
