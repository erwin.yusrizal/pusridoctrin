import datetime

from collections import OrderedDict

from sqlalchemy import asc, desc, exc
from sqlalchemy.sql.expression import and_, func, or_
from flask_sqlalchemy_caching import FromCache, RelationshipCache

from api import bcrypt, cache, db
from api.v1.models import SearchableMixin
from api.v1.models.histories import DoctrinHistory, HistoryModel
from api.utils import (
    get_clients_ip, 
    generate_sms_token, 
    reverse_id, 
    normalize_phone_number,
    generate_unique_id
)


documents_person_involves = db.Table('doctrin_documents_person_involves', 
    db.Column('document_id', db.Integer, db.ForeignKey('doctrin_documents.id', ondelete='cascade'), primary_key=True),
    db.Column('person_involve_id', db.Integer, db.ForeignKey('doctrin_person_involved.id', ondelete='cascade'), primary_key=True)
)


class CustomFieldGroupModel(db.Model):

    __tablename__ = 'doctrin_fields_groups'


    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    name = db.Column(db.String(100), nullable=False)
    slug = db.Column(db.String(100), nullable=False)
    description = db.Column(db.String(255), nullable=True)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    organization_id = db.Column(db.Integer, db.ForeignKey('doctrin_organization.id'), index=True, nullable=True)
    fields = db.relationship('CustomFieldModel', backref='field_group', lazy='dynamic', cascade='all, delete-orphan')


    def __init__(self, **kwargs):
        super(CustomFieldGroupModel, self).__init__(**kwargs)


    def __repr__(self):
        return '<CustomFieldGroup %r, %r>' % (self.id, self.name)


    @classmethod
    def count_all(cls, q=None, organization=None):

        query = cls.query

        if q:
            query = query.filter(cls.name.like('%'+q+'%'))

        if organization:
            query = query.filter(cls.organization_id==organization)


        cached_query = query.options(FromCache(cache))
        return cached_query.count()


    @classmethod
    def get_all(cls, q=None, organization=None, page=1, perpage=25):

        offset = (page - 1) * perpage

        query = cls.query

        if q:
            query = query.filter(cls.name.like('%'+q+'%'))

        if organization:
            query = query.filter(cls.organization_id==organization)


        query = query.order_by(cls.id.asc()).limit(perpage).offset(offset)

        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_by(cls, id):
        
        query = cls.query
        cached_query = query.options(FromCache(cache))
        return cached_query.get(id)

        
class CustomFieldModel(db.Model):

    
    FIELDS = OrderedDict([('text', 'Text'), ('email', 'Email'), ('number', 'Number'), ('textarea', 'Textarea'), ('select', 'Select'), ('checkbox', 'Checkbox'), ('radio', 'radio'), ('switch', 'Switch'), ('file', 'File'), ('datepicker', 'Date Picker'), ('timepicker', 'Time Picker'), ('datetimepicker', 'Date Time Picker')])
    
    
    __tablename__ = 'doctrin_fields'


    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    name = db.Column(db.String(100), nullable=False)
    slug = db.Column(db.String(100), nullable=False)
    description = db.Column(db.String(255), nullable=True)
    field = db.Column(db.Enum(*FIELDS, name="custom_fields"), index=True)
    default = db.Column(db.Text, nullable=True)
    value = db.Column(db.Text, nullable=True)
    options = db.Column(db.Text, nullable=True)
    required = db.Column(db.Boolean, default=False)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    group_id = db.Column(db.Integer, db.ForeignKey('doctrin_fields_groups.id'), index=True)


    def __init__(self, **kwargs):
        super(CustomFieldModel, self).__init__(**kwargs)


    def __repr__(self):
        return '<CustomField %r, %r>' % (self.name, self.field)



class DocumentModel(SearchableMixin, db.Model):
    
    __tablename__ = 'doctrin_documents'

    __searchable__ = ['tracking_number', 'document_number', 'title', 'remarks']

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    tracking_number = db.Column(db.String(25), nullable=False)
    document_number = db.Column(db.String(45), nullable=False)
    title = db.Column(db.String(255), nullable=False)
    remarks = db.Column(db.Text(), nullable=True)
    isarchived = db.Column(db.Boolean, default=False)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow) 
    author_id = db.Column(db.Integer, db.ForeignKey('doctrin_users.id', ondelete='CASCADE'), index=True, nullable=False)
    statuses = db.relationship('StatusTrackingModel', backref='status_document', lazy='dynamic', cascade='all, delete-orphan', order_by='desc(StatusTrackingModel.created)')
    medias = db.relationship('MediaModel', backref='media_document', lazy='dynamic', cascade='all, delete-orphan')   
    person_involves = db.relationship('PersonInvolvedModel', secondary=documents_person_involves, lazy='dynamic',  backref=db.backref('person_involves_documents', lazy='dynamic'))


    def __init__(self, **kwargs):
        super(DocumentModel, self).__init__(**kwargs)


    def __repr__(self):
        return '<Document %r, %r, %r>' % (self.tracking_number, self.document_number, self.title)


    @classmethod
    def count_all(cls, q=None, author_id=None, isarchived=None):
        
        query = cls.query

        if q:
            query = query.filter(or_(cls.tracking_number.like('%'+q+'%'), cls.document_number.like('%'+q+'%'), cls.title.like('%'+q+'%'), cls.remarks.like('%'+q+'%')))

        if author_id:
            query = query.filter(cls.author_id==author_id)

        if isarchived is not None:
            query = query.filter(cls.isarchived==isarchived)


        cached_query = query.options(FromCache(cache))
        return cached_query.count()

    
    @classmethod
    def get_all(cls, q=None, author_id=None, isarchived=None, page=1, perpage=25):
        
        offset = (page - 1) * perpage
    
        query = cls.query

        if q:
            query = query.filter(or_(cls.tracking_number.like('%'+q+'%'), cls.document_number.like('%'+q+'%'), cls.title.like('%'+q+'%'), cls.remarks.like('%'+q+'%')))

        if author_id:
            query = query.filter(cls.author_id==author_id)

        if isarchived is not None:
            query = query.filter(cls.isarchived==isarchived)
        
        query = query.order_by(cls.updated.desc()).limit(perpage).offset(offset)

        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_by(cls, id=None, tracking_number=None, document_number=None):
        query = cls.query

        if id:
            query = query.filter(cls.id==id)
        elif tracking_number:
            query = query.filter(cls.tracking_number==tracking_number)
        elif document_number:
            query = query.filter(cls.document_number==document_number)

        cached_query = query.options(FromCache(cache))
        return cached_query.first()



class DocumentsFieldsModel(db.Model):

    __tablename__ = 'doctrin_documents_fields'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    document_id = db.Column(db.Integer, db.ForeignKey('doctrin_documents.id', ondelete='cascade'), index=True, nullable=False)
    field_id = db.Column(db.Integer, db.ForeignKey('doctrin_fields.id', ondelete='cascade'), index=True, nullable=False)
    value = db.Column(db.Text, nullable=False)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    documents = db.relationship('DocumentModel', backref='documents_fields')
    fields = db.relationship('CustomFieldModel', backref='fields_documents')


    def __init__(self, **kwargs):
        super(DocumentsFieldsModel, self).__init__(**kwargs)


    def __repr__(self):
        return '<DocumentsFields %r, %r, %r, %r>' % (self.id, self.document_id, self.field_id, self.value) 



class StatusModel(db.Model):

    __tablename__ = 'doctrin_status'


    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    name = db.Column(db.String(255), nullable=False)
    slug = db.Column(db.String(255), nullable=False)
    description = db.Column(db.String(255), nullable=True)
    ispublic = db.Column(db.Boolean, default=False)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    organization_id = db.Column(db.Integer, db.ForeignKey('doctrin_organization.id', ondelete='CASCADE'), index=True, nullable=False)

    def __init__(self, **kwargs):
        super(StatusModel, self).__init__(**kwargs)


    def __repr__(self):
        return '<Status %r, %r, %r>' % (self.id, self.name, self.ispublic) 


    @classmethod
    def count_all(cls, q=None, ispublic=None, organization_id=None):
        query = cls.query
        
        if q:
            query = query.filter(cls.name.like('%'+q+'%'))

        if ispublic is not None:
            query = query.filter(cls.ispublic==ispublic)

        if organization_id:
            query = query.filter(cls.organization_id==organization_id)

        cached_query = query.options(FromCache(cache))
        return cached_query.count()


    @classmethod
    def get_all(cls, q=None, ispublic=None, organization_id=None, page=1, perpage=25):

        offset = (page - 1) * perpage

        query = cls.query

        if q:
            query = query.filter(cls.name.like('%'+q+'%'))

        if ispublic is not None:
            query = query.filter(cls.ispublic==ispublic)
        
        if organization_id:
            query = query.filter(cls.organization_id==organization_id)

        query = query.order_by(cls.id.asc()).limit(perpage).offset(offset)

        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_by(cls, id):
        query = cls.query
        cached_query = query.options(FromCache(cache))
        return cached_query.get(id)


class StatusTrackingModel(db.Model):

    __tablename__ = 'doctrin_status_tracking'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    notified = db.Column(db.Boolean, default=False)
    note = db.Column(db.String(255), nullable=True)
    status_id = db.Column(db.Integer, db.ForeignKey('doctrin_status.id', ondelete='CASCADE'), index=True, nullable=False)
    author_id = db.Column(db.Integer, db.ForeignKey('doctrin_users.id', ondelete='CASCADE'), index=True, nullable=False)
    document_id = db.Column(db.Integer, db.ForeignKey('doctrin_documents.id', ondelete='CASCADE'), index=True, nullable=False)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    status = db.relationship('StatusModel', foreign_keys=[status_id])
    author = db.relationship('UserModel', foreign_keys=[author_id])


    def __init__(self, **kwargs):
        super(StatusTrackingModel, self).__init__(**kwargs)


    def __repr__(self):
        return '<StatusTracking %r, %r, %r, %r>' % (self.id, self.status_id, self.author_id, self.document_id) 



class PersonInvolvedModel(db.Model):

    __tablename__ = 'doctrin_person_involved'


    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    fullname = db.Column(db.String(45), nullable=False)
    phone = db.Column(db.String(25), nullable=False)
    note = db.Column(db.String(255), nullable=True)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)


    def __init__(self, **kwargs):
        super(PersonInvolvedModel, self).__init__(**kwargs)


    def __repr__(self):
        return '<PersonInvolved %r, %r, %r>' % (self.id, self.fullname, self.phone)




DoctrinHistory(CustomFieldGroupModel)
DoctrinHistory(CustomFieldModel)
DoctrinHistory(DocumentModel)
DoctrinHistory(StatusModel)
DoctrinHistory(StatusTrackingModel)
DoctrinHistory(PersonInvolvedModel)
