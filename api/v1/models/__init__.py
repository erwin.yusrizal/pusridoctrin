from flask import current_app
from flask_sqlalchemy_caching import FromCache


from api import db, cache
from api.utils.search import(
    add_index,
    update_index,
    delete_index,
    query_index
)

class SearchableMixin(object):

    @classmethod
    def search(cls, expression, page, perpage):
        ids, total = query_index(cls.__tablename__, expression, page, perpage)

        if total == 0:
            return [], 0

        when = []

        for i in range(len(ids)):
            when.append((ids[i], i))

        query = cls.query.filter(cls.id.in_(ids)).order_by(db.case(when, value=cls.id))
        cached_query = query.options(FromCache(cache))
        return cached_query.all(), total

    @classmethod
    def before_commit(cls, session):
        session._changes = {
            'add': list(session.new),
            'update': list(session.dirty),
            'delete': list(session.deleted)
        }


    @classmethod
    def after_commit(cls, session):

        for obj in session._changes['add']:
            if isinstance(obj, SearchableMixin):
                add_index(obj.__tablename__, obj)

        for obj in session._changes['update']:
            if isinstance(obj, SearchableMixin):
                add_index(obj.__tablename__, obj)

        for obj in session._changes['delete']:
            if isinstance(obj, SearchableMixin):
                delete_index(obj.__tablename__, obj)

    @classmethod
    def reindex(cls):
        for obj in cls.query:
            add_index(cls.__tablename__, obj)



db.event.listen(db.session, 'before_commit', SearchableMixin.before_commit)
db.event.listen(db.session, 'after_commit', SearchableMixin.after_commit)

