import datetime
import json

from flask import request, g
from flask_jwt_extended import get_jwt_identity
from sqlalchemy import event
from sqlalchemy.ext.mutable import MutableDict
from sqlalchemy_utils import JSONType
from sqlalchemy.orm.attributes import get_history
from sqlalchemy import asc, desc
from sqlalchemy.sql.expression import func, and_, or_

from flask_sqlalchemy_caching import FromCache, RelationshipCache

from ua_parser import user_agent_parser as ua_parser

from api import db, cache
from api.utils import reverse_id


class HistoryModel(db.Model):

    __tablename__ = 'doctrin_histories'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    module_id = db.Column(db.Integer, nullable=False)
    module = db.Column(db.String(100), nullable=False)
    action = db.Column(db.String(30), nullable=False)
    content = db.Column(MutableDict.as_mutable(JSONType), nullable=False)
    remote_address = db.Column(db.String(20), nullable=False)
    user_agent = db.Column(MutableDict.as_mutable(JSONType), nullable=False)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('doctrin_users.id', ondelete='CASCADE'), index=True, nullable=True)

    def __init__(self, **kwargs):
        super(HistoryModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<History %r>' % self.module


    @classmethod
    def count_all(cls, module=None, user_id=None, start_date=None, end_date=None):

        query = cls.query

        if module:
            query = query.filter(cls.module==module)

        if user_id:
            query = query.filter(cls.user_id==user_id)

        if start_date and end_date:
            query = query.filter(cls.created >= start_date, cls.created <= end_date)

        cached_query = query.options(FromCache(cache))
        return cached_query.count()


    @classmethod
    def get_all(cls, module=None, user_id=None, start_date=None, end_date=None, page=1, perpage=25):

        offset = (page - 1) * perpage

        query = cls.query

        if module:
            query = query.filter(cls.module==module)

        if user_id:
            query = query.filter(cls.user_id==user_id)

        if start_date and end_date:
            
            query = query.filter(cls.created >= start_date, cls.created <= end_date)
        
        query = query.order_by(cls.created.desc()).limit(perpage).offset(offset)

        cached_query = query.options(FromCache(cache))
        return cached_query.all()


class DoctrinHistory():
    
    def __init__(self, model):

        self.model = model
        self.exlude = ['passkey', 'passkey_expire', 'verification_code']

        self.listen()

    def get_clients_ip(self):

        headers_list = request.headers.getlist("X-Forwarded-For")
        user_ip = headers_list[0] if headers_list else request.remote_addr
        return user_ip

    def listen(self):

        event.listen(self.model, "before_update", self._handle_model_update_history)
        event.listen(self.model, "after_insert", self._handle_model_insert_history)
        event.listen(self.model, "after_delete",self._handle_model_delete_history)

    def _get_changed_fields(self, model):

        res = {}

        for k in model.__table__.columns._data.keys():
            if k not in self.exlude:
                v = getattr(model, k)
                if k.startswith("_sa_"):
                    continue
                hist = get_history(model, k)
                if hist.has_changes():
                    res[k] = {
                        "from": str(hist.deleted),
                        "to": str(hist.added)
                    }
        return res


    def _handle_model_update_history(self, mapper, connection, target):

        changed_fields = self._get_changed_fields(target)

        if len(changed_fields) == 0:
            return
        if target.id is None:
            return

        user_id = get_jwt_identity() or None
        user_id = reverse_id(user_id) if user_id else None

        remote_addr = ''
        user_agent = ''

        try:
            remote_addr = self.get_clients_ip()
            user_agent = ua_parser.Parse(request.headers.get("User-Agent"))
        except Exception as e:
            remote_addr = 'System'
            user_agent = 'System'

        insert = HistoryModel.__table__.insert().values(
            module_id = target.id,
            module = target.__class__.__name__.replace('Model', ''),
            action = "Edit",
            content = changed_fields,
            remote_address = remote_addr,
            user_agent = user_agent,
            user_id = user_id
        )

        result = connection.execute(insert)


    def _handle_model_insert_history(self, mapper, connection, target):

        if target.id is None:
            return

        obj_fields = {c.name: str(getattr(target, c.name)) for c in target.__table__.columns}
       
        user_id = get_jwt_identity() or None
        user_id = reverse_id(user_id) if user_id else None

        remote_addr = ''
        user_agent = ''

        try:
            remote_addr = self.get_clients_ip()
            user_agent = ua_parser.Parse(request.headers.get("User-Agent"))
        except Exception as e:
            remote_addr = 'System'
            user_agent = 'System'

        insert = HistoryModel.__table__.insert().values(
            module_id = target.id,
            module = target.__class__.__name__.replace('Model', ''),
            action = "Create",
            content = obj_fields,
            remote_address = remote_addr,
            user_agent = user_agent,
            user_id = user_id
        )

        result = connection.execute(insert)


    def _handle_model_delete_history(self, mapper, connection, target):

        if target.id is None:
            return

        
        obj_fields = {c.name: str(getattr(target, c.name)) for c in target.__table__.columns}

        user_id = get_jwt_identity() or None
        user_id = reverse_id(user_id) if user_id else None        

        remote_addr = ''
        user_agent = ''

        try:
            remote_addr = self.get_clients_ip()
            user_agent = ua_parser.Parse(request.headers.get("User-Agent"))
        except Exception as e:
            remote_addr = 'System'
            user_agent = 'System'

        insert = HistoryModel.__table__.insert().values(
            module_id = target.id,
            module = target.__class__.__name__.replace('Model', ''),
            action = "Delete",
            content = obj_fields,
            remote_address = remote_addr,
            user_agent = user_agent,
            user_id = user_id
        )
        
        result = connection.execute(insert)
