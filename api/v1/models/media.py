import datetime

from api import db, cache
from api.v1.models.histories import DoctrinHistory
from api.v1.models import SearchableMixin

from flask_sqlalchemy_caching import FromCache, RelationshipCache


class MediaModel(SearchableMixin, db.Model):

    __tablename__ = 'doctrin_media'

    __searchable__ = ['original_filename']

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    lookup = db.Column(db.String(255), nullable=True)
    original_filename = db.Column(db.String(255), nullable=False)
    filename = db.Column(db.String(255), nullable=False)
    size = db.Column(db.String(50), nullable=False)
    mimetype = db.Column(db.String(50), nullable=False)
    ismain = db.Column(db.Boolean, default=False)
    isorphan = db.Column(db.Boolean, default=False)
    expired = db.Column(db.Integer)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    document_id = db.Column(db.Integer, db.ForeignKey('doctrin_documents.id', ondelete='CASCADE'), index=True, nullable=True)


    def __init__(self, **kwargs):
    	super(MediaModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<Media %r, %r, %r>' % (self.id, self.original_filename, self.filename)


    @classmethod
    def count_all(cls, q=None):

        query = cls.query.filter(cls.isorphan==False)

        if q:
            query = query.filter(cls.original_filename.like('%'+q+'%'))

        cached_query = query.options(FromCache(cache))
        return cached_query.count()


    @classmethod
    def get_all(cls, q=None, page=1, perpage=25):

        offset = (page -1) * perpage

        query = cls.query.filter(cls.isorphan==False)

        if q:
            query = query.filter(cls.original_filename.like('%'+q+'%'))

        query = query.order_by(cls.created.desc()).limit(perpage).offset(offset)

        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_by(cls, id):

        query = cls.query

        cached_query = query.options(FromCache(cache))
        return cached_query.get(id)


    
DoctrinHistory(MediaModel)
