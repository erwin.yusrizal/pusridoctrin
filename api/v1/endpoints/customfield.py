# -*- coding: utf-8 -*-

import bleach

from slugify import slugify

from flask import (
    g,
    current_app,
    jsonify,
    request,
    url_for
)
from flask_cors import cross_origin
from flask_jwt_extended import (
    jwt_required
)
from flask_restful import (
    Resource
)
from webargs import (
    fields,
    validate,
    missing,
    ValidationError
)
from webargs.flaskparser import (
    use_args,
    parser
)

from api import cache, db
from api.v1 import api
from api.v1.models.documents import (
    CustomFieldGroupModel,
    CustomFieldModel
)
from api.v1.schemas.documents import(
    CustomFieldGroupSchema,
    CustomFieldSchema
)
from api.v1.models.histories import (
    HistoryModel
)
from api.utils import(
    reverse_id
)   
from api.utils.pagination import(
    Pagination
)
from api.decorators import (
    current_user,
    permission_required
)


class CustomFields(Resource):
    
    '''
    @method GET
    @endpoint /v1/customfields
    @group required
    @return custom field list
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    get_args = {
        'q': fields.Str(missing=None),
        'organization': fields.Int(missing=None),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=[
            validate.OneOf(choices=[10, 25, 50, 100], error="Invalid choice")
        ], missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('customfield', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' All Custom Field '''

        q = args['q']
        organization_id = reverse_id(args['organization']) if args['organization'] else None

        page = args['page']
        perpage = args['perpage']
        offset = (page - 1) * perpage

        total = CustomFieldGroupModel.count_all(q, organization_id)
        query = CustomFieldGroupModel.get_all(q, organization_id, page, perpage)

        schema = CustomFieldGroupSchema(many=True, only=args['only'], exclude=args['exclude'])
        groups = schema.dump(query).data

        pagination = Pagination('api.customfields', total, **args)

        return jsonify({
            'success': True,
            'message': True,
            'data': {
                'groups': groups,
                'pagination': pagination.paginate
            }
        }), 200


    '''
    @method POST
    @endpoint /v1/customfields
    @group required
    @return new custom field group object
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    post_args = {
        'name': fields.Str(required=True),
        'slug': fields.Str(missing=None),
        'description': fields.Str(missing=None),
        'organization': fields.Int(required=True),
        'fields': fields.List(fields.Dict(), required=True),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('customfield', 'create')
    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' Add Custom Field '''

        name = bleach.clean(payload['name'], strip=True)
        slug = slugify(payload['slug']) if payload['slug'] else slugify(payload['name'])
        description = bleach.clean(payload['description'], strip=True) if payload['description'] else None
        organization_id = reverse_id(payload['organization'])
        fields = payload['fields']


        is_exist = CustomFieldGroupModel.query.filter(CustomFieldGroupModel.slug==slug, CustomFieldGroupModel.organization_id==organization_id).first()

        if is_exist:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'fieldgroup': ['Field group with the given name and organization already exist.']
                    }
                }
            }), 422


        fieldgroup = CustomFieldGroupModel()
        fieldgroup.name = name
        fieldgroup.slug = slug
        fieldgroup.description = description
        fieldgroup.organization_id = organization_id

        db.session.add(fieldgroup)
        db.session.commit()

        for field in fields:
            newfield = CustomFieldModel()
            newfield.name = field['name']
            newfield.slug = slugify(field['name']) 
            newfield.description = field['description'] if 'description' in field else None
            newfield.field = field['field']
            newfield.default = field['default'] if 'default' in field else None
            newfield.value = field['value'] if 'value' in field else None
            newfield.options = field['options'] if 'options' in field else None
            newfield.required = field['required'] if 'required' in field else False
            newfield.group_id = fieldgroup.id

            db.session.add(newfield)
            db.session.commit()

        schema = CustomFieldGroupSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(fieldgroup).data

        return jsonify({
            'success': True,
            'message': 'New field group has been added',
            'data': {
                'group': result
            }
        }), 201

api.add_resource(CustomFields, '/customfields')




class CustomField(Resource):
    
    '''
    @method GET
    @endpoint /v1/customfields/<int:group_id/>
    @group required
    @return custom field object
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('customfield', 'viewown')
    @use_args(get_args, locations=['query'])
    def get(self, args, group_id):

        ''' View Custom Field '''

        group_id = reverse_id(group_id)
        fieldgroup = CustomFieldGroupModel.get_by(group_id)

        if not fieldgroup:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'group': ['Group not found or has been deleted.']
                    }
                }
            }), 404

        schema = CustomFieldGroupSchema(only=args['only'], exclude=args['exclude'])
        result = schema.dump(fieldgroup).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'group': result
            }
        }), 200


    '''
    @method PUT
    @endpoint /v1/customfields/<int:group_id>
    @group required
    @return updated group object
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    put_args = {
        'name': fields.Str(required=True),
        'slug': fields.Str(missing=None),
        'description': fields.Str(missing=None),
        'organization': fields.Int(required=True),
        'fields': fields.List(fields.Dict(), required=True),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('customfield', 'editown')
    @use_args(put_args, locations=['json'])
    def put(self, payload, group_id):

        ''' Update Group '''

        group_id = reverse_id(group_id)
        fieldgroup = CustomFieldGroupModel.get_by(group_id)

        if not fieldgroup:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'group': ['Group not found or has been deleted.']
                    }
                }
            }), 404

        name = bleach.clean(payload['name'], strip=True)
        slug = slugify(payload['slug']) if payload['slug'] else slugify(payload['name'])
        description = bleach.clean(payload['description'], strip=True) if payload['description'] else None
        organization_id = reverse_id(payload['organization'])
        fields = payload['fields']

        is_exist = CustomFieldGroupModel.query.filter(CustomFieldGroupModel.slug==slug, CustomFieldGroupModel.organization_id==organization_id, CustomFieldGroupModel.id!=fieldgroup.id).first()

        if is_exist:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'group': ['Group with the given name already exist.']
                    }
                }
            }), 422


        fieldgroup.name = name
        fieldgroup.slug = slug
        fieldgroup.description = description
        fieldgroup.organization_id = organization_id

        fieldgroup.fields = []

        db.session.commit()

        for field in fields:
            newfield = CustomFieldModel()
            newfield.name = field['name']
            newfield.slug = slugify(field['name']) 
            newfield.description = field['description'] if 'description' in field else None
            newfield.field = field['field']
            newfield.default = field['default'] if 'default' in field else None
            newfield.value = field['value'] if 'value' in field else None
            newfield.options = field['options'] if 'options' in field else None
            newfield.required = field['required'] if 'required' in field else False
            newfield.group_id = fieldgroup.id

            db.session.add(newfield)
            db.session.commit()

        schema = CustomFieldGroupSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(fieldgroup).data

        return jsonify({
            'success': True,
            'message': 'Group has been updated',
            'data': {
                'group': result
            }
        }), 200



    '''
    @method DELETE
    @endpoint /v1/customfields/<int:group_id>
    @group required
    @return delete group object
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    put_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('customfield', 'deleteown')
    @use_args(put_args, locations=['json'])
    def delete(self, payload, group_id):

        ''' Delete Group '''

        group_id = reverse_id(group_id)
        group = CustomFieldGroupModel.get_by(group_id)

        if not group:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'group': ['Group not found or has been deleted.']
                    }
                }
            }), 404

        schema = CustomFieldGroupSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(group).data

        db.session.delete(group)
        db.session.commit()

        return jsonify({
            'success': True,
            'message': 'Group has been deleted and cannot be undone',
            'data': {
                'group': result
            }
        }), 200


api.add_resource(CustomField, '/customfields/<int:group_id>')



