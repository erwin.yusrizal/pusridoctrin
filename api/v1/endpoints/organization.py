# -*- coding: utf-8 -*-

import bleach

from slugify import slugify

from flask import (
    g,
    current_app,
    jsonify,
    request,
    url_for
)
from flask_cors import cross_origin
from flask_jwt_extended import (
    jwt_required
)
from flask_restful import (
    Resource
)
from webargs import (
    fields,
    validate,
    missing,
    ValidationError
)
from webargs.flaskparser import (
    use_args,
    parser
)

from api import cache, db
from api.v1 import api
from api.v1.models.users import (
    OrganizationModel
)
from api.v1.schemas.users import(
    OrganizationSchema
)
from api.v1.models.histories import (
    HistoryModel
)
from api.utils import(
    reverse_id
)   
from api.utils.pagination import(
    Pagination
)
from api.decorators import (
    current_user,
    permission_required
)


class Organizations(Resource):
    
    '''
    @method GET
    @endpoint /v1/organizations
    @organization required
    @return organization list
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    get_args = {
        'q': fields.Str(missing=None),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=[
            validate.OneOf(choices=[10, 25, 50, 100], error="Invalid choice")
        ], missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('organization', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' All Organizations '''

        q = args['q']

        page = args['page']
        perpage = args['perpage']
        offset = (page - 1) * perpage

        total = OrganizationModel.count_all(q)
        query = OrganizationModel.get_all(q, page, perpage)

        schema = OrganizationSchema(many=True, only=args['only'], exclude=args['exclude'])
        organizations = schema.dump(query).data

        pagination = Pagination('api.organizations', total, **args)

        return jsonify({
            'success': True,
            'message': True,
            'data': {
                'organizations': organizations,
                'pagination': pagination.paginate
            }
        }), 200


    '''
    @method POST
    @endpoint /v1/organizations
    @organization required
    @return new organization object
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    post_args = { 
        'code': fields.Str(required=True),
        'name': fields.Str(required=True),
        'cost_center': fields.Str(required=True),
        'description': fields.Str(missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('organization', 'create')
    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' Create Organizations '''
        
        code = bleach.clean(payload['code'], strip=True)
        name = bleach.clean(payload['name'], strip=True)
        cost_center = bleach.clean(payload['cost_center'], strip=True)
        description = bleach.clean(payload['description'], strip=True) if payload['description'] else None

        is_exist = OrganizationModel.query.filter(OrganizationModel.code==code).first()

        if is_exist:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'organization': ['Organization with the given code already exist.']
                    }
                }
            }), 422



        organization = OrganizationModel()
        organization.code = code
        organization.name = name
        organization.cost_center = cost_center
        organization.description = description

        db.session.add(organization)
        db.session.commit()

        schema = OrganizationSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(organization).data

        return jsonify({
            'success': True,
            'message': 'New organization has been added',
            'data': {
                'organization': result
            }
        }), 201

api.add_resource(Organizations, '/organizations')




class Organization(Resource):
    
    '''
    @method GET
    @endpoint /v1/organizations/<int:organization_id/>
    @organization required
    @return organization object
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('organization', 'viewother')
    @use_args(get_args, locations=['query'])
    def get(self, args, organization_id):

        ''' View Organization '''

        organization_id = reverse_id(organization_id)
        organization = OrganizationModel.get_by(organization_id)

        if not organization:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'organization': ['Organization not found or has been deleted.']
                    }
                }
            }), 404

        schema = OrganizationSchema(only=args['only'], exclude=args['exclude'])
        result = schema.dump(organization).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'organization': result
            }
        }), 200


    '''
    @method PUT
    @endpoint /v1/organizations/<int:organization_id>
    @organization required
    @return updated organization object
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    put_args = { 
        'code': fields.Str(required=True),
        'name': fields.Str(required=True),
        'cost_center': fields.Str(required=True),
        'description': fields.Str(missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('organization', 'editother')
    @use_args(put_args, locations=['json'])
    def put(self, payload, organization_id):

        ''' Update Organization '''

        organization_id = reverse_id(organization_id)
        organization = OrganizationModel.get_by(organization_id)

        if not organization:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'organization': ['Organization not found or has been deleted.']
                    }
                }
            }), 404

        code = bleach.clean(payload['code'], strip=True)
        name = bleach.clean(payload['name'], strip=True)
        cost_center = bleach.clean(payload['cost_center'], strip=True)
        description = bleach.clean(payload['description'], strip=True) if payload['description'] else None

        is_exist = OrganizationModel.query.filter(OrganizationModel.code==code,OrganizationModel.id!=organization.id).first()

        if is_exist:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'organization': ['Organization with the given name already exist.']
                    }
                }
            }), 422


        organization.code = code
        organization.name = name
        organization.cost_center = cost_center
        organization.description = description

        db.session.commit()

        schema = OrganizationSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(organization).data

        return jsonify({
            'success': True,
            'message': 'Organization has been updated',
            'data': {
                'organization': result
            }
        }), 200



    '''
    @method DELETE
    @endpoint /v1/organizations/<int:organization_id>
    @organization required
    @return delete organization object
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    put_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('organization', 'deleteother')
    @use_args(put_args, locations=['json'])
    def delete(self, payload, organization_id):

        ''' Delete Organization '''

        organization_id = reverse_id(organization_id)
        organization = OrganizationModel.get_by(organization_id)

        if not organization:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'organization': ['Organization not found or has been deleted.']
                    }
                }
            }), 404


        schema = OrganizationSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(organization).data

        db.session.delete(organization)

        return jsonify({
            'success': True,
            'message': 'Organization has been deleted and cannot be undone',
            'data': {
                'organization': result
            }
        }), 200


api.add_resource(Organization, '/organizations/<int:organization_id>')



