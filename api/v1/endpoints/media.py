# -*- coding: utf-8 -*-
import os
import time
import uuid
import easywebdav

from ua_parser import user_agent_parser as ua_parser

from datetime import (
    datetime,
    timedelta
)
from werkzeug.utils import secure_filename
from sqlalchemy import asc, desc
from sqlalchemy.sql.expression import func, and_, or_

from flask import (
    g,
    current_app,
    jsonify,
    request,
    url_for
)
from flask_cors import cross_origin
from flask_jwt_extended import (
    jwt_required
)
from flask_restful import (
    Resource
)
from webargs import (
    fields,
    validate,
    missing,
    ValidationError
)
from webargs.flaskparser import (
    use_args,
    use_kwargs,
    parser
)
from slugify import slugify
from api import cache, db, jwt, mail
from api.v1 import api
from api.v1.models.media import(
    MediaModel
)
from api.v1.schemas.media import (
        MediaTmpSchema
)
from api.v1.models.documents import (
    DocumentModel
)
from api.v1.schemas.documents import (
    DocumentSchema
)
from api.utils import(
    allowed_file, 
    is_dir_exist, 
    reverse_id
)
from api.decorators import (
    permission_required,
    current_user
)
from api.utils.pagination import Pagination



class MediaTmps(Resource):


    """
    @method GET
    @endpoint /v1/media/tmp
    @permission jwt_required
    @return media object
    """

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    get_args = {
        'q': fields.Str(missing=None),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=[
            validate.OneOf(choices=[10, 25, 50, 100], error="Invalid choice")
        ], missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()    
    @jwt_required
    @current_user()
    @permission_required('media', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        q = args['q']
        page = args['page']
        perpage = args['perpage']

        total = MediaModel.count_all(q)
        query = MediaModel.get_all(q, page, perpage)

        schema = MediaTmpSchema(many=True, only=args['only'], exclude=args['exclude'])
        medias = schema.dump(query).data

        pagination = Pagination('api.mediatmps', total, **args)

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'medias': medias,
                'pagination': pagination.paginate
            }
        }), 200



    """
    @method POST
    @endpoint /v1/media/tmp
    @permission jwt_required, media, create
    @return media object
    """
    
    post_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    file_args = {
        'media': fields.Field(required=True)
    }

    @cross_origin()    
    @jwt_required
    @current_user()
    @permission_required('media', 'create')
    @use_args(post_args, locations=['query'])
    @use_args(file_args, locations=['files'])
    def post(self, payload, media_args):
        

        files = media_args['media']

        fn = secure_filename(files.filename)
        name, extension = os.path.splitext(files.filename)
        filename = str(uuid.uuid4()) + extension

        mimetype = files.content_type

        if not allowed_file(filename):
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
			'file_type': ['Supported file format: png, jpg, jpeg, pdf, doc, docx, xls, xlsx']
                    }
                }
            }), 422

        uploaded_file_path = os.path.join(current_app.config.get('TMP_FOLDER'), filename)

        is_dir_exist(uploaded_file_path)
        files.save(uploaded_file_path)

        size = os.path.getsize(uploaded_file_path)

        if size > (8 * 1024 * 1024):
            os.remove(os.path.join(current_app.config.get('TMP_FOLDER'), filename))
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'file_size': ['File size cannot more than 8MB']
                    }
                }
            }), 422
        
        media = MediaModel()
	media.original_filename = fn
        media.filename = filename
        media.size = size
        media.mimetype = mimetype 
        media.ismain = 0      
        media.isorphan = 1

        nexthour = datetime.now() + timedelta(hours=1)
        nexthour_timestamp = time.mktime(nexthour.timetuple())
        media.expired = nexthour_timestamp

        db.session.add(media)        
        db.session.commit()
        
        schema = MediaTmpSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(media).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'media': result
            }
        }), 201


api.add_resource(MediaTmps, '/media/tmp')


class MediaTmp(Resource):

    """
    @method GET
    @endpoint /v1/media/tmp/<int:media_id>
    @permission jwt_required, media, delete
    @return delete media object
    """

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('media', 'viewown')
    @use_args(get_args, locations=['query'])
    def get(self, args, media_id):
    
        media_id = reverse_id(media_id)
        media = MediaModel.get_by(media_id)

        if not media:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'media': ['File not found or has been deleted.']
                    }
                }
            }), 404


        schema = MediaTmpSchema(only=args['only'], exclude=args['exclude'])
        result = schema.dump(media).data


        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'media': result
            }
            
        }), 200


    
    """
    @method DELETE
    @endpoint /v1/media/tmp/<int:media_id>
    @permission jwt_required, media, delete
    @return delete media object
    """

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('media', 'deleteown')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, media_id):

        media_id = reverse_id(media_id)
        media = MediaModel.query.get(media_id)

        if media is None:
            return jsonify({
                'success': False,
                'code': 404,
                'errors': {
                    'messages': {
                        'media': ['File not found or has been deleted.']
                    }
                }
            }), 404

        try:

            schema = MediaTmpSchema(only=payload['only'], exclude=payload['exclude'])
            result = schema.dump(media).data

            os.remove(os.path.join(current_app.config.get('TMP_FOLDER'), media.filename))
            db.session.delete(media)
            db.session.commit()

            return jsonify({
                'success': True,
                'message': 'File has been deleted and cannot be undone.',
                'data': {
                    'media': result    
                }
            }), 200

        except Exception as e:
            return jsonify({
                'success': False,                
                'errors': {
                    'code': 422,
                    'messages': {
                        'media': [str(e)]
                    }
                }
            }), 422


api.add_resource(MediaTmp, '/media/tmp/<int:media_id>')



class MediaDocuments(Resource):


    """
    @method POST
    @endpoint /v1/media/documents/<int:document_id>
    @permission jwt_required, media, delete
    @return delete media object
    """


    post_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    file_args = {
        'media': fields.Field(required=True)
    }

    @cross_origin()    
    @jwt_required
    @current_user()
    @permission_required('media', 'editown')
    @use_args(post_args, locations=['query'])
    @use_args(file_args, locations=['files'])
    def post(self, payload, media_args, document_id):
        

        document_id = reverse_id(document_id)
        document = DocumentModel.get_by(document_id)

        if not document:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'document': ['Document not found or already deleted.']
                    }
                }
            }), 404


        files = media_args['media']

        fn = secure_filename(files.filename)
        name, extension = os.path.splitext(files.filename)
        filename = str(uuid.uuid4()) + extension

        mimetype = files.content_type

        if not allowed_file(filename):
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
			'file_type': ['Supported file format: png, jpg, jpeg, pdf, doc, docx, xls, xlsx']
                    }
                }
            }), 422

        uploaded_file_path = os.path.join(current_app.config.get('TMP_FOLDER'), filename)

        is_dir_exist(uploaded_file_path)
        files.save(uploaded_file_path)

        size = os.path.getsize(uploaded_file_path)

        if size > (8 * 1024 * 1024):
            os.remove(os.path.join(current_app.config.get('TMP_FOLDER'), filename))
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'file_size': ['File size cannot more than 8MB']
                    }
                }
            }), 422
        
        media = MediaModel()
	media.original_filename = fn
        media.filename = filename
        media.size = size
        media.mimetype = mimetype 
        media.ismain = False    
        media.isorphan = False
        media.expired = None
        media.document_id = document.id

        db.session.add(media)        
        db.session.commit()


        webdav = easywebdav.connect(
            current_app.config.get('WEBDAV_HOST'),
            username=current_app.config.get('WEBDAV_USERNAME'),
            password=current_app.config.get('WEBDAV_PASSWORD'),
            protocol=current_app.config.get('WEBDAV_PROTOCOL'),
            port=current_app.config.get('WEBDAV_PORT'),
            path=current_app.config.get('WEBDAV_PATH')
        )

        file_path = os.path.join(current_app.config.get('TMP_FOLDER'), media.filename)             
        remote_path = '/doctrin/'+document.document_author.user_organization.code+'/'+document.document_number+'/'
                
        if not webdav.exists(remote_path):
            webdav.mkdirs(remote_path)

        webdav.upload(file_path, remote_path+'/'+media.original_filename)

        schema = MediaTmpSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(media).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'media': result
            }
        }), 201


api.add_resource(MediaDocuments, '/media/documents/<int:document_id>')



class MediaDocument(Resource):

    """
    @method DELETE
    @endpoint /v1/media/documents/<int:media_id>
    @permission jwt_required, media, deleteown
    @return media object
    """

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('media', 'deleteown')
    def delete(self, media_id):

        media_id = reverse_id(media_id)
        media = MediaModel.query.get(media_id)

        if not media:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'media': ['File not found or has been deleted.']
                    }
                }
            }), 404

        schema = MediaTmpSchema()
        result = schema.dump(media).data

        try:
            
            webdav = easywebdav.connect(
                current_app.config.get('WEBDAV_HOST'),
                username=current_app.config.get('WEBDAV_USERNAME'),
                password=current_app.config.get('WEBDAV_PASSWORD'),
                protocol=current_app.config.get('WEBDAV_PROTOCOL'),
                port=current_app.config.get('WEBDAV_PORT'),
                path=current_app.config.get('WEBDAV_PATH')
            )

            file_path = os.path.join(current_app.config.get('TMP_FOLDER'), media.filename)             
            remote_path = '/doctrin/'+media.media_document.document_author.user_organization.code+'/'+media.media_document.document_number+'/'+media.original_filename

            webdav.delete(remote_path)

            os.remove(os.path.join(current_app.config.get('TMP_FOLDER'), media.filename))

            db.session.delete(media)
            db.session.commit()

            return jsonify({
                'success': True,
                'message': 'File has been deleted and cannot be undone.',
                'data': {
                    'media': result
                }
            }), 200

        except Exception as e:
            return jsonify({
                'success': False,
                'code': 422,
                'errors': {
                    'messages': {
                        'media': [str(e)]
                    }
                }
            }), 422

api.add_resource(MediaDocument, '/media/documents/<int:media_id>')
