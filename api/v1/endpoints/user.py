# -*- coding: utf-8 -*-
import re
import bleach

from slugify import slugify

from flask import (
    g,
    current_app,
    jsonify,
    request,
    url_for
)
from flask_cors import cross_origin
from flask_jwt_extended import (
    jwt_required
)
from flask_restful import (
    Resource
)
from webargs import (
    fields,
    validate,
    missing,
    ValidationError
)
from webargs.flaskparser import (
    use_args,
    parser
)

from api import cache, db
from api.v1 import api
from api.v1.models.users import (
    UserModel
)
from api.v1.schemas.users import(
    UserSchema
)
from api.v1.models.histories import (
    HistoryModel
)
from api.utils import(
    reverse_id,
    normalize_phone_number
)   
from api.utils.pagination import(
    Pagination
)
from api.decorators import (
    current_user,
    permission_required
)


class Users(Resource):
    
    '''
    @method GET
    @endpoint /v1/users
    @user required
    @return user list
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    get_args = {
        'q': fields.Str(missing=None),
        'status': fields.Str(validate=[
            validate.OneOf(choices=['unverified', 'verified', 'banned'], error="Invalid choice")
        ], missing=None),
        'organization': fields.Int(missing=None),
        'role': fields.Int(missing=None),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=[
            validate.OneOf(choices=[10, 25, 50, 100], error="Invalid choice")
        ], missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('user', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' All Users '''

        q = args['q']
        status = args['status']
        role_id = reverse_id(args['role']) if args['role'] else None
        organization_id = reverse_id(args['organization']) if args['organization'] else None

        page = args['page']
        perpage = args['perpage']
        offset = (page - 1) * perpage

        total = UserModel.count_all(q, organization_id, status, role_id)
        query = UserModel.get_all(q, organization_id, status, role_id, page, perpage)

        schema = UserSchema(many=True, only=args['only'], exclude=args['exclude'])
        users = schema.dump(query).data

        pagination = Pagination('api.users', total, **args)

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'users': users,
                'pagination': pagination.paginate
            }
        }), 200


    '''
    @method POST
    @endpoint /v1/users
    @user required
    @return new user object
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    post_args = {
        'fullname': fields.Str(required=True, validate=[validate_required]),
        'badge': fields.Str(missing=None),
        'phone': fields.Str(required=True, validate=[validate_required]),
        'status': fields.Str(validate=[
            validate.OneOf(choices=['unverified', 'verified', 'banned'], error="Invalid choice")
        ], missing='unverified'),
        'role': fields.Int(required=True),
        'organization': fields.Int(missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('user', 'create')
    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' Add Users '''

        fullname = bleach.clean(payload['fullname'], strip=True)
        badge = slugify(payload['badge']) if payload['badge'] else None
        phone = bleach.clean(payload['phone'], strip=True) if payload['phone'] else None
        status = payload['status']
        role_id = reverse_id(payload['role'])
        organization_id = reverse_id(payload['organization']) if payload['organization'] else None

       	phone_match = re.search(r'\(?(?:\+628|628|08)(?:\d{2,3})?\)?[ .-]?\d{2,4}[ .-]?\d{2,4}[ .-]?\d{2,4}', phone)

        if phone_match is None:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'phone': ['Invalid phone number']
                    }
                }
            }), 422

        phone = normalize_phone_number(phone)

        is_exist = UserModel.query.filter(UserModel.phone==phone).first()

        if is_exist:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'user': ['User with the given phone number already exist.']
                    }
                }
            }), 422


        user = UserModel()
        user.fullname = fullname
        user.badge = badge
        user.phone = phone
        user.status = status
        user.role_id = role_id
        user.organization_id = organization_id

        db.session.add(user)
        db.session.commit()

        schema = UserSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(user).data

        return jsonify({
            'success': True,
            'message': 'New user has been added',
            'data': {
                'user': result
            }
        }), 201

api.add_resource(Users, '/users')


class User(Resource):
    
    '''
    @method GET
    @endpoint /v1/users/<int:user_id/>
    @user required
    @return user object
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('user', 'viewother')
    @use_args(get_args, locations=['query'])
    def get(self, args, user_id):

        ''' View User '''

        user_id = reverse_id(user_id)
        user = UserModel.get_by(user_id)

        if not user:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'user': ['User not found or has been deleted.']
                    }
                }
            }), 404
        
        schema = UserSchema(only=args['only'], exclude=args['exclude'])
        result = schema.dump(user).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'user': result
            }
        }), 200


    '''
    @method PUT
    @endpoint /v1/users/<int:user_id>
    @user required
    @return updated user object
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    put_args = { 
        'fullname': fields.Str(required=True),
        'badge': fields.Str(missing=None),
        'phone': fields.Str(required=True),
        'status': fields.Str(validate=[
            validate.OneOf(choices=['unverified', 'verified', 'banned'], error="Invalid choice")
        ], missing='unverified'),
        'role': fields.Int(required=True),
        'organization': fields.Int(missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('user', 'editother')
    @use_args(put_args, locations=['json'])
    def put(self, payload, user_id):

        ''' Update User '''

        user_id = reverse_id(user_id)
        user = UserModel.get_by(user_id)

        if not user:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'user': ['User not found or has been deleted.']
                    }
                }
            }), 404

        
        fullname = bleach.clean(payload['fullname'], strip=True)
        badge = slugify(payload['badge']) if payload['badge'] else None
        phone = bleach.clean(payload['phone'], strip=True) if payload['phone'] else None
        status = payload['status']
        role_id = reverse_id(payload['role'])
        organization_id = reverse_id(payload['organization']) if payload['organization'] else None
        
        root_users = UserModel.query.filter(UserModel.role_id==2).count()

        if user.user_role.slug == 'root' and role_id != 2 and root_users == 1:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'root': ['You need at least one root user to run this application']
                    }
                }
            }), 422

        
       	phone_match = re.search(r'\(?(?:\+628|628|08)(?:\d{2,3})?\)?[ .-]?\d{2,4}[ .-]?\d{2,4}[ .-]?\d{2,4}', phone)

        if phone_match is None:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'phone': ['Invalid phone number']
                    }
                }
            }), 422

        phone = normalize_phone_number(phone)

        is_exist = UserModel.query.filter(UserModel.phone==phone, UserModel.id!=user.id).first()
        
        if is_exist:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'user': ['User with the given phone number already exist.']
                    }
                }
            }), 422


        user.fullname = fullname
        user.badge = badge
        user.phone = phone
        user.status = status
        user.role_id = role_id
        user.organization_id = organization_id

        db.session.commit()

        schema = UserSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(user).data

        return jsonify({
            'success': True,
            'message': 'User has been updated',
            'data': {
                'user': result
            }
        }), 200



    '''
    @method DELETE
    @endpoint /v1/users/<int:user_id>
    @user required
    @return delete user object
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('user', 'deleteother')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, user_id):

        ''' Delete User '''

        user_id = reverse_id(user_id)
        user = UserModel.get_by(user_id)

        if not user:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'user': ['User not found or has been deleted.']
                    }
                }
            }), 404
       
        root_users = UserModel.query.filter(UserModel.role_id==2).count()

        if user.user_role.slug == 'root' and root_users == 1:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'root': ['You need at least one root user to run this application']
                    }
                }
            }), 422
        else:
            user.role_id = 1
            user.status = 'unverified'

        db.session.commit()

        schema = UserSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(user).data


        return jsonify({
            'success': True,
            'message': 'User has been deleted and cannot be undone',
            'data': {
                'user': result
            }
        }), 200


api.add_resource(User, '/users/<int:user_id>')



class UserMe(Resource):
    
    '''
    @method GET
    @endpoint /v1/users/me
    @user required
    @return user object
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('user', 'viewown')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' View User '''
        
        schema = UserSchema(only=args['only'], exclude=args['exclude'])
        result = schema.dump(g.current_user).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'user': result
            }
        }), 200

    
    
    '''
    @method PUT
    @endpoint /v1/users/me
    @user required
    @return user object
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    put_args = { 
        'fullname': fields.Str(required=True),
        'badge': fields.Str(missing=None),
        'phone': fields.Str(required=True),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('user', 'editown')
    @use_args(put_args, locations=['json'])
    def put(self, payload):

        ''' Edit User '''

        user = UserModel.query.get(g.current_user.id)
        
        fullname = bleach.clean(payload['fullname'], strip=True)
        badge = bleach.clean(payload['badge'], strip=True) if payload['badge'] else None
        phone = payload['phone']
       
       	phone_match = re.search(r'\(?(?:\+628|628|08)(?:\d{2,3})?\)?[ .-]?\d{2,4}[ .-]?\d{2,4}[ .-]?\d{2,4}', phone)

        if phone_match is None:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'phone': ['Invalid phone number']
                    }
                }
            }), 422

        phone = normalize_phone_number(phone)

        is_exist = UserModel.query.filter(UserModel.phone==phone, UserModel.id!=user.id).first()
        
        if is_exist:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'user': ['User with the given phone number already exist.']
                    }
                }
            }), 422

        user.fullname = fullname
        user.badge = badge
        user.phone = phone

        db.session.commit()

        schema = UserSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(user).data

        return jsonify({
            'success': True,
            'message': 'Account has been updated.',
            'data': {
                'user': result
            }
        }), 200

api.add_resource(UserMe, '/users/me')
