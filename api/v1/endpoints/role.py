# -*- coding: utf-8 -*-

import bleach

from slugify import slugify

from flask import (
    g,
    current_app,
    jsonify,
    request,
    url_for
)
from flask_cors import cross_origin
from flask_jwt_extended import (
    jwt_required
)
from flask_restful import (
    Resource
)
from webargs import (
    fields,
    validate,
    missing,
    ValidationError
)
from webargs.flaskparser import (
    use_args,
    parser
)

from api import cache, db
from api.v1 import api
from api.v1.models.users import (
    RoleModel,
    PermissionModel
)
from api.v1.schemas.users import(
    RoleSchema
)
from api.v1.models.histories import (
    HistoryModel
)
from api.utils import(
    reverse_id
)   
from api.utils.pagination import(
    Pagination
)
from api.decorators import (
    current_user,
    permission_required
)


class Roles(Resource):
    
    '''
    @method GET
    @endpoint /v1/roles
    @role required
    @return role list
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    get_args = {
        'q': fields.Str(missing=None),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=[
            validate.OneOf(choices=[10, 25, 50, 100], error="Invalid choice")
        ], missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('role', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' All Roles '''

        q = args['q']

        page = args['page']
        perpage = args['perpage']
        offset = (page - 1) * perpage

        total = RoleModel.count_all(q)
        query = RoleModel.get_all(q, page, perpage)

        schema = RoleSchema(many=True, only=args['only'], exclude=args['exclude'])
        roles = schema.dump(query).data

        pagination = Pagination('api.roles', total, **args)

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'roles': roles,
                'pagination': pagination.paginate
            }
        }), 200


    '''
    @method POST
    @endpoint /v1/roles
    @role required
    @return new role object
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    post_args = {
        'name': fields.Str(required=True),
        'slug': fields.Str(missing=None),
        'description': fields.Str(missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('role', 'create')
    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' Add Roles '''

        name = bleach.clean(payload['name'], strip=True)
        slug = slugify(payload['slug']) if payload['slug'] else slugify(payload['name'])
        description = bleach.clean(payload['description'], strip=True) if payload['description'] else None

        is_exist = RoleModel.query.filter(RoleModel.slug==slug).first()

        if is_exist:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'role': ['Role with the given name already exist.']
                    }
                }
            }), 422


        role = RoleModel()
        role.name = name
        role.slug = slug
        role.description = description

        permissions = PermissionModel.get_all()
        role_permissions = {}

        for p in permissions:
            role_permissions[p.slug] = {
		'menu': 0,
                'create': 0,
                'viewall': 0,
                'viewown': 0,
                'viewother': 0,
                'editown': 0,
                'editother': 0,
                'deleteown': 0,
                'deleteother': 0
            } 

        role.permissions = role_permissions

        db.session.add(role)
        db.session.commit()

        schema = RoleSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(role).data

        return jsonify({
            'success': True,
            'message': 'New role has been added',
            'data': {
                'role': result
            }
        }), 201

api.add_resource(Roles, '/roles')




class Role(Resource):
    
    '''
    @method GET
    @endpoint /v1/roles/<int:role_id/>
    @role required
    @return role object
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('role', 'viewother')
    @use_args(get_args, locations=['query'])
    def get(self, args, role_id):

        ''' View Role '''

        role_id = reverse_id(role_id)
        role = RoleModel.get_by(role_id)

        if not role:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'role': ['Role not found or has been deleted.']
                    }
                }
            }), 404

        schema = RoleSchema(only=args['only'], exclude=args['exclude'])
        result = schema.dump(role).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'role': result
            }
        }), 200


    '''
    @method PUT
    @endpoint /v1/roles/<int:role_id>
    @role required
    @return updated role object
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    put_args = {
        'name': fields.Str(required=True),
        'slug': fields.Str(missing=None),
        'description': fields.Str(missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('role', 'editother')
    @use_args(put_args, locations=['json'])
    def put(self, payload, role_id):

        ''' Update Role '''

        role_id = reverse_id(role_id)
        role = RoleModel.get_by(role_id)

        if not role:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'role': ['Role not found or has been deleted.']
                    }
                }
            }), 404


        if role.slug in ['orphan', 'root']:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'role': ['Default role cannot be edited.']
                    }
                }
            }), 422

        name = bleach.clean(payload['name'], strip=True)
        slug = slugify(payload['slug']) if payload['slug'] else slugify(payload['name'])
        description = bleach.clean(payload['description'], strip=True) if payload['description'] else None

        is_exist = RoleModel.query.filter(RoleModel.slug==slug, RoleModel.id!=role.id).first()

        if is_exist:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'role': ['Role with the given name already exist.']
                    }
                }
            }), 422


        role.name = name
        role.slug = slug
        role.description = description

        db.session.commit()

        schema = RoleSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(role).data

        return jsonify({
            'success': True,
            'message': 'Role has been updated',
            'data': {
                'role': result
            }
        }), 200



    '''
    @method DELETE
    @endpoint /v1/roles/<int:role_id>
    @role required
    @return delete role object
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('role', 'deleteother')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, role_id):

        ''' Delete Role '''

        role_id = reverse_id(role_id)
        role = RoleModel.get_by(role_id)

        if not role:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'role': ['Role not found or has been deleted.']
                    }
                }
            }), 404


        if role.slug in ['orphan', 'root']:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'role': ['Default role cannot be deleted.']
                    }
                }
            }), 422

        
        users = role.users.all()

        if users:
            UserModel.query.filter_by(role_id=role.id).update({'role_id': 1})


        schema = RoleSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(role).data

        db.session.delete(role)
        db.session.commit()

        return jsonify({
            'success': True,
            'message': 'Role has been deleted and cannot be undone',
            'data': {
                'role': result
            }
        }), 200


api.add_resource(Role, '/roles/<int:role_id>')


class RolePermissions(Resource):
    
    '''
    @method GET
    @endpoint /v1/roles/<int:role_id>/permissions
    @permission jwt_required
    @return role object
    '''  

    put_args = {
        'permissions': fields.Dict(required=True),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('role', 'editother')
    @use_args(put_args, locations=['json'])
    def put(self, payload, role_id):

        ''' Edit Role Permissions '''

        role_id = reverse_id(role_id)
        permissions = payload['permissions']
        role = RoleModel.get_by(role_id)

        if not role:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'role': ['Role not found or has been deleted.']
                    }
                }
            }), 404

        default_permissions = PermissionModel.get_all(None, 1, 100)

        default_permissions_list = []
        permission_keys = ['menu', 'create', 'viewall', 'viewown', 'viewother', 'editown', 'editother', 'deleteown', 'deleteother']
        role_permissions = {}

        for dp in default_permissions:
            default_permissions_list.append(dp.slug)

        for key, value in permissions.iteritems():
            if key in default_permissions_list and any(pk in value for pk in permission_keys):
                role_permissions[key] = permissions[key]


        if role_permissions:
            role.permissions = role_permissions
            db.session.commit()

        return jsonify({
            'success': True,
            'message': 'Permission has been updated.',
            'data': {
                'permissions': role.permissions
            }
        }), 200        


api.add_resource(RolePermissions, '/roles/<int:role_id>/permissions')
