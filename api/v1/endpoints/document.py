# -*- coding: utf-8 -*-
import os
import easywebdav
import bleach

from slugify import slugify

from flask import (
    g,
    current_app,
    jsonify,
    request,
    url_for
)
from flask_cors import cross_origin
from flask_jwt_extended import (
    jwt_required
)
from flask_restful import (
    Resource
)
from webargs import (
    fields,
    validate,
    missing,
    ValidationError
)
from webargs.flaskparser import (
    use_args,
    parser
)

from api import cache, db
from api.v1 import api
from api.v1.models.documents import (
    CustomFieldGroupModel,
    CustomFieldModel,
    DocumentModel,
    DocumentsFieldsModel,
    StatusModel,
    StatusTrackingModel,
    PersonInvolvedModel
)
from api.v1.schemas.documents import(
    CustomFieldGroupSchema,
    CustomFieldSchema,
    DocumentSchema
)
from api.v1.models.media import (
    MediaModel
)
from api.v1.models.histories import (
    HistoryModel
)
from api.utils import(
    reverse_id,
    generate_unique_id,
    normalize_phone_number
)   
from api.utils.pagination import(
    Pagination
)
from api.utils.notif import OneSignal
from api.decorators import (
    current_user,
    permission_required
)

from api.tasks import doctrin_sms_queue

class Documents(Resource):
    
    '''
    @method GET
    @endpoint /v1/documents
    @group required
    @return document list
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    get_args = {
        'q': fields.Str(missing=None),
        'author': fields.Int(missing=None),
        'archived': fields.Bool(missing=None),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=[
            validate.OneOf(choices=[10, 25, 50, 100], error="Invalid choice")
        ], missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('document', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' All Documents '''

        q = args['q']
        author_id = reverse_id(args['author']) if args['author'] else None
        isarchived = args['archived']

        page = args['page']
        perpage = args['perpage']
        offset = (page - 1) * perpage

        total = DocumentModel.count_all(q, author_id, isarchived)
        query = DocumentModel.get_all(q, author_id, isarchived, page, perpage)

        schema = DocumentSchema(many=True, only=args['only'], exclude=args['exclude'])
        documents = schema.dump(query).data

        pagination = Pagination('api.documents', total, **args)

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'documents': documents,
                'pagination': pagination.paginate
            }
        }), 200


    '''
    @method POST
    @endpoint /v1/documents
    @permission document required
    @return new document object
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    post_args = {
        'document_number': fields.Str(required=True, validate=[validate_required]),
        'title': fields.Str(required=True, validate=[validate_required]),
        'remarks': fields.Str(missing=None),
        'author': fields.Int(missing=None),
        'persons': fields.List(fields.Dict(), missing=None),
        'fields': fields.List(fields.Dict(), missing=None),
        'medias': fields.List(fields.Int(), missing=None),
        'status': fields.Int(required=True),
        'notified': fields.Bool(missing=False),
        'note': fields.Str(missing=None),
        'archived': fields.Bool(missing=False),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('document', 'create')
    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' Add Document '''

        document_number = bleach.clean(payload['document_number'], strip=True)
        title = bleach.clean(payload['title'], strip=True)
        remarks = bleach.clean(payload['remarks'], strip=True) if payload['remarks'] else None
        author_id = reverse_id(payload['author']) if payload['author'] else g.current_user.id
        media_ids = [reverse_id(media_id) for media_id in payload['medias']] if payload['medias'] else None
        person_involves = payload['persons']
        custom_fields = payload['fields']
        status_id = reverse_id(payload['status'])
        notified = payload['notified']
        note = bleach.clean(payload['note'], strip=True) if payload['note'] else None
        isarchived = payload['archived']

        is_exist = DocumentModel.query.filter(DocumentModel.document_number==document_number).first()

        if is_exist:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'document': ['Document with the given number already exist.']
                    }
                }
            }), 422


        doc = DocumentModel()
        doc.tracking_number = generate_unique_id()
        doc.document_number = document_number
        doc.title = title
        doc.remarks = remarks
        doc.author_id = author_id
        doc.isarchived = archived

        db.session.add(doc)
        db.session.commit()

        status = StatusTrackingModel()
        status.notified = notified
        status.note = note
        status.status_id = status_id
        status.author_id = g.current_user.id
        status.document_id = doc.id

        db.session.add(status)
        db.session.commit()


        if person_involves:
            for p in person_involves:

                if p['fullname'] and p['phone']:

                    person_phone = normalize_phone_number(p['phone'])
                    person = PersonInvolvedModel.query.filter_by(phone=person_phone).first()

                    if not person:
                        person = PersonInvolvedModel()
                        person.fullname = bleach.clean(p['fullname'], strip=True)
                        person.phone = normalize_phone_number(p['phone'])
                        person.note = p['note'] if 'note' in p else None
                        db.session.add(person)

                    doc.person_involves.append(person)
                    db.session.commit()


        if custom_fields:
            for f in custom_fields:
                field_id = reverse_id(f['id'])
                field = DocumentsFieldsModel.query.filter_by(document_id=doc.id,field_id=field_id).first()

                if not field:
                    field = DocumentsFieldsModel()
                    field.document_id = doc.id
                    field.field_id = field_id
                    field.value = bleach.clean(f['value'], strip=True)
                    db.session.add(field)
                    db.session.commit()


        if media_ids:
            webdav = easywebdav.connect(
                current_app.config.get('WEBDAV_HOST'),
                username=current_app.config.get('WEBDAV_USERNAME'),
                password=current_app.config.get('WEBDAV_PASSWORD'),
                protocol=current_app.config.get('WEBDAV_PROTOCOL'),
                port=current_app.config.get('WEBDAV_PORT'),
                path=current_app.config.get('WEBDAV_PATH')
            )

            for media_id in media_ids:
                media = MediaModel.query.get(media_id)

                if media and media.isorphan:
       	            file_path = os.path.join(current_app.config.get('TMP_FOLDER'), media.filename)             
                    remote_path = '/doctrin/'+doc.document_author.user_organization.code+'/'+doc.document_number+'/'
                    
                    if not webdav.exists(remote_path):
                        webdav.mkdirs(remote_path)

                    webdav.upload(file_path, remote_path+'/'+media.original_filename)

                    media.isorphan = None
                    media.expired = None
                    media.document_id = doc.id
                    db.session.commit()

        schema = DocumentSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(doc).data

        if notified and person_involves:
            for p in person_involves:
                if p['fullname'] and p['phone']:
                    phone = normalize_phone_number(p['phone'])

                    doctrin_sms_queue.apply_async(kwargs={
                        'phonenumber': phone,
                        'content': 'Yth Bpk/Ibu ' + p['fullname']  + ', status dokumen anda dengan nomor tracking ' + doc.tracking_number + ' adalah ' + status.name
                    }, countdown=60, queue='DOCTRINQUEUE')


        return jsonify({
            'success': True,
            'message': 'New document has been added',
            'data': {
                'document': result
            }
        }), 201

api.add_resource(Documents, '/documents')



class Document(Resource):
    
    '''
    @method GET
    @endpoint /v1/documents/<int:document_id>
    @group required
    @return document object
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('document', 'viewown')
    @use_args(get_args, locations=['query'])
    def get(self, args, document_id):

        ''' View Document '''

        document_id = reverse_id(document_id)
        document = DocumentModel.get_by(document_id)

        if not document:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'document': ['Document not found or has been deleted.']
                    }
                }
            }), 404

        schema = DocumentSchema(only=args['only'], exclude=args['exclude'])
        result = schema.dump(document).data
        
        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'document': result
            }
        }), 200


    '''
    @method PUT
    @endpoint /v1/documents/<int:document_id>
    @permission document required
    @return updated document object
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    put_args = {
        'document_number': fields.Str(required=True, validate=[validate_required]),
        'title': fields.Str(required=True, validate=[validate_required]),
        'remarks': fields.Str(missing=None),
        'author': fields.Int(missing=None),
        'persons': fields.List(fields.Dict(), missing=None),
        'fields': fields.List(fields.Dict(), missing=None),
        'status': fields.Int(required=True),
        'notified': fields.Bool(missing=None),
        'note': fields.Str(missing=None),
        'archived': fields.Bool(missing=False),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('document', 'editother')
    @use_args(put_args, locations=['json'])
    def put(self, payload, document_id):

        ''' Update Document '''

        document_id = reverse_id(document_id)
        doc = DocumentModel.get_by(document_id)

        if not doc:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'document': ['Document not found or has been deleted.']
                    }
                }
            }), 404

        document_number = bleach.clean(payload['document_number'], strip=True)
        title = bleach.clean(payload['title'], strip=True)
        remarks = bleach.clean(payload['remarks'], strip=True) if payload['remarks'] else None
        author_id = reverse_id(payload['author']) if payload['author'] else g.current_user.id
        person_involves = payload['persons']
        custom_fields = payload['fields']
        status_id = reverse_id(payload['status'])
        notified = payload['notified']
        note = bleach.clean(payload['note'], strip=True) if payload['note'] else None
        isarchived = payload['archived']

        is_exist = DocumentModel.query.filter(DocumentModel.document_number==document_number,DocumentModel.id!=doc.id).first()

        if is_exist:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'document': ['Document with the given number already exist.']
                    }
                }
            }), 422


        doc.document_number = document_number
        doc.title = title
        doc.remarks = remarks
        doc.isarchived = isarchived

        db.session.commit()

        status = StatusTrackingModel.query.filter_by(author_id=author_id, document_id=doc.id).order_by(StatusTrackingModel.id.asc()).first()

        if status:
            if status.status_id != status_id:
                status.status_id = status_id
                status.note = note
                db.session.commit()
        else:
            status = StatusTrackingModel()
            status.notified = notified
            statue.note = note
            status.status_id = status_id
            status.author_id = author_id
            status.document_id = doc.id

            db.session.add(status)
            db.session.commit()


        if person_involves:

            doc.person_involves = []

            for p in person_involves:

                if p['fullname'] and p['phone']:

                    person_phone = normalize_phone_number(p['phone'])
                    person = PersonInvolvedModel.query.filter_by(phone=person_phone).first()

                    if not person:
                        person = PersonInvolvedModel()
                        person.fullname = bleach.clean(p['fullname'], strip=True)
                        person.phone = normalize_phone_number(p['phone'])
                        person.note = p['note'] if 'note' in p else None
                        db.session.add(person)

                    doc.person_involves.append(person)
            db.session.commit()


        if custom_fields:
            for f in custom_fields:
                field_id = reverse_id(f['id'])
                field = DocumentsFieldsModel.query.filter_by(document_id=doc.id,field_id=field_id).first()

                if not field:
                    field = DocumentsFieldsModel()
                    field.document_id = doc.id
                    field.field_id = field_id
                    field.value = bleach.clean(f['value'], strip=True)
                    db.session.add(field)
                    db.session.commit()

                else:

                    field.value = bleach.clean(f['value'], strip=True)
                    db.session.commit()


        schema = DocumentSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(doc).data

        if notified and person_involves:
            for p in person_involves:
                if p['fullname'] and p['phone']:
                    phone = normalize_phone_number(p['phone'])

                    doctrin_sms_queue.apply_async(kwargs={
                        'phonenumber': phone,
                        'content': 'Yth Bpk/Ibu ' + p['fullname']  + ', status dokumen anda dengan nomor tracking ' + doc.tracking_number + ' adalah ' + status.name
                    }, countdown=60, queue='DOCTRINQUEUE')
        
        
        return jsonify({
            'success': True,
            'message': 'Document has been updated',
            'data': {
                'document': result
            }
        }), 201

    
    
    '''
    @method DELETE
    @endpoint /v1/documents/<int:document_id>
    @permission document required
    @return deleted document object
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('document', 'deleteother')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, document_id):

        ''' Delete Document '''

        document_id = reverse_id(document_id)
        document = DocumentModel.get_by(document_id)

        if not document:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'document': ['Document not found or has been deleted.']
                    }
                }
                }), 404


        if document.isarchived:

            if document.medias:
                
                for media in document.medias:
                    os.remove(os.path.join(current_app.config.get('TMP_FOLDER'), media.filename))

                webdav = easywebdav.connect(
                    current_app.config.get('WEBDAV_HOST'),
                    username=current_app.config.get('WEBDAV_USERNAME'),
                    password=current_app.config.get('WEBDAV_PASSWORD'),
                    protocol=current_app.config.get('WEBDAV_PROTOCOL'),
                    port=current_app.config.get('WEBDAV_PORT'),
                    path=current_app.config.get('WEBDAV_PATH')
                )

                remote_path = '/doctrin/'+document.document_organization.code+'/'+document.document_number+'/'

                if webdav.exists(remote_path):
                    webdav.rmdir(remote_path)

            schema = DocumentSchema(only=payload['only'], exclude=payload['exclude'])
            result = schema.dump(document).data

            db.session.delete(document)
            db.session.commit()

            return jsonify({
                'success': True,
                'message': 'Document has been deleted and cannot be undone',
                'data': {
                    'document': result
                }
            }), 200


        else:
            document.isarchived = True
            db.session.commit()

            return jsonify({
                'success': True,
                'message': 'Document has been achived',
                'data': {
                    'document': result
                }
            }), 200
           


api.add_resource(Document, '/documents/<int:document_id>')



class DocumentSearch(Resource):
    
    '''
    @method GET
    @endpoint /v1/documents/search
    @group required
    @return document list
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    get_args = {
        'q': fields.Str(required=True, validate=[validate_required]),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=[
            validate.OneOf(choices=[10, 25, 50, 100], error="Invalid choice")
        ], missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('document', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' Search Document '''

        q = args['q']
        page = args['page']
        perpage = args['perpage']

        documents, total = DocumentModel.search(q, page, perpage)

        schema = DocumentSchema(many=True, only=args['only'], exclude=args['exclude'])
        result = schema.dump(documents).data
        
        pagination = Pagination('api.documentsearch', total, **args)

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'documents': result,
                'pagination': pagination.paginate
            }
        }), 200


api.add_resource(DocumentSearch, '/documents/search')


class DocumentStatus(Resource):


    '''
    @method PUT
    @endpoint /v1/documents/<int:document_id>/status
    @permission document required
    @return updated document object
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    put_args = {
        'status': fields.Int(required=True),
        'note': fields.Str(missing=None),
        'notified': fields.Bool(missing=False),
        'persons': fields.List(fields.Str(), missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('document', 'editother')
    @use_args(put_args, locations=['json'])
    def put(self, payload, document_id):

        ''' Update Document '''

        document_id = reverse_id(document_id)
        doc = DocumentModel.get_by(document_id)

        if not doc:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'document': ['Document not found or has been deleted.']
                    }
                }
            }), 404

        
        status_id = reverse_id(payload['status'])
        note = bleach.clean(payload['note']) if payload['note'] else None
        notified = payload['notified']
        persons = payload['persons']

        status = StatusModel.query.get(status_id)

        if not status:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'status': ['Status not found or has been deleted']
                    }
                }
            }), 404

        status_tracking  = StatusTrackingModel()
        status_tracking.notified = notified
        status_tracking.note = note
        status_tracking.status_id = status.id
        status_tracking.document_id = doc.id
        status_tracking.author_id = g.current_user.id


        db.session.add(status_tracking)
        db.session.commit()

        # send sms to person involves
    
        #if notified and persons:
        #    for ph in persons:
        #        phone = normalize_phone_number(ph)
        #
        #        doctrin_sms_queue(kwargs={
        #            'phone': phone,
        #            'content': 'Your document ' + doc.tracking_number  + ' status updated: ' + status.name
        #        }, countdown=60, queue='DOCTRINQUEUE')


        schema = DocumentSchema(only=payload['only'], exclude=payload['axclude'])
        document = schema.dump(doc).data

        return jsonify({
            'success': True,
            'message': 'Document ' + doc.tracking_number + ' has been updated',
            'data': {
                'document': document
            }
        }), 200



api.add_resource(DocumentStatus, '/documents/<int:document_id>/status')

