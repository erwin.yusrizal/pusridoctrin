# -*- coding: utf-8 -*-

import bleach

from slugify import slugify

from flask import (
    g,
    current_app,
    jsonify,
    request,
    url_for
)
from flask_cors import cross_origin
from flask_jwt_extended import (
    jwt_required
)
from flask_restful import (
    Resource
)
from webargs import (
    fields,
    validate,
    missing,
    ValidationError
)
from webargs.flaskparser import (
    use_args,
    parser
)

from api import cache, db
from api.v1 import api
from api.v1.models.users import (
    PermissionModel,
    RoleModel
)
from api.v1.schemas.users import(
    PermissionSchema
)
from api.v1.models.histories import (
    HistoryModel
)
from api.utils import(
    reverse_id
)   
from api.utils.pagination import(
    Pagination
)
from api.decorators import (
    current_user,
    permission_required
)


class Permissions(Resource):
    
    '''
    @method GET
    @endpoint /v1/permissions
    @permission required
    @return permission list
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    get_args = {
        'q': fields.Str(missing=None),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=[
            validate.OneOf(choices=[10, 25, 50, 100], error="Invalid choice")
        ], missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('permission', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' All Permissions '''

        q = args['q']

        page = args['page']
        perpage = args['perpage']
        offset = (page - 1) * perpage

        total = PermissionModel.count_all(q)
        query = PermissionModel.get_all(q, page, perpage)

        schema = PermissionSchema(many=True, only=args['only'], exclude=args['exclude'])
        permissions = schema.dump(query).data

        pagination = Pagination('api.permissions', total, **args)

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'permissions': permissions,
                'pagination': pagination.paginate
            }
        }), 200


    '''
    @method POST
    @endpoint /v1/permissions
    @permission required
    @return new permission object
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    post_args = {
        'name': fields.Str(required=True),
        'slug': fields.Str(missing=None),
        'description': fields.Str(missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('permission', 'create')
    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' Add Permission '''

        name = bleach.clean(payload['name'], strip=True)
        slug = slugify(payload['slug']) if payload['slug'] else slugify(payload['name'])
        description = bleach.clean(payload['description'], strip=True) if payload['description'] else None

        is_exist = PermissionModel.query.filter(PermissionModel.slug==slug).first()

        if is_exist:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'permission': ['Permission with the given name already exist.']
                    }
                }
            }), 422

        slug = slug.replace('-', '')

        permission = PermissionModel()
        permission.name = name
        permission.slug = slug
        permission.description = description

        db.session.add(permission)
        db.session.commit()

        roles = RoleModel.get_all()

        for role in roles:
            role_permissions = role.permissions
            
            role_permissions[slug] = {
                'menu': 0,
                'create': 0,
                'viewall': 0,
                'viewown': 0,
                'viewother': 0,
                'editown': 0,
                'editother': 0,
                'deleteown': 0,
                'deleteother': 0
            }

            role.permissions = role_permissions

            db.session.commit()

        schema = PermissionSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(permission).data

        return jsonify({
            'success': True,
            'message': 'New permission has been added',
            'data': {
                'permission': result
            }
        }), 201

api.add_resource(Permissions, '/permissions')



class Permission(Resource):
    
    '''
    @method GET
    @endpoint /v1/permissions/<int:permission_id/>
    @permission required
    @return permission object
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('permission', 'viewother')
    @use_args(get_args, locations=['query'])
    def get(self, args, permission_id):

        ''' View Permission '''

        permission_id = reverse_id(permission_id)
        permission = PermissionModel.get_by(permission_id)

        if not permission:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'permission': ['Permission not found or has been deleted.']
                    }
                }
            }), 404

        schema = PermissionSchema(only=args['only'], exclude=args['exclude'])
        result = schema.dump(permission).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'permission': result
            }
        }), 200


    '''
    @method PUT
    @endpoint /v1/permissions/<int:permission_id>
    @permission required
    @return updated permission object
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    put_args = {
        'name': fields.Str(required=True),
        'slug': fields.Str(missing=None),
        'description': fields.Str(missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('permission', 'editother')
    @use_args(put_args, locations=['json'])
    def put(self, payload, permission_id):

        ''' Update Permission '''

        permission_id = reverse_id(permission_id)
        permission = PermissionModel.get_by(permission_id)

        if not permission:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'permission': ['Permission not found or has been deleted.']
                    }
                }
            }), 404

        name = bleach.clean(payload['name'], strip=True)
        slug = slugify(payload['slug']) if payload['slug'] else slugify(payload['name'])
        description = bleach.clean(payload['description'], strip=True) if payload['description'] else None

        is_exist = PermissionModel.query.filter(PermissionModel.slug==slug, PermissionModel.id!=permission.id).first()

        if is_exist:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'permission': ['Permission with the given name already exist.']
                    }
                }
            }), 422

        old_slug = permission.slug

        slug = slug.replace('-','')

        permission.name = name
        permission.slug = slug
        permission.description = description

        db.session.commit()

        if old_slug != slug:

            roles = RoleModel.get_all()

            for role in roles:
                role_permissions = role.permissions
                curr_permissions = role_permissions[old_slug]

                if role_permissions[old_slug]:
                    del role_permissions[old_slug]

                role_permissions[slug] = curr_permissions

                role.permissions = role_permissions

                db.session.commit()


        schema = PermissionSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(permission).data

        return jsonify({
            'success': True,
            'message': 'Permission has been updated',
            'data': {
                'permission': result
            }
        }), 200



    '''
    @method DELETE
    @endpoint /v1/permissions/<int:permission_id>
    @permission required
    @return delete permission object
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    put_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('permission', 'deleteother')
    @use_args(put_args, locations=['json'])
    def delete(self, payload, permission_id):

        ''' Delete Permission '''

        permission_id = reverse_id(permission_id)
        permission = PermissionModel.get_by(permission_id)

        if not permission:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'permission': ['Permission not found or has been deleted.']
                    }
                }
            }), 404


        schema = PermissionSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(permission).data

        deleted_slug = permission.slug

        db.session.delete(permission)
        db.session.commit()

        roles = RoleModel.get_all()

        for role in roles:
            role_permissions = role.permissions

            try:
                del role_permissions[deleted_slug]
                role.permissions = role_permissions
                db.session.commit()
            except:
                pass


        return jsonify({
            'success': True,
            'message': 'Permission has been deleted and cannot be undone',
            'data': {
                'permission': result
            }
        }), 200


api.add_resource(Permission, '/permissions/<int:permission_id>')



