# -*- coding: utf-8 -*-
import arrow
import bleach
import re

from flask import (
    g,
    current_app,
    jsonify
)

from flask_cors import cross_origin
from flask_jwt_extended import jwt_required
from flask_restful import Resource
from webargs import (
    fields,
    validate,
    missing,
    ValidationError
)
from webargs.flaskparser import(
    use_args,
    use_kwargs,
    parser
)
from slugify import slugify

from api import cache, db
from api.v1 import api

from api.v1.models.documents import (
    DocumentModel
)
from api.v1.schemas.documents import(
    DocumentSchema
)
from api.utils import(
    reverse_id,
    normalize_phone_number,
    denormalize_phone_number
)
from api.utils.pagination import Pagination
from api.decorators import (
    permission_required,
    current_user
)


class Tracking(Resource):
    '''
    @method GET
    @endpoint /v1/tracking
    @access public
    '''

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError('Please fill all required fields')

    get_args = {
        'tracking_number': fields.Str(required=True),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
 
    }

    @cross_origin()
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' TRACKING DOCUMENT '''

        tracking_number = args['tracking_number']
        
        document = DocumentModel.get_by(tracking_number=tracking_number)
        
        schema = DocumentSchema(only=args['only'], exclude=args['exclude'])
        result = schema.dump(document).data


        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'document': result
            }
        }), 200


api.add_resource(Tracking, '/tracking')

