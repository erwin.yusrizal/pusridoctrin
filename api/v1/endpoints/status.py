# -*- coding: utf-8 -*-

import bleach

from slugify import slugify

from flask import (
    g,
    current_app,
    jsonify,
    request,
    url_for
)
from flask_cors import cross_origin
from flask_jwt_extended import (
    jwt_required
)
from flask_restful import (
    Resource
)
from webargs import (
    fields,
    validate,
    missing,
    ValidationError
)
from webargs.flaskparser import (
    use_args,
    parser
)

from api import cache, db
from api.v1 import api
from api.v1.models.documents import (
    StatusModel
)
from api.v1.schemas.documents import(
    StatusSchema
)
from api.v1.models.histories import (
    HistoryModel
)
from api.utils import(
    reverse_id
)   
from api.utils.pagination import(
    Pagination
)
from api.decorators import (
    current_user,
    permission_required
)


class Statuses(Resource):
    
    '''
    @method GET
    @endpoint /v1/statuses
    @role required
    @return role list
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    get_args = {
        'q': fields.Str(missing=None),
        'ispublic': fields.Bool(missing=None),
        'organization': fields.Int(missing=None),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=[
            validate.OneOf(choices=[10, 25, 50, 100], error="Invalid choice")
        ], missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('status', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' All Statuses '''

        q = args['q']
        ispublic = args['ispublic']
        organization_id = reverse_id(args['organization']) if args['organization'] else None
        
        page = args['page']
        perpage = args['perpage']
        offset = (page - 1) * perpage

        total = StatusModel.count_all(q, ispublic, organization_id)
        query = StatusModel.get_all(q, ispublic, organization_id, page, perpage)

        schema = StatusSchema(many=True, only=args['only'], exclude=args['exclude'])
        statuses = schema.dump(query).data

        pagination = Pagination('api.statuses', total, **args)

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'statuses': statuses,
                'pagination': pagination.paginate
            }
        }), 200


    '''
    @method POST
    @endpoint /v1/statuses
    @role required
    @return new role object
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    post_args = {
        'name': fields.Str(required=True, validate=[validate_required]),
        'slug': fields.Str(missing=None),
        'description': fields.Str(missing=None),
        'ispublic': fields.Bool(missing=False),
        'organization': fields.Int(required=True),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('status', 'create')
    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' Add Statuses '''

        name = bleach.clean(payload['name'], strip=True)
        slug = slugify(payload['slug']) if payload['slug'] else slugify(payload['name'])
        description = bleach.clean(payload['description'], strip=True) if payload['description'] else None
        ispublic = payload['ispublic']
        organization_id = reverse_id(payload['organization'])

        is_exist = StatusModel.query.filter(StatusModel.slug==slug, StatusModel.organization_id==organization_id).first()

        if is_exist:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'status': ['Status with the given name and organization already exist.']
                    }
                }
            }), 422


        status = StatusModel()
        status.name = name
        status.slug = slug
        status.description = description
        status.ispublic = ispublic
        status.organization_id = organization_id
        
        db.session.add(status)
        db.session.commit()

        schema = StatusSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(status).data

        return jsonify({
            'success': True,
            'message': 'New status has been added',
            'data': {
                'status': result
            }
        }), 201

api.add_resource(Statuses, '/statuses')


class Status(Resource):
    
    '''
    @method GET
    @endpoint /v1/statuses/<int:status_id/>
    @role required
    @return status object
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('status', 'viewown')
    @use_args(get_args, locations=['query'])
    def get(self, args, status_id):

        ''' View Status '''

        status_id = reverse_id(status_id)
        status = StatusModel.get_by(status_id)

        if not status:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'status': ['Status not found or has been deleted.']
                    }
                }
            }), 404

        schema = StatusSchema(only=args['only'], exclude=args['exclude'])
        result = schema.dump(status).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'status': result
            }
        }), 200


    '''
    @method PUT
    @endpoint /v1/statuses/<int:status_id>
    @role required
    @return updated role object
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    put_args = {
        'name': fields.Str(required=True, validate=[validate_required]),
        'slug': fields.Str(missing=None),
        'description': fields.Str(missing=None),
        'ispublic': fields.Bool(missing=None),
        'organization': fields.Int(required=True),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('status', 'editown')
    @use_args(put_args, locations=['json'])
    def put(self, payload, status_id):

        ''' Update Status '''

        status_id = reverse_id(status_id)
        status = StatusModel.get_by(status_id)

        if not status:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'status': ['Status not found or has been deleted.']
                    }
                }
            }), 404

        name = bleach.clean(payload['name'], strip=True)
        slug = slugify(payload['slug']) if payload['slug'] else slugify(payload['name'])
        description = bleach.clean(payload['description'], strip=True) if payload['description'] else None
        ispublic = payload['ispublic']
        organization_id = reverse_id(payload['organization'])

        is_exist = StatusModel.query.filter(StatusModel.slug==slug, StatusModel.organization_id==organization_id, StatusModel.id!=status.id).first()

        if is_exist:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'status': ['Status with the given name already exist.']
                    }
                }
            }), 422


        status.name = name
        status.slug = slug
        status.description = description
        status.ispublic = ispublic
        status.organization_id = organization_id

        db.session.commit()

        schema = StatusSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(status).data

        return jsonify({
            'success': True,
            'message': 'Status has been updated',
            'data': {
                'status': result
            }
        }), 200



    '''
    @method DELETE
    @endpoint /v1/statuses/<int:status_id>
    @role required
    @return delete role object
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('status', 'deleteown')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, status_id):

        ''' Delete Status '''

        status_id = reverse_id(status_id)
        status = StatusModel.get_by(status_id)

        if not status:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'status': ['Status not found or has been deleted.']
                    }
                }
            }), 404

        schema = StatusSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(status).data

        db.session.delete(status)
        db.session.commit()

        return jsonify({
            'success': True,
            'message': 'Status has been deleted and cannot be undone',
            'data': {
                'status': result
            }
        }), 200


api.add_resource(Status, '/statuses/<int:status_id>')



