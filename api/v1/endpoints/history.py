# -*- coding: utf-8 -*-

import bleach

from slugify import slugify

from flask import (
    g,
    current_app,
    jsonify,
    request,
    url_for
)
from flask_cors import cross_origin
from flask_jwt_extended import (
    jwt_required
)
from flask_restful import (
    Resource
)
from webargs import (
    fields,
    validate,
    missing,
    ValidationError
)
from webargs.flaskparser import (
    use_args,
    parser
)

from api import cache, db
from api.v1 import api
from api.v1.models.histories import (
    HistoryModel
)
from api.v1.schemas.histories import(
    HistorySchema
)
from api.utils import(
    reverse_id
)   
from api.utils.pagination import(
    Pagination
)
from api.decorators import (
    current_user,
    permission_required
)


class Histories(Resource):
    
    '''
    @method GET
    @endpoint /v1/histories
    @role required
    @return history list
    
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    get_args = {
        'module': fields.Str(missing=None),
        'user': fields.Int(missing=None),
        'daterange': fields.Str(missing=None),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=[
            validate.OneOf(choices=[10, 25, 50, 100], error="Invalid choice")
        ], missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('history', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' All Histories '''

        module = args['module']
        user_id = reverse_id(args['user']) if args['user'] else None 
        start_date, end_date = args['daterange'].split(',') if args['daterange'] else [None, None]
        
        page = args['page']
        perpage = args['perpage']

        total = HistoryModel.count_all(module, user_id, start_date, end_date)
        histories = HistoryModel.get_all(module, user_id, start_date, end_date, page, perpage)

        schema = HistorySchema(many=True, only=args['only'], exclude=args['exclude'])
        result = schema.dump(histories).data

        pagination = Pagination('api.histories', total, **args)

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'histories': result,
                'pagination': pagination.paginate
            }
        }), 200
        

api.add_resource(Histories, '/histories')
