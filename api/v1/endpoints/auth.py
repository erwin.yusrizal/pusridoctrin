# -*- coding: utf-8 -*-

import arrow
import bleach
import redis
import time
import re

from ua_parser import user_agent_parser as ua_parser

from datetime import (
    datetime,
    timedelta
)

from flask import (
    g,
    current_app,
    jsonify,
    request,
    url_for
)
from flask_cors import cross_origin
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    jwt_required,
    jwt_refresh_token_required,
    get_jti,
    get_jwt_identity,
    get_raw_jwt
)
from flask_restful import (
    Resource
)
from webargs import (
    fields,
    validate,
    missing,
    ValidationError
)
from webargs.flaskparser import (
    use_args,
    parser
)

from api import cache, db, jwt, mail
from api.v1 import api
from api.v1.models.users import (
    UserModel,
    RoleModel
)
from api.v1.schemas.users import(
    UserSchema
)
from api.v1.models.histories import (
    HistoryModel
)
from api.tasks import (
    doctrin_email_queue
)
from api.utils import(
    reverse_id,
    parse_verification_token,
    get_clients_ip,
    normalize_phone_number,
    denormalize_phone_number,
)   
from api.decorators import (
    anonymous_required,
    permission_required,
    current_user
)


ACCESS_EXPIRES = timedelta(days=30)
REFRESH_EXPIRES = timedelta(days=30)
REFRESH_REVOKED = timedelta(days=365)


revoked_store = redis.StrictRedis(
    host='localhost', 
    port=6379, 
    db=2, 
    password='root', 
    decode_responses=True
)

@jwt.user_claims_loader
def add_claims_to_access_token(user):
    return {'id': reverse_id(user.id)}


@jwt.user_identity_loader
def user_identity_lookup(user):
    return reverse_id(user.id)


@jwt.unauthorized_loader
def unauthorized_loader_callback(callback):
    return jsonify({
        'success': False,
        'errors': {
            'code': 401,
            'messages': {
                'authorization': ['You are not authorized to access this resource.']
            }
        }
    }), 401


@jwt.expired_token_loader
def expired_token_loader_callback():
    return jsonify({
        'success': False,
        'errors': {
            'code': 401,
            'messages': {
                'authorization': ['Token has expired.']
            }
        }
    }), 401


@jwt.invalid_token_loader
def invalid_token_loader_callback(callback):
    return jsonify({
        'success': False,
        'errors': {
            'code': 401,
            'messages': {
                'authorization': ['Invalid token.']
            }
        }
    }), 401


@jwt.revoked_token_loader
def revoked_token_callback():
    return jsonify({
        'success': False,
        'errors': {
            'code': 401,
            'messages': {
                'authorization': ['You already loggged out. You need to login to continue.']
            }
        }
    }), 401


@jwt.token_in_blacklist_loader
def check_if_token_is_revoked(callback):
    jti = callback['jti']
    entry = revoked_store.get(jti)
    if entry is None:
        return True
    return entry == 'true'


class Login(Resource):
    
    '''
    @method POST
    @endpoint /v1/auth/login
    @permission anonymous
    @return verification_code, expiracy
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    login_args = {
        'identity': fields.Str(required=True, validate=[validate_required]),
        'access': fields.Str(required=True, validate=[
            validate_required,
            validate.OneOf(choices=['dashboard', 'mobile'], error="Invalid choice")
        ]) 
    }

    @cross_origin()
    @anonymous_required()
    @use_args(login_args, locations=['json'])
    def post(self, payload):

        ''' Login '''

        identity = payload['identity']
        access = payload['access']
        phone_match = re.search(r'\(?(?:\+628|628|08)(?:\d{2,3})?\)?[ .-]?\d{2,4}[ .-]?\d{2,4}[ .-]?\d{2,4}', identity)

        if phone_match:
            phone = normalize_phone_number(identity)
            user = UserModel.get_by(phone=phone)
        else:
            user = None

        if not user:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'identity': ['Invalid phone number.']
                    }
                }
            }), 422

        elif not  user.check_permission(access, 'viewall'):

            return jsonify({
                'success': False,
                'errors': {
                    'code': 403,
                    'messages': {
                        'permission': ['You do not have permission to access this resource.']
                    }
                }
            }), 403

        elif user.status != 'verified':
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'status': ['Your account is inactive or has been banned by administrator.']
                    }
                }
            }), 422

        else:

            verification_code = user.verify()
            return jsonify({
                'success': True,
                'message': '',
                'data': {
                    'verification_code': verification_code
                }                
            }), 200


api.add_resource(Login, '/auth/login')


class Verify(Resource):
    
    '''
    @method POST
    @endpoint /v1/auth/verify
    @permission anonymous
    @return access_token
    '''

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError('Please fill all required fields')   

    verify_args = {
        'code': fields.Str(required=True, validate=[validate_required]),
        'passkey': fields.Str(required=True, validate=[validate_required]),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @anonymous_required()
    @use_args(verify_args, locations=['json'])
    def post(self, payload):

        ''' Verification '''

        code = payload['code']
        passkey = payload['passkey']

        data = parse_verification_token(code)

        list_keys = ['module', 'signature', 'expire']

        if not any(p in list_keys for p in data) or (data['expire'] <= int(time.time())):
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'verification_code': ['Invalid verification code or has expired.']
                    }
                }
            }), 422

        else:
            user = UserModel.query.filter(
                UserModel.passkey==passkey,
                UserModel.verification_code==str(data['signature']), 
                UserModel.passkey_expire >= int(time.time())).first()        

            if user is None:
                return jsonify({
                    'success': False,
                    'errors': {
                        'code': 422,
                        'messages': {
                            'passkey': ['Invalid passkey or has expired.']
                        }
                    }
                }), 422

            else:  

                user.passkey = None
                user.passkey_expire = None
                user.verification_code = None

                user_agent = ua_parser.Parse(request.headers.get("User-Agent"))
                
                history = HistoryModel()
                history.module_id = user.id
                history.module = 'User'
                history.action = 'Login'
                history.content = {}
                history.remote_address = get_clients_ip()
                history.user_agent = user_agent
                history.user_id = user.id

                user.histories.append(history)
                db.session.commit()              
            
                access_token = create_access_token(user)
                refresh_token = create_refresh_token(user)

                access_jti = get_jti(encoded_token=access_token)
                refresh_jti = get_jti(encoded_token=refresh_token)
                revoked_store.set(access_jti, 'false', ACCESS_EXPIRES)
                revoked_store.set(refresh_jti, 'false', REFRESH_EXPIRES)

                schema = UserSchema(only=payload['only'], exclude=payload['exclude'])
                person = schema.dump(user).data

                return jsonify({
                    'success': True,
                    'message': '',
                    'data': {
                        'user': person,
                        'access_token': access_token,
                        'refresh_token': refresh_token
                    }  
                }), 200



    '''
    @method PUT
    @endpoint /v1/auth/verify
    @permission anonymous
    @return verification_code
    '''

    reverify_args = {
        'code': fields.Str(required=True, validate=[validate_required])
    }

    @cross_origin()
    @use_args(reverify_args, locations=['json'])
    def put(self, payload):
        
        ''' Reverify '''        
        
        code = payload['code']

        data = parse_verification_token(code)
        list_keys = ['signature', 'expire']

        if not any(p in list_keys for p in data):
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'verification_code': ['Invalid verification code or has expired.']
                    }
                }
            }), 422

        else:

            user = UserModel.query.filter_by(verification_code=data['signature']).first()
            if user is None:
                return jsonify({
                    'success': False,
                    'errors': {
                        'code': 422,
                        'messages': {
                            'verification_code': ['Invalid verification code or has expired.']
                        }
                    }
                }), 422
            else:
                
                verification_code = user.verify()
                return jsonify({
                    'success': True,
                    'message': '',
                    'data': {
                        'verification_code': verification_code
                    }
                }), 200


api.add_resource(Verify, '/auth/verify')


class RefreshToken(Resource):
        
    '''
    @method POST
    @endpoint /v1/auth/refresh
    @permission jwt_refresh_token_required
    @return new access token
    '''

    @cross_origin()
    @jwt_refresh_token_required
    def post(self):
        
        ''' Refresh Token '''
        
        userid = reverse_id(get_jwt_identity())
        user = UserModel.query.get(userid)

        if user is None:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 401,
                    'messages': {
                        'authorization': ['You are not authorized to access this resource.']
                    }
                }
            }), 401

        access_token = create_access_token(user)
        access_jti = get_jti(encoded_token=access_token)

        revoked_store.set(access_jti, 'false', ACCESS_EXPIRES)

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'access_token': access_token
            }
        }), 200
        

api.add_resource(RefreshToken, '/auth/refresh')


class AccessRevoke(Resource):
    
    '''
    @method DELETE
    @endpoint /v1/auth/revoke/access
    @permission jwt_required
    @return success message
    '''

    @cross_origin()
    @jwt_required
    def delete(self):
        
        ''' Delete Access Token (Logout) '''

        userid = reverse_id(get_jwt_identity())
        user = UserModel.query.get(userid)

        if user is None:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'code': ['Invalid token.']
                    }
                }
            }), 422


        history = HistoryModel()
        history.module_id = user.id
        history.module = 'User'
        history.action = 'Logout'
        history.content = {}
        history.remote_address = get_clients_ip()
        history.user_agent = ua_parser.Parse(request.headers.get("User-Agent"))
        history.user_id = user.id

        user.histories.append(history)
        db.session.commit()

        jti = get_raw_jwt()['jti']
        revoked_store.set(jti, 'true', ACCESS_EXPIRES)

        return jsonify({
            'success': True,
            'message': 'You have logged out.',
            'data': {}
        }), 200


api.add_resource(AccessRevoke, '/auth/revoke/access')


class RefreshRevoke(Resource):

    '''
    @method DELETE
    @endpoint /v1/auth/revoke/refresh
    @permission jwt_refresh_token_required
    @return success message
    '''

    @cross_origin()
    @jwt_refresh_token_required
    def delete(self):

        ''' Delete Refresh Token (Logout) '''

        jti = get_raw_jwt()['jti']
        revoked_store.set(jti, 'true', REFRESH_REVOKED)

        return jsonify({
            'success': True,
            'message': 'You have logged out.',
            'data': {}
        }), 200


api.add_resource(RefreshRevoke, '/auth/revoke/refresh')



class Device(Resource):

    '''
    @method PUT
    @endpoint /v1/auth/device
    @permission @jwt_required
    @return success message
    '''

    @cross_origin()
    @jwt_required
    @current_user()
    def put(self):

        ''' Update device id '''

        user = UserModel.query.get(g.current_user.id)
        nid = request.headers.get('nid')

        if not user.nid:
            user.nid = nid
            db.session.commit()

        elif user.nid != nid:
            user.nid = nid
            db.session.commit()

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'nid': user.nid
            }
        }), 200


api.add_resource(Device, '/auth/device')

