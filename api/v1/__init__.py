from flask import Blueprint
from flask_restful import Api, abort
from webargs.flaskparser import parser
from api import limiter

''' Register Blueprint '''

blueprint = Blueprint('api', __name__, url_prefix='/v1')
api = Api(blueprint)

''' Ratelimit Error Handler '''

limiter.limit('200/minute', error_message=dict(
    code=429,
    message='You have reached your request limit. Please try again later.'
))(blueprint)


''' Webargs Custom Error Handler '''

@parser.error_handler
def handle_error(err, req, schema, status_code, headers):
    abort(422, errors=dict(code=422, messages=err.messages), success=False)


from api.v1.endpoints import(
    auth,
    media,
    permission,
    role,
    user,
    organization,
    customfield,
    document,
    status,
    tracking,
    history
)
