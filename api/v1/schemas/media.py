import arrow

from marshmallow import (
    Schema, 
    fields
)
from flask import url_for
from api.utils import reverse_id

class ReverseId(fields.Field):
    def _serialize(self, value, attr, obj):
        if not isinstance(value, (int, long)):
            return value
        return reverse_id(value)


class MediaURLField(fields.Field):
    def _serialize(self, value, attr, obj):

        if obj.mimetype in ['image/jpeg', 'image/jpg', 'image/png']: 
            url = dict(
                original=url_for('static', filename='uploads/tmp/' + obj.filename, _external=True),
                small=url_for('images.crop', filename='uploads/tmp/' + obj.filename, width=150, height=150, quality=95, _external=True),
                medium=url_for('images.crop', filename='uploads/tmp/' + obj.filename, width=300, height=300, quality=95, _external=True), 
                large=url_for('images.crop', filename='uploads/tmp/' + obj.filename, width=1024, height=1024, quality=95, _external=True)
            )
        else:
            url = dict(
                original=url_for('static', filename='uploads/tmp/' + obj.filename, _external=True),
            )

        return url


class MediaTmpSchema(Schema):
    
    id = ReverseId(dump_only=True)
    original_filename = fields.Str()
    filename = fields.Str()
    size = fields.Str()
    mimetype = fields.Str()
    ismain = fields.Bool()
    url = MediaURLField(attribute='id')
    created = fields.DateTime()
    document = fields.Nested('DocumentSchema', attribute='media_document', exclude=('medias', 'custom_fields', 'person_involves', 'statuses', ))
