import arrow

from marshmallow import (
    Schema, 
    fields
)
from api.utils import (
    reverse_id, 
    denormalize_phone_number,
)

class ReverseId(fields.Field):
    def _serialize(self, value, attr, obj):
        if not isinstance(value, (int, long)):
            return value
        return reverse_id(value)

class DenormalizePhone(fields.Field):
    def _serialize(self, value, attr, obj):
        if value:
            return denormalize_phone_number(value)
        return value


class HistorySchema(Schema):
    id = ReverseId(dump_only=True)
    module_id = ReverseId(dump_only=True)
    module = fields.Str()
    action = fields.Str()
    content = fields.Dict()
    remote_address = fields.Str()
    user_agent = fields.Dict()
    created = fields.DateTime()
    updated = fields.DateTime()
    user = fields.Nested('UserSchema', attribute='history_user', exclude=('histories', 'role', ))

