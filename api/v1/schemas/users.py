import arrow

from marshmallow import (
    Schema, 
    fields
)
from api.utils import (
    reverse_id, 
    denormalize_phone_number,
)

class ReverseId(fields.Field):
    def _serialize(self, value, attr, obj):
        if not isinstance(value, (int, long)):
            return value
        return reverse_id(value)

class DenormalizePhone(fields.Field):
    def _serialize(self, value, attr, obj):
        if value:
            return denormalize_phone_number(value)
        return value


class PermissionSchema(Schema):
    id = ReverseId(dump_only=True)
    name = fields.Str()
    slug = fields.Str()
    description = fields.Str()
    created = fields.DateTime()
    updated = fields.DateTime()


class RoleSchema(Schema):
    id = ReverseId(dump_only=True)
    name = fields.Str()
    slug = fields.Str()
    description = fields.Str()
    permissions = fields.Dict()
    created = fields.DateTime()
    updated = fields.DateTime()
    total_user = fields.Method('count_total_user')
    
    def count_total_user(self, obj):
        return obj.users.count()


class OrganizationSchema(Schema):
    id = ReverseId(dump_only=True)
    code = fields.Str()
    name = fields.Str()
    cost_center = fields.Str()
    description = fields.Str()
    total_user = fields.Method('count_total_user')
    statuses = fields.Nested('StatusSchema', many=True, exclude=('organization', ))
    fields = fields.Nested('CustomFieldGroupSchema', many=True, exclude=('organization', ))


    def count_total_user(self, obj):
        return obj.users.count()



class UserSchema(Schema):
    id = ReverseId(dump_only=True)
    nid = fields.Str()
    fullname = fields.Str()
    badge = fields.Str()
    phone = DenormalizePhone()
    status = fields.Str()
    created = fields.DateTime()
    updated = fields.DateTime()
    role = fields.Nested('RoleSchema', attribute='user_role')
    organization = fields.Nested('OrganizationSchema', attribute='user_organization')

