import arrow

from marshmallow import (
    Schema, 
    fields
)
from api.utils import (
    reverse_id, 
    denormalize_phone_number,
)

class ReverseId(fields.Field):
    def _serialize(self, value, attr, obj):
        if not isinstance(value, (int, long)):
            return value
        return reverse_id(value)

class DenormalizePhone(fields.Field):
    def _serialize(self, value, attr, obj):
        if value:
            return denormalize_phone_number(value)
        return value

class CustomFieldGroupSchema(Schema):
    id = ReverseId(dump_only=True)
    name = fields.Str()
    slug = fields.Str()
    description = fields.Str()
    created = fields.DateTime()
    updated = fields.DateTime()
    organization = fields.Nested('OrganizationSchema', attribute='field_group_organization')
    fields = fields.Nested('CustomFieldSchema', many=True, exclude=('group', ))


class CustomFieldSchema(Schema):
    id = ReverseId(dump_only=True)
    name = fields.Str()
    slug = fields.Str()
    description = fields.Str()
    field = fields.Str()
    default = fields.Str()
    value = fields.Str()
    options = fields.Str()
    required = fields.Bool()
    created = fields.DateTime()
    updated = fields.DateTime()
    group = fields.Nested('CustomFieldGroupSchema', attribute='field_group', exclude=('fields', 'organization', ))


class DocumentsFieldsSchema(Schema):
    id = ReverseId(dump_only=True)
    value = fields.Str()
    documents = fields.Nested('DocumentSchema')
    fields = fields.Nested('CustomFieldSchema')


class DocumentSchema(Schema):
    id = ReverseId(dump_only=True)
    tracking_number = fields.Str()
    document_number = fields.Str()
    title = fields.Str()
    remarks = fields.Str()
    isarchived = fields.Bool()
    created = fields.DateTime()
    updated = fields.DateTime()
    author = fields.Nested('UserSchema', attribute='document_author', exclude=('role', 'organization.fields', 'organization.statuses', ))  
    custom_fields = fields.Nested('DocumentsFieldsSchema', attribute='documents_fields', many=True, exclude=('documents', ))
    medias = fields.Nested('MediaTmpSchema', many=True)
    person_involves = fields.Nested('PersonInvolvedSchema', many=True)
    statuses = fields.Nested('StatusTrackingSchema', many=True, exclude=('document', 'author.role'))


class PersonInvolvedSchema(Schema):
    id = ReverseId(dump_only=True)
    fullname = fields.Str()
    phone = DenormalizePhone()
    note = fields.Str()
    created = fields.DateTime()
    updated = fields.DateTime()


class StatusSchema(Schema):
    id = ReverseId(dump_only=True)
    name = fields.Str()
    slug = fields.Str()
    description = fields.Str()
    ispublic = fields.Bool()
    created = fields.DateTime()
    updated = fields.DateTime()
    organization = fields.Nested('OrganizationSchema', attribute='status_organization', exclude=('statuses', ))


class StatusTrackingSchema(Schema):
    id = ReverseId(dump_only=True)
    notified = fields.Bool()
    note = fields.Str()
    status = fields.Nested('StatusSchema')
    author = fields.Nested('UserSchema')
    document = fields.Nested('DocumentSchema', attribute='status_document', exclude=('author', 'custom_fields', 'medias', 'person_involves', 'statuses', ))
