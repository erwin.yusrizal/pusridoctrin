import os
import logging
#from werkzeug.contrib.profiler import ProfilerMiddleware

from flask import request
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager, Server, Shell

from api import db, create_app

from api.v1.models.histories import(
    HistoryModel
)
from api.v1.models.media import(
    MediaModel
)
from api.v1.models.users import(
    PermissionModel,
    RoleModel,
    OrganizationModel,
    UserModel
)
from api.v1.models.documents import(
    CustomFieldGroupModel,
    CustomFieldModel,
    DocumentModel,
    StatusModel,
    StatusTrackingModel,
    PersonInvolvedModel
)

app = create_app('default')
manager = Manager(app)
migrate = Migrate(app, db)

COV = None

if app.config.get('FLASK_COVERAGE'):
    import coverage
    COV = coverage.coverage(
        data_file=os.path.join(os.path.dirname(__file__), '.coverage'),
        branch=True, 
        include=os.path.join(os.path.dirname(__file__), 'api/*')
    )
    COV.start()

#app.config['PROFILE'] = True
#app.wsgi_app = ProfilerMiddleware(app.wsgi_app, restrictions=[30])

logger = logging.getLogger()
logger.addHandler(logging.StreamHandler())


@app.before_request
def option_autoreply():

    ''' Always return 200 on OPTIONS request '''

    if request.method == 'OPTIONS':
        resp = app.make_default_options_response()
        headers = None

        if 'ACCESS_CONTROL_REQUEST_HEADERS' in request.headers:
            headers = request.headers['ACCESS_CONTROL_REQUEST_HEADERS']

        h = resp.headers

        h['Access-Control-Allow-Credentials'] = "true"
        h['Access-Control-Allow-Origin'] = request.headers['Origin']
        h['Access-Control-Allow-Methods'] = request.headers['Access-Control-Request-Method']
        h['Access-Control-Max-Age'] = "10"

        if headers is not None:
            h['Access-Control-Allow-Headers'] = headers

        return resp



@app.after_request
def set_allow_origin(resp):

    ''' Set origin for GET, POST, PUT, DELETE requests '''

    h = resp.headers

    # Allow cross domain for other http verbs
    if request.method != 'OPTIONS' and 'Origin' in request.headers:
        h['Access-Control-Allow-Origin'] = request.headers['Origin']
        h['Cache-Control'] = 'no-cache, no-store, must-revalidate'
        h['Pragma'] = 'no-cache'
        h['Expires'] = "0"

    return resp


def shell_context():
    return dict(app=app, db=db)


manager.add_command('shell', Shell(make_context=shell_context))
manager.add_command('db', MigrateCommand)
manager.add_command('runserver', Server(
    threaded=True,
    use_reloader=True,
    use_debugger=True,
    host='api.doctrin',
    port=3000
))

@manager.command
def test(coverage=False):

    if coverage and not app.config.get('FLASK_COVERAGE'):
        app.config['FLASK_COVERAGE'] = True

    import unittest
    tests = unittest.TestLoader().discover('tests')
    unittest.TextTestRunner(verbosity=2).run(tests)

    if COV:
        COV.stop()
        COV.save()
        print('Coverage Summary:')
        COV.report()

        basedir = os.path.abspath(os.path.dirname(__file__))
        covdir = os.path.join(basedir, 'tmp/coverage')
        COV.html_report(directory=covdir)
        print('HTML verison: file://%s/index.html' % covdir)
        COV.erase()


if __name__ == '__main__':
    manager.run()

