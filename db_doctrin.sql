-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 27, 2019 at 02:38 PM
-- Server version: 10.1.40-MariaDB-0ubuntu0.18.04.1
-- PHP Version: 7.2.19-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_doctrin`
--

-- --------------------------------------------------------

--
-- Table structure for table `alembic_version`
--

CREATE TABLE `alembic_version` (
  `version_num` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alembic_version`
--

INSERT INTO `alembic_version` (`version_num`) VALUES
('96ec4f1cc25a');

-- --------------------------------------------------------

--
-- Table structure for table `doctrin_documents`
--

CREATE TABLE `doctrin_documents` (
  `id` int(11) NOT NULL,
  `tracking_number` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `document_number` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci,
  `isarchived` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `author_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `doctrin_documents`
--

INSERT INTO `doctrin_documents` (`id`, `tracking_number`, `document_number`, `title`, `remarks`, `isarchived`, `created`, `updated`, `author_id`) VALUES
(1, 'Z69671CTGVEG49XK', 'INV-1234567890', 'Lorem ipsum dolor sit amet consectus', 'Paleo javanicus brutus coconus', 0, '2019-08-22 01:24:24', '2019-08-22 01:24:24', 1),
(2, 'PK1O60K0NSOGOEO5', 'SPD1298000', 'Surat Perjalanan Dinas', 'Palembang - Surabaya 5 Hari kerja', 0, '2019-08-26 14:18:09', '2019-08-26 14:18:09', 1);

-- --------------------------------------------------------

--
-- Table structure for table `doctrin_documents_fields`
--

CREATE TABLE `doctrin_documents_fields` (
  `id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `field_id` int(11) NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `doctrin_documents_fields`
--

INSERT INTO `doctrin_documents_fields` (`id`, `document_id`, `field_id`, `value`, `created`, `updated`) VALUES
(1, 1, 9, 'Mas Jono Sukerto', '2019-08-26 09:08:23', '2019-08-26 09:08:23'),
(2, 1, 10, 'Makan\nMinum\nOlah Raga\nIbadah', '2019-08-26 09:08:23', '2019-08-26 09:08:23'),
(3, 1, 14, 'Joni', '2019-08-26 09:08:23', '2019-08-26 09:08:23'),
(4, 1, 15, '9200000', '2019-08-26 09:08:23', '2019-08-26 09:08:23'),
(5, 1, 16, 'Apa aja', '2019-08-26 09:08:23', '2019-08-26 09:08:23'),
(6, 2, 22, '2019-08-27T17:00:00.000Z', '2019-08-26 14:18:10', '2019-08-26 14:18:10');

-- --------------------------------------------------------

--
-- Table structure for table `doctrin_documents_person_involves`
--

CREATE TABLE `doctrin_documents_person_involves` (
  `document_id` int(11) NOT NULL,
  `person_involve_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `doctrin_documents_person_involves`
--

INSERT INTO `doctrin_documents_person_involves` (`document_id`, `person_involve_id`) VALUES
(1, 1),
(1, 2),
(2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `doctrin_fields`
--

CREATE TABLE `doctrin_fields` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `field` enum('text','email','number','textarea','select','checkbox','radio','switch','file','datepicker','timepicker','datetimepicker') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default` text COLLATE utf8mb4_unicode_ci,
  `value` text COLLATE utf8mb4_unicode_ci,
  `options` text COLLATE utf8mb4_unicode_ci,
  `required` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `doctrin_fields`
--

INSERT INTO `doctrin_fields` (`id`, `name`, `slug`, `description`, `field`, `default`, `value`, `options`, `required`, `created`, `updated`, `group_id`) VALUES
(9, 'Nama Karyawan', 'nama-karyawan', ' Nama lengkap karyawan', 'text', '', NULL, '', 1, '2019-08-25 15:39:35', '2019-08-25 15:39:35', 2),
(10, 'Aktifitas', 'aktifitas', 'Pilih aktifitas, boleh lebih dari satu', 'checkbox', '', NULL, 'Makan\nMinum\nOlah Raga\nIbadah', 1, '2019-08-25 15:39:35', '2019-08-25 15:39:35', 2),
(14, 'Nama Lengkap', 'nama-lengkap', '', 'text', '', NULL, '', 1, '2019-08-26 08:49:32', '2019-08-26 08:49:32', 3),
(15, 'Badge', 'badge', '', 'number', '', NULL, '', 1, '2019-08-26 08:49:32', '2019-08-26 08:49:32', 3),
(16, 'Jabatan', 'jabatan', '', 'text', '', NULL, '', 1, '2019-08-26 08:49:32', '2019-08-26 08:49:32', 3),
(18, 'Tanggal Kegiatan', 'tanggal-kegiatan', '', 'datepicker', '', NULL, '', 1, '2019-08-26 13:33:46', '2019-08-26 13:33:46', 5),
(19, 'Waktu Kegiatan', 'waktu-kegiatan', '', 'timepicker', '', NULL, '', 1, '2019-08-26 13:33:46', '2019-08-26 13:33:46', 5),
(20, 'Tanggal & Waktu', 'tanggal-waktu', '', 'datetimepicker', '', NULL, '', 1, '2019-08-26 13:33:46', '2019-08-26 13:33:46', 5),
(22, 'Tanggal Lahir', 'tanggal-lahir', 'Pilih Tanggal', 'datepicker', '', NULL, '', 1, '2019-08-26 13:46:26', '2019-08-26 13:46:26', 4),
(23, 'Jenis Mobil', 'jenis-mobil', 'Jenis mobil favoritmu', 'select', '', NULL, 'Honda\nSuzuki\nToyota\nMitsubishi', 1, '2019-08-26 13:56:23', '2019-08-26 13:56:23', 6),
(24, 'Yes or No', 'yes-or-no', '', 'radio', '', NULL, 'Iya\nTidak', 1, '2019-08-26 13:56:23', '2019-08-26 13:56:23', 6);

-- --------------------------------------------------------

--
-- Table structure for table `doctrin_fields_groups`
--

CREATE TABLE `doctrin_fields_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `organization_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `doctrin_fields_groups`
--

INSERT INTO `doctrin_fields_groups` (`id`, `name`, `slug`, `description`, `created`, `updated`, `organization_id`) VALUES
(2, 'Aktifitas Karyawan', 'aktifitas-karyawan', NULL, '2019-08-25 04:16:17', '2019-08-25 13:08:33', 1),
(3, 'Penanggung Jawab', 'penanggung-jawab', NULL, '2019-08-26 01:06:33', '2019-08-26 01:06:33', 1),
(4, 'Kalender', 'kalender', NULL, '2019-08-26 13:32:37', '2019-08-26 13:32:37', 1),
(5, 'Tanggal dan Waktu', 'tanggal-dan-waktu', NULL, '2019-08-26 13:33:46', '2019-08-26 13:33:46', 1),
(6, 'Pilih Salah Satu', 'pilih-salah-satu', NULL, '2019-08-26 13:35:54', '2019-08-26 13:35:54', 1);

-- --------------------------------------------------------

--
-- Table structure for table `doctrin_histories`
--

CREATE TABLE `doctrin_histories` (
  `id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `module` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `remote_address` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `doctrin_histories`
--

INSERT INTO `doctrin_histories` (`id`, `module_id`, `module`, `action`, `content`, `remote_address`, `user_agent`, `created`, `updated`, `user_id`) VALUES
(1, 1, 'User', 'Logout', '{}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-24 16:29:25', '2019-08-24 16:29:25', 1),
(2, 1, 'User', 'Login', '{}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-24 16:29:43', '2019-08-24 16:29:43', 1),
(3, 1, 'Status', 'Create', '{\"organization_id\": {\"to\": \"[7]\", \"from\": \"()\"}, \"description\": {\"to\": \"[None]\", \"from\": \"()\"}, \"ispublic\": {\"to\": \"[True]\", \"from\": \"()\"}, \"name\": {\"to\": \"[u\'Diterima Sekretariat Perusahaan\']\", \"from\": \"()\"}, \"slug\": {\"to\": \"[\'diterima-sekretariat-perusahaan\']\", \"from\": \"()\"}}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-24 16:44:12', '2019-08-24 16:44:12', 1),
(4, 1, 'Status', 'Edit', '{\"name\": {\"to\": \"[u\'Diterima Sekper\']\", \"from\": \"[u\'Diterima Sekretariat Perusahaan\']\"}, \"slug\": {\"to\": \"[\'diterima-sekper\']\", \"from\": \"[u\'diterima-sekretariat-perusahaan\']\"}}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-24 16:56:12', '2019-08-24 16:56:12', 1),
(5, 2, 'Status', 'Create', '{\"organization_id\": {\"to\": \"[4]\", \"from\": \"()\"}, \"description\": {\"to\": \"[u\'Test\']\", \"from\": \"()\"}, \"ispublic\": {\"to\": \"[False]\", \"from\": \"()\"}, \"name\": {\"to\": \"[u\'Test\']\", \"from\": \"()\"}, \"slug\": {\"to\": \"[\'test\']\", \"from\": \"()\"}}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-24 17:25:22', '2019-08-24 17:25:22', 1),
(6, 3, 'Status', 'Create', '{\"organization_id\": {\"to\": \"[6]\", \"from\": \"()\"}, \"description\": {\"to\": \"[None]\", \"from\": \"()\"}, \"ispublic\": {\"to\": \"[False]\", \"from\": \"()\"}, \"name\": {\"to\": \"[u\'Double Test\']\", \"from\": \"()\"}, \"slug\": {\"to\": \"[\'double-test\']\", \"from\": \"()\"}}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-24 17:46:56', '2019-08-24 17:46:56', 1),
(7, 4, 'Status', 'Create', '{\"organization_id\": {\"to\": \"[4]\", \"from\": \"()\"}, \"description\": {\"to\": \"[u\'sss\']\", \"from\": \"()\"}, \"ispublic\": {\"to\": \"[False]\", \"from\": \"()\"}, \"name\": {\"to\": \"[u\'ssss\']\", \"from\": \"()\"}, \"slug\": {\"to\": \"[\'ssss\']\", \"from\": \"()\"}}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-24 18:18:29', '2019-08-24 18:18:29', 1),
(8, 5, 'Status', 'Create', '{\"organization_id\": {\"to\": \"[7]\", \"from\": \"()\"}, \"description\": {\"to\": \"[u\'dsafasfasfa\']\", \"from\": \"()\"}, \"ispublic\": {\"to\": \"[False]\", \"from\": \"()\"}, \"name\": {\"to\": \"[u\'Jono\']\", \"from\": \"()\"}, \"slug\": {\"to\": \"[\'jono\']\", \"from\": \"()\"}}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-24 18:22:11', '2019-08-24 18:22:11', 1),
(9, 6, 'Status', 'Create', '{\"organization_id\": {\"to\": \"[7]\", \"from\": \"()\"}, \"description\": {\"to\": \"[None]\", \"from\": \"()\"}, \"ispublic\": {\"to\": \"[False]\", \"from\": \"()\"}, \"name\": {\"to\": \"[u\'gggggg\']\", \"from\": \"()\"}, \"slug\": {\"to\": \"[\'gggggg\']\", \"from\": \"()\"}}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-24 18:24:29', '2019-08-24 18:24:29', 1),
(10, 7, 'Status', 'Create', '{\"organization_id\": {\"to\": \"[7]\", \"from\": \"()\"}, \"description\": {\"to\": \"[None]\", \"from\": \"()\"}, \"ispublic\": {\"to\": \"[False]\", \"from\": \"()\"}, \"name\": {\"to\": \"[u\'apobae\']\", \"from\": \"()\"}, \"slug\": {\"to\": \"[\'apobae\']\", \"from\": \"()\"}}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-24 18:25:50', '2019-08-24 18:25:50', 1),
(12, 8, 'Status', 'Create', '{\"organization_id\": {\"to\": \"[4]\", \"from\": \"()\"}, \"description\": {\"to\": \"[None]\", \"from\": \"()\"}, \"ispublic\": {\"to\": \"[False]\", \"from\": \"()\"}, \"name\": {\"to\": \"[u\'Jono Sukerto\']\", \"from\": \"()\"}, \"slug\": {\"to\": \"[\'jono-sukerto\']\", \"from\": \"()\"}}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-24 19:16:03', '2019-08-24 19:16:03', 1),
(13, 8, 'Status', 'Delete', '{\"updated\": \"2019-08-24 19:16:03\", \"description\": \"None\", \"created\": \"2019-08-24 19:16:03\", \"ispublic\": \"False\", \"id\": \"8\", \"organization_id\": \"4\", \"slug\": \"jono-sukerto\", \"name\": \"Jono Sukerto\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-24 19:16:16', '2019-08-24 19:16:16', 1),
(14, 9, 'Status', 'Create', '{\"updated\": \"2019-08-24 19:18:36.666272\", \"description\": \"None\", \"created\": \"2019-08-24 19:18:36.666264\", \"ispublic\": \"False\", \"id\": \"9\", \"organization_id\": \"7\", \"slug\": \"diterima-di-sekper\", \"name\": \"Diterima Di Sekper\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-24 19:18:36', '2019-08-24 19:18:36', 1),
(15, 1, 'CustomFieldGroup', 'Create', '{\"updated\": \"2019-08-25 02:06:45.682748\", \"description\": \"Custom field for sekretaris perusahaan\", \"created\": \"2019-08-25 02:06:45.682717\", \"id\": \"1\", \"organization_id\": \"7\", \"slug\": \"field-sekper\", \"name\": \"Field Sekper\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": null, \"patch_minor\": null, \"minor\": null, \"family\": \"Other\", \"patch\": null}, \"user_agent\": {\"major\": null, \"minor\": null, \"family\": \"Other\", \"patch\": null}, \"string\": \"PostmanRuntime/7.15.2\"}', '2019-08-25 02:06:45', '2019-08-25 02:06:45', 1),
(16, 1, 'CustomField', 'Create', '{\"updated\": \"2019-08-25 02:06:45.727934\", \"description\": \"Nama lengkap sekper\", \"created\": \"2019-08-25 02:06:45.727914\", \"default\": \"None\", \"required\": \"True\", \"value\": \"None\", \"id\": \"1\", \"field\": \"text\", \"options\": \"None\", \"group_id\": \"1\", \"slug\": \"nama-lengkap\", \"name\": \"Nama Lengkap\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": null, \"patch_minor\": null, \"minor\": null, \"family\": \"Other\", \"patch\": null}, \"user_agent\": {\"major\": null, \"minor\": null, \"family\": \"Other\", \"patch\": null}, \"string\": \"PostmanRuntime/7.15.2\"}', '2019-08-25 02:06:45', '2019-08-25 02:06:45', 1),
(17, 2, 'CustomField', 'Create', '{\"updated\": \"2019-08-25 02:06:45.744705\", \"description\": \"Hobi sekper\", \"created\": \"2019-08-25 02:06:45.744689\", \"default\": \"0|Pilih Hobi\", \"required\": \"True\", \"value\": \"None\", \"id\": \"2\", \"field\": \"select\", \"options\": \"makan|Makan\\nminum|Minum\", \"group_id\": \"1\", \"slug\": \"hobi\", \"name\": \"Hobi\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": null, \"patch_minor\": null, \"minor\": null, \"family\": \"Other\", \"patch\": null}, \"user_agent\": {\"major\": null, \"minor\": null, \"family\": \"Other\", \"patch\": null}, \"string\": \"PostmanRuntime/7.15.2\"}', '2019-08-25 02:06:45', '2019-08-25 02:06:45', 1),
(18, 2, 'CustomFieldGroup', 'Create', '{\"updated\": \"2019-08-25 04:16:17.588581\", \"description\": \"None\", \"created\": \"2019-08-25 04:16:17.588557\", \"id\": \"2\", \"organization_id\": \"4\", \"slug\": \"aktifitas-karyawan\", \"name\": \"Aktifitas Karyawan\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-25 04:16:17', '2019-08-25 04:16:17', 1),
(19, 3, 'CustomField', 'Create', '{\"updated\": \"2019-08-25 04:16:17.678168\", \"description\": \"\", \"created\": \"2019-08-25 04:16:17.678138\", \"default\": \"\", \"required\": \"True\", \"value\": \"None\", \"id\": \"3\", \"field\": \"text\", \"options\": \"\", \"group_id\": \"2\", \"slug\": \"nama-karyawan\", \"name\": \"Nama Karyawan\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-25 04:16:17', '2019-08-25 04:16:17', 1),
(20, 4, 'CustomField', 'Create', '{\"updated\": \"2019-08-25 04:16:17.698203\", \"description\": \"\", \"created\": \"2019-08-25 04:16:17.698180\", \"default\": \"\", \"required\": \"True\", \"value\": \"None\", \"id\": \"4\", \"field\": \"checkbox\", \"options\": \"makan|Makan\\nminum|Minum\\nolahraga|Olah Raga\\nibadah|Ibadah\", \"group_id\": \"2\", \"slug\": \"aktifitas\", \"name\": \"Aktifitas\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-25 04:16:17', '2019-08-25 04:16:17', 1),
(21, 3, 'CustomField', 'Delete', '{\"updated\": \"2019-08-25 04:16:17\", \"description\": \"\", \"created\": \"2019-08-25 04:16:17\", \"default\": \"\", \"required\": \"True\", \"value\": \"None\", \"id\": \"3\", \"field\": \"text\", \"options\": \"\", \"group_id\": \"2\", \"slug\": \"nama-karyawan\", \"name\": \"Nama Karyawan\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-25 04:18:51', '2019-08-25 04:18:51', 1),
(22, 4, 'CustomField', 'Delete', '{\"updated\": \"2019-08-25 04:16:17\", \"description\": \"\", \"created\": \"2019-08-25 04:16:17\", \"default\": \"\", \"required\": \"True\", \"value\": \"None\", \"id\": \"4\", \"field\": \"checkbox\", \"options\": \"makan|Makan\\nminum|Minum\\nolahraga|Olah Raga\\nibadah|Ibadah\", \"group_id\": \"2\", \"slug\": \"aktifitas\", \"name\": \"Aktifitas\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-25 04:18:51', '2019-08-25 04:18:51', 1),
(23, 5, 'CustomField', 'Create', '{\"updated\": \"2019-08-25 04:18:51.300924\", \"description\": \" Nama lengkap karyawan\", \"created\": \"2019-08-25 04:18:51.300914\", \"default\": \"\", \"required\": \"True\", \"value\": \"None\", \"id\": \"5\", \"field\": \"text\", \"options\": \"\", \"group_id\": \"2\", \"slug\": \"nama-karyawan\", \"name\": \"Nama Karyawan\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-25 04:18:51', '2019-08-25 04:18:51', 1),
(24, 6, 'CustomField', 'Create', '{\"updated\": \"2019-08-25 04:18:51.312198\", \"description\": \"Pilih aktifitas, boleh lebih dari satu\", \"created\": \"2019-08-25 04:18:51.312183\", \"default\": \"\", \"required\": \"True\", \"value\": \"None\", \"id\": \"6\", \"field\": \"checkbox\", \"options\": \"makan|Makan\\nminum|Minum\\nolahraga|Olah Raga\\nibadah|Ibadah\", \"group_id\": \"2\", \"slug\": \"aktifitas\", \"name\": \"Aktifitas\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-25 04:18:51', '2019-08-25 04:18:51', 1),
(25, 1, 'CustomField', 'Delete', '{\"updated\": \"2019-08-25 02:06:45\", \"description\": \"Nama lengkap sekper\", \"created\": \"2019-08-25 02:06:45\", \"default\": \"None\", \"required\": \"True\", \"value\": \"None\", \"id\": \"1\", \"field\": \"text\", \"options\": \"None\", \"group_id\": \"1\", \"slug\": \"nama-lengkap\", \"name\": \"Nama Lengkap\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-25 04:25:46', '2019-08-25 04:25:46', 1),
(26, 2, 'CustomField', 'Delete', '{\"updated\": \"2019-08-25 02:06:45\", \"description\": \"Hobi sekper\", \"created\": \"2019-08-25 02:06:45\", \"default\": \"0|Pilih Hobi\", \"required\": \"True\", \"value\": \"None\", \"id\": \"2\", \"field\": \"select\", \"options\": \"makan|Makan\\nminum|Minum\", \"group_id\": \"1\", \"slug\": \"hobi\", \"name\": \"Hobi\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-25 04:25:46', '2019-08-25 04:25:46', 1),
(27, 1, 'CustomFieldGroup', 'Delete', '{\"updated\": \"2019-08-25 02:06:45\", \"description\": \"Custom field for sekretaris perusahaan\", \"created\": \"2019-08-25 02:06:45\", \"id\": \"1\", \"organization_id\": \"7\", \"slug\": \"field-sekper\", \"name\": \"Field Sekper\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-25 04:25:46', '2019-08-25 04:25:46', 1),
(28, 2, 'CustomFieldGroup', 'Edit', '{\"organization_id\": {\"to\": \"[1]\", \"from\": \"[4]\"}}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-25 13:08:33', '2019-08-25 13:08:33', 1),
(29, 5, 'CustomField', 'Delete', '{\"updated\": \"2019-08-25 04:18:51\", \"description\": \" Nama lengkap karyawan\", \"created\": \"2019-08-25 04:18:51\", \"default\": \"\", \"required\": \"True\", \"value\": \"None\", \"id\": \"5\", \"field\": \"text\", \"options\": \"\", \"group_id\": \"2\", \"slug\": \"nama-karyawan\", \"name\": \"Nama Karyawan\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-25 13:08:33', '2019-08-25 13:08:33', 1),
(30, 6, 'CustomField', 'Delete', '{\"updated\": \"2019-08-25 04:18:51\", \"description\": \"Pilih aktifitas, boleh lebih dari satu\", \"created\": \"2019-08-25 04:18:51\", \"default\": \"\", \"required\": \"True\", \"value\": \"None\", \"id\": \"6\", \"field\": \"checkbox\", \"options\": \"makan|Makan\\nminum|Minum\\nolahraga|Olah Raga\\nibadah|Ibadah\", \"group_id\": \"2\", \"slug\": \"aktifitas\", \"name\": \"Aktifitas\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-25 13:08:33', '2019-08-25 13:08:33', 1),
(31, 7, 'CustomField', 'Create', '{\"updated\": \"2019-08-25 13:08:33.800450\", \"description\": \" Nama lengkap karyawan\", \"created\": \"2019-08-25 13:08:33.800444\", \"default\": \"\", \"required\": \"True\", \"value\": \"None\", \"id\": \"7\", \"field\": \"text\", \"options\": \"\", \"group_id\": \"2\", \"slug\": \"nama-karyawan\", \"name\": \"Nama Karyawan\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-25 13:08:33', '2019-08-25 13:08:33', 1),
(32, 8, 'CustomField', 'Create', '{\"updated\": \"2019-08-25 13:08:33.815435\", \"description\": \"Pilih aktifitas, boleh lebih dari satu\", \"created\": \"2019-08-25 13:08:33.815430\", \"default\": \"\", \"required\": \"True\", \"value\": \"None\", \"id\": \"8\", \"field\": \"checkbox\", \"options\": \"makan|Makan\\nminum|Minum\\nolahraga|Olah Raga\\nibadah|Ibadah\", \"group_id\": \"2\", \"slug\": \"aktifitas\", \"name\": \"Aktifitas\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-25 13:08:33', '2019-08-25 13:08:33', 1),
(33, 7, 'CustomField', 'Delete', '{\"updated\": \"2019-08-25 13:08:33\", \"description\": \" Nama lengkap karyawan\", \"created\": \"2019-08-25 13:08:33\", \"default\": \"\", \"required\": \"True\", \"value\": \"None\", \"id\": \"7\", \"field\": \"text\", \"options\": \"\", \"group_id\": \"2\", \"slug\": \"nama-karyawan\", \"name\": \"Nama Karyawan\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-25 15:39:35', '2019-08-25 15:39:35', 1),
(34, 8, 'CustomField', 'Delete', '{\"updated\": \"2019-08-25 13:08:33\", \"description\": \"Pilih aktifitas, boleh lebih dari satu\", \"created\": \"2019-08-25 13:08:33\", \"default\": \"\", \"required\": \"True\", \"value\": \"None\", \"id\": \"8\", \"field\": \"checkbox\", \"options\": \"makan|Makan\\nminum|Minum\\nolahraga|Olah Raga\\nibadah|Ibadah\", \"group_id\": \"2\", \"slug\": \"aktifitas\", \"name\": \"Aktifitas\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-25 15:39:35', '2019-08-25 15:39:35', 1),
(35, 9, 'CustomField', 'Create', '{\"updated\": \"2019-08-25 15:39:35.489477\", \"description\": \" Nama lengkap karyawan\", \"created\": \"2019-08-25 15:39:35.489470\", \"default\": \"\", \"required\": \"True\", \"value\": \"None\", \"id\": \"9\", \"field\": \"text\", \"options\": \"\", \"group_id\": \"2\", \"slug\": \"nama-karyawan\", \"name\": \"Nama Karyawan\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-25 15:39:35', '2019-08-25 15:39:35', 1),
(36, 10, 'CustomField', 'Create', '{\"updated\": \"2019-08-25 15:39:35.497424\", \"description\": \"Pilih aktifitas, boleh lebih dari satu\", \"created\": \"2019-08-25 15:39:35.497418\", \"default\": \"\", \"required\": \"True\", \"value\": \"None\", \"id\": \"10\", \"field\": \"checkbox\", \"options\": \"Makan\\nMinum\\nOlah Raga\\nIbadah\", \"group_id\": \"2\", \"slug\": \"aktifitas\", \"name\": \"Aktifitas\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-25 15:39:35', '2019-08-25 15:39:35', 1),
(37, 3, 'CustomFieldGroup', 'Create', '{\"updated\": \"2019-08-26 01:06:33.185467\", \"description\": \"None\", \"created\": \"2019-08-26 01:06:33.185458\", \"id\": \"3\", \"organization_id\": \"1\", \"slug\": \"penanggung-jawab\", \"name\": \"Penanggung Jawab\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 01:06:33', '2019-08-26 01:06:33', 1),
(38, 11, 'CustomField', 'Create', '{\"updated\": \"2019-08-26 01:06:33.215175\", \"description\": \"\", \"created\": \"2019-08-26 01:06:33.215168\", \"default\": \"\", \"required\": \"True\", \"value\": \"None\", \"id\": \"11\", \"field\": \"text\", \"options\": \"\", \"group_id\": \"3\", \"slug\": \"nama-lengkap\", \"name\": \"Nama Lengkap\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 01:06:33', '2019-08-26 01:06:33', 1),
(39, 12, 'CustomField', 'Create', '{\"updated\": \"2019-08-26 01:06:33.223975\", \"description\": \"\", \"created\": \"2019-08-26 01:06:33.223969\", \"default\": \"\", \"required\": \"True\", \"value\": \"None\", \"id\": \"12\", \"field\": \"text\", \"options\": \"\", \"group_id\": \"3\", \"slug\": \"badge\", \"name\": \"Badge\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 01:06:33', '2019-08-26 01:06:33', 1),
(40, 13, 'CustomField', 'Create', '{\"updated\": \"2019-08-26 01:06:33.230211\", \"description\": \"\", \"created\": \"2019-08-26 01:06:33.230205\", \"default\": \"\", \"required\": \"True\", \"value\": \"None\", \"id\": \"13\", \"field\": \"text\", \"options\": \"\", \"group_id\": \"3\", \"slug\": \"jabatan\", \"name\": \"Jabatan\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 01:06:33', '2019-08-26 01:06:33', 1),
(41, 10, 'Status', 'Create', '{\"updated\": \"2019-08-26 06:49:54.023327\", \"description\": \"None\", \"created\": \"2019-08-26 06:49:54.023319\", \"ispublic\": \"True\", \"id\": \"10\", \"organization_id\": \"1\", \"slug\": \"diterima-direktur-utama\", \"name\": \"Diterima Direktur Utama\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 06:49:54', '2019-08-26 06:49:54', 1),
(42, 1, 'Media', 'Create', '{\"mimetype\": \"application/vnd.openxmlformats-officedocument.wordprocessingml.document\", \"updated\": \"2019-08-26 07:10:26.451148\", \"created\": \"2019-08-26 07:10:26.451140\", \"isorphan\": \"False\", \"filename\": \"0cddd9cb-4328-4b2f-929d-1e1764efa64b.docx\", \"ismain\": \"False\", \"lookup\": \"None\", \"original_filename\": \"What_is_hate_speech.docx\", \"expired\": \"None\", \"id\": \"1\", \"document_id\": \"1\", \"size\": \"13593\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 07:10:26', '2019-08-26 07:10:26', 1),
(43, 2, 'Media', 'Create', '{\"mimetype\": \"application/pdf\", \"updated\": \"2019-08-26 07:26:50.624279\", \"created\": \"2019-08-26 07:26:50.624271\", \"isorphan\": \"False\", \"filename\": \"7632610f-0139-4e03-ba02-7fb548c80c31.pdf\", \"ismain\": \"False\", \"lookup\": \"None\", \"original_filename\": \"25883897.pdf\", \"expired\": \"None\", \"id\": \"2\", \"document_id\": \"1\", \"size\": \"2481972\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 07:26:50', '2019-08-26 07:26:50', 1),
(44, 3, 'Media', 'Create', '{\"mimetype\": \"application/vnd.openxmlformats-officedocument.wordprocessingml.document\", \"updated\": \"2019-08-26 07:41:15.096056\", \"created\": \"2019-08-26 07:41:15.096047\", \"isorphan\": \"False\", \"filename\": \"221d0ac7-b70d-411e-9f83-cb8860a82f04.docx\", \"ismain\": \"False\", \"lookup\": \"None\", \"original_filename\": \"What_is_hate_speech.docx\", \"expired\": \"None\", \"id\": \"3\", \"document_id\": \"1\", \"size\": \"13593\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 07:41:15', '2019-08-26 07:41:15', 1),
(45, 1, 'User', 'Edit', '{\"organization_id\": {\"to\": \"[1]\", \"from\": \"[None]\"}}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 07:42:34', '2019-08-26 07:42:34', 1),
(46, 4, 'Media', 'Create', '{\"mimetype\": \"image/png\", \"updated\": \"2019-08-26 07:43:37.107776\", \"created\": \"2019-08-26 07:43:37.107759\", \"isorphan\": \"False\", \"filename\": \"8cf53ccb-dde7-407d-aa5a-ebce52be95aa.PNG\", \"ismain\": \"False\", \"lookup\": \"None\", \"original_filename\": \"Capture.PNG\", \"expired\": \"None\", \"id\": \"4\", \"document_id\": \"1\", \"size\": \"88038\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 07:43:37', '2019-08-26 07:43:37', 1),
(47, 5, 'Media', 'Create', '{\"mimetype\": \"image/png\", \"updated\": \"2019-08-26 07:44:36.193025\", \"created\": \"2019-08-26 07:44:36.193018\", \"isorphan\": \"False\", \"filename\": \"14f96d38-2d8c-4ea8-b15b-e116a5354617.PNG\", \"ismain\": \"False\", \"lookup\": \"None\", \"original_filename\": \"Capture.PNG\", \"expired\": \"None\", \"id\": \"5\", \"document_id\": \"1\", \"size\": \"88038\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 07:44:36', '2019-08-26 07:44:36', 1),
(48, 5, 'Media', 'Delete', '{\"mimetype\": \"image/png\", \"updated\": \"2019-08-26 07:44:36\", \"created\": \"2019-08-26 07:44:36\", \"isorphan\": \"False\", \"filename\": \"14f96d38-2d8c-4ea8-b15b-e116a5354617.PNG\", \"ismain\": \"False\", \"lookup\": \"None\", \"original_filename\": \"Capture.PNG\", \"expired\": \"None\", \"id\": \"5\", \"document_id\": \"1\", \"size\": \"88038\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 07:46:45', '2019-08-26 07:46:45', 1),
(49, 4, 'Media', 'Delete', '{\"mimetype\": \"image/png\", \"updated\": \"2019-08-26 07:43:37\", \"created\": \"2019-08-26 07:43:37\", \"isorphan\": \"False\", \"filename\": \"8cf53ccb-dde7-407d-aa5a-ebce52be95aa.PNG\", \"ismain\": \"False\", \"lookup\": \"None\", \"original_filename\": \"Capture.PNG\", \"expired\": \"None\", \"id\": \"4\", \"document_id\": \"1\", \"size\": \"88038\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 07:55:08', '2019-08-26 07:55:08', 1),
(50, 3, 'Media', 'Delete', '{\"mimetype\": \"application/vnd.openxmlformats-officedocument.word\", \"updated\": \"2019-08-26 07:41:15\", \"created\": \"2019-08-26 07:41:15\", \"isorphan\": \"False\", \"filename\": \"221d0ac7-b70d-411e-9f83-cb8860a82f04.docx\", \"ismain\": \"False\", \"lookup\": \"None\", \"original_filename\": \"What_is_hate_speech.docx\", \"expired\": \"None\", \"id\": \"3\", \"document_id\": \"1\", \"size\": \"13593\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 08:00:33', '2019-08-26 08:00:33', 1),
(51, 2, 'Media', 'Delete', '{\"mimetype\": \"application/pdf\", \"updated\": \"2019-08-26 07:26:50\", \"created\": \"2019-08-26 07:26:50\", \"isorphan\": \"False\", \"filename\": \"7632610f-0139-4e03-ba02-7fb548c80c31.pdf\", \"ismain\": \"False\", \"lookup\": \"None\", \"original_filename\": \"25883897.pdf\", \"expired\": \"None\", \"id\": \"2\", \"document_id\": \"1\", \"size\": \"2481972\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 08:00:59', '2019-08-26 08:00:59', 1),
(52, 1, 'Media', 'Delete', '{\"mimetype\": \"application/vnd.openxmlformats-officedocument.word\", \"updated\": \"2019-08-26 07:10:26\", \"created\": \"2019-08-26 07:10:26\", \"isorphan\": \"False\", \"filename\": \"0cddd9cb-4328-4b2f-929d-1e1764efa64b.docx\", \"ismain\": \"False\", \"lookup\": \"None\", \"original_filename\": \"What_is_hate_speech.docx\", \"expired\": \"None\", \"id\": \"1\", \"document_id\": \"1\", \"size\": \"13593\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 08:01:03', '2019-08-26 08:01:03', 1),
(53, 6, 'Media', 'Create', '{\"mimetype\": \"application/vnd.openxmlformats-officedocument.wordprocessingml.document\", \"updated\": \"2019-08-26 08:01:50.900277\", \"created\": \"2019-08-26 08:01:50.900268\", \"isorphan\": \"False\", \"filename\": \"392a9f7f-25fd-4e02-94c6-1967cf14ea00.docx\", \"ismain\": \"False\", \"lookup\": \"None\", \"original_filename\": \"What_is_hate_speech.docx\", \"expired\": \"None\", \"id\": \"6\", \"document_id\": \"1\", \"size\": \"13593\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 08:01:50', '2019-08-26 08:01:50', 1),
(54, 11, 'CustomField', 'Delete', '{\"updated\": \"2019-08-26 01:06:33\", \"description\": \"\", \"created\": \"2019-08-26 01:06:33\", \"default\": \"\", \"required\": \"True\", \"value\": \"None\", \"id\": \"11\", \"field\": \"text\", \"options\": \"\", \"group_id\": \"3\", \"slug\": \"nama-lengkap\", \"name\": \"Nama Lengkap\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 08:49:32', '2019-08-26 08:49:32', 1),
(55, 12, 'CustomField', 'Delete', '{\"updated\": \"2019-08-26 01:06:33\", \"description\": \"\", \"created\": \"2019-08-26 01:06:33\", \"default\": \"\", \"required\": \"True\", \"value\": \"None\", \"id\": \"12\", \"field\": \"text\", \"options\": \"\", \"group_id\": \"3\", \"slug\": \"badge\", \"name\": \"Badge\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 08:49:32', '2019-08-26 08:49:32', 1),
(56, 13, 'CustomField', 'Delete', '{\"updated\": \"2019-08-26 01:06:33\", \"description\": \"\", \"created\": \"2019-08-26 01:06:33\", \"default\": \"\", \"required\": \"True\", \"value\": \"None\", \"id\": \"13\", \"field\": \"text\", \"options\": \"\", \"group_id\": \"3\", \"slug\": \"jabatan\", \"name\": \"Jabatan\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 08:49:32', '2019-08-26 08:49:32', 1),
(57, 14, 'CustomField', 'Create', '{\"updated\": \"2019-08-26 08:49:32.880896\", \"description\": \"\", \"created\": \"2019-08-26 08:49:32.880869\", \"default\": \"\", \"required\": \"True\", \"value\": \"None\", \"id\": \"14\", \"field\": \"text\", \"options\": \"\", \"group_id\": \"3\", \"slug\": \"nama-lengkap\", \"name\": \"Nama Lengkap\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 08:49:32', '2019-08-26 08:49:32', 1),
(58, 15, 'CustomField', 'Create', '{\"updated\": \"2019-08-26 08:49:32.899654\", \"description\": \"\", \"created\": \"2019-08-26 08:49:32.899632\", \"default\": \"\", \"required\": \"True\", \"value\": \"None\", \"id\": \"15\", \"field\": \"number\", \"options\": \"\", \"group_id\": \"3\", \"slug\": \"badge\", \"name\": \"Badge\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 08:49:32', '2019-08-26 08:49:32', 1),
(59, 16, 'CustomField', 'Create', '{\"updated\": \"2019-08-26 08:49:32.917599\", \"description\": \"\", \"created\": \"2019-08-26 08:49:32.917577\", \"default\": \"\", \"required\": \"True\", \"value\": \"None\", \"id\": \"16\", \"field\": \"text\", \"options\": \"\", \"group_id\": \"3\", \"slug\": \"jabatan\", \"name\": \"Jabatan\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 08:49:32', '2019-08-26 08:49:32', 1),
(60, 1, 'PersonInvolved', 'Create', '{\"updated\": \"2019-08-26 09:08:23.540726\", \"created\": \"2019-08-26 09:08:23.540718\", \"note\": \"None\", \"phone\": \"6281234567890\", \"fullname\": \"Robot\", \"id\": \"1\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 09:08:23', '2019-08-26 09:08:23', 1),
(61, 2, 'PersonInvolved', 'Create', '{\"updated\": \"2019-08-26 09:08:23.570465\", \"created\": \"2019-08-26 09:08:23.570459\", \"note\": \"None\", \"phone\": \"62812098765432\", \"fullname\": \"Robi\", \"id\": \"2\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 09:08:23', '2019-08-26 09:08:23', 1),
(62, 7, 'Media', 'Create', '{\"mimetype\": \"application/pdf\", \"updated\": \"2019-08-26 09:13:33.278726\", \"created\": \"2019-08-26 09:13:33.278718\", \"isorphan\": \"False\", \"filename\": \"19ccd538-14a5-4130-8458-0ed99ca0f84e.pdf\", \"ismain\": \"False\", \"lookup\": \"None\", \"original_filename\": \"25883897.pdf\", \"expired\": \"None\", \"id\": \"7\", \"document_id\": \"1\", \"size\": \"2481972\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 09:13:33', '2019-08-26 09:13:33', 1);
INSERT INTO `doctrin_histories` (`id`, `module_id`, `module`, `action`, `content`, `remote_address`, `user_agent`, `created`, `updated`, `user_id`) VALUES
(63, 1, 'StatusTracking', 'Create', '{\"updated\": \"2019-08-26 11:31:57.883060\", \"created\": \"2019-08-26 11:31:57.883046\", \"notified\": \"False\", \"status_id\": \"10\", \"author_id\": \"1\", \"id\": \"1\", \"document_id\": \"1\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 11:31:57', '2019-08-26 11:31:57', 1),
(64, 4, 'CustomFieldGroup', 'Create', '{\"updated\": \"2019-08-26 13:32:37.513948\", \"description\": \"None\", \"created\": \"2019-08-26 13:32:37.513941\", \"id\": \"4\", \"organization_id\": \"1\", \"slug\": \"kalender\", \"name\": \"Kalender\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 13:32:37', '2019-08-26 13:32:37', 1),
(65, 17, 'CustomField', 'Create', '{\"updated\": \"2019-08-26 13:32:37.548610\", \"description\": \"\", \"created\": \"2019-08-26 13:32:37.548602\", \"default\": \"\", \"required\": \"True\", \"value\": \"None\", \"id\": \"17\", \"field\": \"select\", \"options\": \"Makan\\nMinum\\nTidur\\nMain\\nIbadah\", \"group_id\": \"4\", \"slug\": \"pilih-salah-satu\", \"name\": \"Pilih Salah Satu\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 13:32:37', '2019-08-26 13:32:37', 1),
(66, 5, 'CustomFieldGroup', 'Create', '{\"updated\": \"2019-08-26 13:33:46.114263\", \"description\": \"None\", \"created\": \"2019-08-26 13:33:46.114250\", \"id\": \"5\", \"organization_id\": \"1\", \"slug\": \"tanggal-dan-waktu\", \"name\": \"Tanggal dan Waktu\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 13:33:46', '2019-08-26 13:33:46', 1),
(67, 18, 'CustomField', 'Create', '{\"updated\": \"2019-08-26 13:33:46.130155\", \"description\": \"\", \"created\": \"2019-08-26 13:33:46.130147\", \"default\": \"\", \"required\": \"True\", \"value\": \"None\", \"id\": \"18\", \"field\": \"datepicker\", \"options\": \"\", \"group_id\": \"5\", \"slug\": \"tanggal-kegiatan\", \"name\": \"Tanggal Kegiatan\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 13:33:46', '2019-08-26 13:33:46', 1),
(68, 19, 'CustomField', 'Create', '{\"updated\": \"2019-08-26 13:33:46.143462\", \"description\": \"\", \"created\": \"2019-08-26 13:33:46.143456\", \"default\": \"\", \"required\": \"True\", \"value\": \"None\", \"id\": \"19\", \"field\": \"timepicker\", \"options\": \"\", \"group_id\": \"5\", \"slug\": \"waktu-kegiatan\", \"name\": \"Waktu Kegiatan\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 13:33:46', '2019-08-26 13:33:46', 1),
(69, 20, 'CustomField', 'Create', '{\"updated\": \"2019-08-26 13:33:46.153509\", \"description\": \"\", \"created\": \"2019-08-26 13:33:46.153504\", \"default\": \"\", \"required\": \"True\", \"value\": \"None\", \"id\": \"20\", \"field\": \"datetimepicker\", \"options\": \"\", \"group_id\": \"5\", \"slug\": \"tanggal-waktu\", \"name\": \"Tanggal & Waktu\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 13:33:46', '2019-08-26 13:33:46', 1),
(70, 6, 'CustomFieldGroup', 'Create', '{\"updated\": \"2019-08-26 13:35:54.934561\", \"description\": \"None\", \"created\": \"2019-08-26 13:35:54.934555\", \"id\": \"6\", \"organization_id\": \"1\", \"slug\": \"pilih-salah-satu\", \"name\": \"Pilih Salah Satu\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 13:35:54', '2019-08-26 13:35:54', 1),
(71, 21, 'CustomField', 'Create', '{\"updated\": \"2019-08-26 13:35:54.974256\", \"description\": \"Jenis mobil favoritmu\", \"created\": \"2019-08-26 13:35:54.974250\", \"default\": \"\", \"required\": \"True\", \"value\": \"None\", \"id\": \"21\", \"field\": \"select\", \"options\": \"Honda\\nSuzuki\\nToyota\\nMitsubishi\", \"group_id\": \"6\", \"slug\": \"jenis-mobil\", \"name\": \"Jenis Mobil\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 13:35:54', '2019-08-26 13:35:54', 1),
(72, 17, 'CustomField', 'Delete', '{\"updated\": \"2019-08-26 13:32:37\", \"description\": \"\", \"created\": \"2019-08-26 13:32:37\", \"default\": \"\", \"required\": \"True\", \"value\": \"None\", \"id\": \"17\", \"field\": \"select\", \"options\": \"Makan\\nMinum\\nTidur\\nMain\\nIbadah\", \"group_id\": \"4\", \"slug\": \"pilih-salah-satu\", \"name\": \"Pilih Salah Satu\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 13:46:26', '2019-08-26 13:46:26', 1),
(73, 22, 'CustomField', 'Create', '{\"updated\": \"2019-08-26 13:46:26.104785\", \"description\": \"Pilih Tanggal\", \"created\": \"2019-08-26 13:46:26.104770\", \"default\": \"\", \"required\": \"True\", \"value\": \"None\", \"id\": \"22\", \"field\": \"datepicker\", \"options\": \"\", \"group_id\": \"4\", \"slug\": \"tanggal-lahir\", \"name\": \"Tanggal Lahir\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 13:46:26', '2019-08-26 13:46:26', 1),
(74, 21, 'CustomField', 'Delete', '{\"updated\": \"2019-08-26 13:35:54\", \"description\": \"Jenis mobil favoritmu\", \"created\": \"2019-08-26 13:35:54\", \"default\": \"\", \"required\": \"True\", \"value\": \"None\", \"id\": \"21\", \"field\": \"select\", \"options\": \"Honda\\nSuzuki\\nToyota\\nMitsubishi\", \"group_id\": \"6\", \"slug\": \"jenis-mobil\", \"name\": \"Jenis Mobil\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 13:56:23', '2019-08-26 13:56:23', 1),
(75, 23, 'CustomField', 'Create', '{\"updated\": \"2019-08-26 13:56:23.105859\", \"description\": \"Jenis mobil favoritmu\", \"created\": \"2019-08-26 13:56:23.105853\", \"default\": \"\", \"required\": \"True\", \"value\": \"None\", \"id\": \"23\", \"field\": \"select\", \"options\": \"Honda\\nSuzuki\\nToyota\\nMitsubishi\", \"group_id\": \"6\", \"slug\": \"jenis-mobil\", \"name\": \"Jenis Mobil\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 13:56:23', '2019-08-26 13:56:23', 1),
(76, 24, 'CustomField', 'Create', '{\"updated\": \"2019-08-26 13:56:23.130058\", \"description\": \"\", \"created\": \"2019-08-26 13:56:23.130052\", \"default\": \"\", \"required\": \"True\", \"value\": \"None\", \"id\": \"24\", \"field\": \"radio\", \"options\": \"Iya\\nTidak\", \"group_id\": \"6\", \"slug\": \"yes-or-no\", \"name\": \"Yes or No\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 13:56:23', '2019-08-26 13:56:23', 1),
(77, 8, 'Media', 'Create', '{\"mimetype\": \"application/pdf\", \"updated\": \"2019-08-26 14:18:01.856589\", \"created\": \"2019-08-26 14:18:01.856574\", \"isorphan\": \"1\", \"filename\": \"8dcc9312-dc49-4c09-ac18-361c9d5c23c0.pdf\", \"ismain\": \"0\", \"lookup\": \"None\", \"original_filename\": \"Z69671CTGVEG49XK.pdf\", \"expired\": \"1566832681.0\", \"id\": \"8\", \"document_id\": \"None\", \"size\": \"55220\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 14:18:01', '2019-08-26 14:18:01', 1),
(78, 2, 'Document', 'Create', '{\"updated\": \"2019-08-26 14:18:09.758742\", \"created\": \"2019-08-26 14:18:09.758730\", \"document_number\": \"SPD1298000\", \"title\": \"Surat Perjalanan Dinas\", \"tracking_number\": \"PK1O60K0NSOGOEO5\", \"remarks\": \"Palembang - Surabaya 5 Hari kerja\", \"author_id\": \"1\", \"id\": \"2\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 14:18:09', '2019-08-26 14:18:09', 1),
(79, 2, 'StatusTracking', 'Create', '{\"updated\": \"2019-08-26 14:18:09.993366\", \"created\": \"2019-08-26 14:18:09.993360\", \"notified\": \"False\", \"status_id\": \"10\", \"author_id\": \"1\", \"id\": \"2\", \"document_id\": \"2\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 14:18:09', '2019-08-26 14:18:09', 1),
(80, 9, 'Media', 'Create', '{\"mimetype\": \"application/pdf\", \"updated\": \"2019-08-26 14:21:29.685833\", \"created\": \"2019-08-26 14:21:29.685826\", \"isorphan\": \"False\", \"filename\": \"a950e200-837e-4e0c-857e-05b4b81dce8a.pdf\", \"ismain\": \"False\", \"lookup\": \"None\", \"original_filename\": \"Z69671CTGVEG49XK.pdf\", \"expired\": \"None\", \"id\": \"9\", \"document_id\": \"2\", \"size\": \"55220\"}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 14:21:29', '2019-08-26 14:21:29', 1),
(81, 1, 'User', 'Logout', '{}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 19:12:52', '2019-08-26 19:12:52', 1),
(82, 1, 'User', 'Login', '{}', '127.0.0.1', '{\"device\": {\"brand\": null, \"model\": null, \"family\": \"Other\"}, \"os\": {\"major\": \"10\", \"patch_minor\": null, \"minor\": null, \"family\": \"Windows\", \"patch\": null}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\"}', '2019-08-26 19:14:46', '2019-08-26 19:14:46', 1),
(83, 1, 'User', 'Login', '{}', '127.0.0.1', '{\"device\": {\"brand\": \"Generic_Android\", \"model\": \"Pixel 2 XL\", \"family\": \"Pixel 2 XL\"}, \"os\": {\"major\": \"8\", \"patch_minor\": null, \"minor\": \"0\", \"family\": \"Android\", \"patch\": \"0\"}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome Mobile\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Linux; Android 8.0.0; Pixel 2 XL Build/OPD1.170816.004) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Mobile Safari/537.36\"}', '2019-08-27 03:36:31', '2019-08-27 03:36:31', 1),
(84, 1, 'User', 'Logout', '{}', '127.0.0.1', '{\"device\": {\"brand\": \"Generic_Android\", \"model\": \"Pixel 2 XL\", \"family\": \"Pixel 2 XL\"}, \"os\": {\"major\": \"8\", \"patch_minor\": null, \"minor\": \"0\", \"family\": \"Android\", \"patch\": \"0\"}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome Mobile\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Linux; Android 8.0.0; Pixel 2 XL Build/OPD1.170816.004) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Mobile Safari/537.36\"}', '2019-08-27 03:58:17', '2019-08-27 03:58:17', 1),
(85, 1, 'User', 'Login', '{}', '127.0.0.1', '{\"device\": {\"brand\": \"Generic_Android\", \"model\": \"Pixel 2 XL\", \"family\": \"Pixel 2 XL\"}, \"os\": {\"major\": \"8\", \"patch_minor\": null, \"minor\": \"0\", \"family\": \"Android\", \"patch\": \"0\"}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome Mobile\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Linux; Android 8.0.0; Pixel 2 XL Build/OPD1.170816.004) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Mobile Safari/537.36\"}', '2019-08-27 04:00:10', '2019-08-27 04:00:10', 1),
(86, 1, 'User', 'Edit', '{\"badge\": {\"to\": \"[u\'92000000\']\", \"from\": \"[None]\"}}', '127.0.0.1', '{\"device\": {\"brand\": \"Generic_Android\", \"model\": \"Pixel 2 XL\", \"family\": \"Pixel 2 XL\"}, \"os\": {\"major\": \"8\", \"patch_minor\": null, \"minor\": \"0\", \"family\": \"Android\", \"patch\": \"0\"}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome Mobile\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Linux; Android 8.0.0; Pixel 2 XL Build/OPD1.170816.004) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Mobile Safari/537.36\"}', '2019-08-27 04:24:15', '2019-08-27 04:24:15', 1),
(87, 1, 'User', 'Login', '{}', '127.0.0.1', '{\"device\": {\"brand\": \"Generic_Android\", \"model\": \"Pixel 2 XL\", \"family\": \"Pixel 2 XL\"}, \"os\": {\"major\": \"8\", \"patch_minor\": null, \"minor\": \"0\", \"family\": \"Android\", \"patch\": \"0\"}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome Mobile\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Linux; Android 8.0.0; Pixel 2 XL Build/OPD1.170816.004) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Mobile Safari/537.36\"}', '2019-08-27 04:25:21', '2019-08-27 04:25:21', 1),
(88, 1, 'User', 'Edit', '{\"badge\": {\"to\": \"[u\'92500000\']\", \"from\": \"[u\'92000000\']\"}}', '127.0.0.1', '{\"device\": {\"brand\": \"Generic_Android\", \"model\": \"Pixel 2 XL\", \"family\": \"Pixel 2 XL\"}, \"os\": {\"major\": \"8\", \"patch_minor\": null, \"minor\": \"0\", \"family\": \"Android\", \"patch\": \"0\"}, \"user_agent\": {\"major\": \"76\", \"minor\": \"0\", \"family\": \"Chrome Mobile\", \"patch\": \"3809\"}, \"string\": \"Mozilla/5.0 (Linux; Android 8.0.0; Pixel 2 XL Build/OPD1.170816.004) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Mobile Safari/537.36\"}', '2019-08-27 04:26:25', '2019-08-27 04:26:25', 1);

-- --------------------------------------------------------

--
-- Table structure for table `doctrin_media`
--

CREATE TABLE `doctrin_media` (
  `id` int(11) NOT NULL,
  `lookup` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `original_filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mimetype` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ismain` tinyint(1) DEFAULT NULL,
  `isorphan` tinyint(1) DEFAULT NULL,
  `expired` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `document_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `doctrin_media`
--

INSERT INTO `doctrin_media` (`id`, `lookup`, `original_filename`, `filename`, `size`, `mimetype`, `ismain`, `isorphan`, `expired`, `created`, `updated`, `document_id`) VALUES
(6, NULL, 'What_is_hate_speech.docx', '392a9f7f-25fd-4e02-94c6-1967cf14ea00.docx', '13593', 'application/vnd.openxmlformats-officedocument.word', 0, 0, NULL, '2019-08-26 08:01:50', '2019-08-26 08:01:50', 1),
(7, NULL, '25883897.pdf', '19ccd538-14a5-4130-8458-0ed99ca0f84e.pdf', '2481972', 'application/pdf', 0, 0, NULL, '2019-08-26 09:13:33', '2019-08-26 09:13:33', 1),
(8, NULL, 'Z69671CTGVEG49XK.pdf', '8dcc9312-dc49-4c09-ac18-361c9d5c23c0.pdf', '55220', 'application/pdf', 0, 1, 1566832681, '2019-08-26 14:18:01', '2019-08-26 14:18:01', NULL),
(9, NULL, 'Z69671CTGVEG49XK.pdf', 'a950e200-837e-4e0c-857e-05b4b81dce8a.pdf', '55220', 'application/pdf', 0, 0, NULL, '2019-08-26 14:21:29', '2019-08-26 14:21:29', 2);

-- --------------------------------------------------------

--
-- Table structure for table `doctrin_organization`
--

CREATE TABLE `doctrin_organization` (
  `id` int(11) NOT NULL,
  `code` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cost_center` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `doctrin_organization`
--

INSERT INTO `doctrin_organization` (`id`, `code`, `name`, `cost_center`, `description`, `created`, `updated`) VALUES
(1, 'A0000000', 'DIREKTORAT UTAMA', 'F001000000', 'Direktorat Utama', '2019-08-18 00:00:00', '2019-08-18 00:00:00'),
(2, 'AB000000', 'DIVISI SATUAN PENGAWAS INTERN', 'F001100000', 'Satuan Pengawasan Intern', '2019-08-18 00:00:00', '2019-08-18 00:00:00'),
(3, 'AB100000', 'DEPARTEMEN PENGAWASAN OPERASIONAL', 'F001120000', 'Departemen Pengawasan Operasional', '2019-08-18 00:00:00', '2019-08-18 00:00:00'),
(4, 'AB101000', 'KEL. AUDITOR WAS OPERASIONAL', 'F001120000', 'Departemen Pengawasan Operasional', '2019-08-18 00:00:00', '2019-08-18 00:00:00'),
(5, 'AB200000', 'DEPARTEMEN PENGAWASAN KEUANGAN', 'F001110000', 'Departemen Pengawasan Keuangan', '2019-08-18 00:00:00', '2019-08-18 00:00:00'),
(6, 'AB201000', 'KEL.AUDITOR WAS KEUANGAN', 'F001110000', 'Departemen Pengawasan Keuangan', '2019-08-19 01:52:58', '2019-08-19 01:52:58'),
(7, 'AC000000', 'DIVISI SEKRETARIAT PERUSAHAAN', 'F001200000', 'Sekretaris Perusahaan', '2019-08-19 01:54:25', '2019-08-19 02:01:47'),
(8, 'AB23000000', 'Departemen Makanan &amp; Minuman', 'F002220000', 'Makanan &amp; Minuman', '2019-08-24 07:57:28', '2019-08-24 08:04:42');

-- --------------------------------------------------------

--
-- Table structure for table `doctrin_permissions`
--

CREATE TABLE `doctrin_permissions` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `doctrin_permissions`
--

INSERT INTO `doctrin_permissions` (`id`, `name`, `slug`, `description`, `created`, `updated`) VALUES
(1, 'Dashboard', 'dashboard', 'Dashboard access permission', '2019-08-18 00:00:00', '2019-08-18 00:00:00'),
(2, 'Mobile', 'mobile', 'Application access permission', '2019-08-18 00:00:00', '2019-08-18 00:00:00'),
(3, 'Permission', 'permission', 'Permission module access', '2019-08-18 00:00:00', '2019-08-18 00:00:00'),
(4, 'Role', 'role', 'Role module access', '2019-08-18 00:00:00', '2019-08-18 00:00:00'),
(5, 'User', 'user', 'User module access', '2019-08-18 00:00:00', '2019-08-18 00:00:00'),
(6, 'Organization', 'organization', 'Organization module access', '2019-08-18 00:00:00', '2019-08-18 00:00:00'),
(7, 'Document', 'document', 'Document module permission', '2019-08-19 04:15:50', '2019-08-19 04:15:50'),
(8, 'Custom Field', 'customfield', 'Custom Field module permission', '2019-08-21 02:56:59', '2019-08-21 02:56:59'),
(9, 'Status', 'status', 'Status module permission', '2019-08-22 08:04:20', '2019-08-22 08:04:20'),
(10, 'History', 'history', 'History module permission', '2019-08-22 17:47:54', '2019-08-22 17:47:54');

-- --------------------------------------------------------

--
-- Table structure for table `doctrin_person_involved`
--

CREATE TABLE `doctrin_person_involved` (
  `id` int(11) NOT NULL,
  `fullname` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `doctrin_person_involved`
--

INSERT INTO `doctrin_person_involved` (`id`, `fullname`, `phone`, `note`, `created`, `updated`) VALUES
(1, 'Robot', '6281234567890', NULL, '2019-08-26 09:08:23', '2019-08-26 09:08:23'),
(2, 'Robi', '62812098765432', NULL, '2019-08-26 09:08:23', '2019-08-26 09:08:23');

-- --------------------------------------------------------

--
-- Table structure for table `doctrin_roles`
--

CREATE TABLE `doctrin_roles` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `doctrin_roles`
--

INSERT INTO `doctrin_roles` (`id`, `name`, `slug`, `description`, `permissions`, `created`, `updated`) VALUES
(1, 'Orphan', 'orphan', 'Orphan user role', '{\"status\": {\"deleteother\": 0, \"menu\": 0, \"create\": 0, \"editown\": 0, \"viewown\": 0, \"viewother\": 0, \"deleteown\": 0, \"editother\": 0, \"viewall\": 0}, \"permission\": {\"deleteown\": 0, \"menu\": 0, \"create\": 0, \"editown\": 0, \"viewown\": 0, \"viewother\": 0, \"deleteother\": 0, \"editother\": 0, \"viewall\": 0}, \"mobile\": {\"deleteown\": 0, \"menu\": 0, \"create\": 0, \"editown\": 0, \"viewown\": 0, \"viewother\": 0, \"deleteother\": 0, \"editother\": 0, \"viewall\": 0}, \"media\": {\"deleteown\": 0, \"menu\": 0, \"create\": 0, \"editown\": 0, \"viewown\": 0, \"viewother\": 0, \"deleteother\": 0, \"editother\": 0, \"viewall\": 0}, \"history\": {\"deleteown\": 0, \"menu\": 0, \"create\": 0, \"editown\": 0, \"viewown\": 0, \"viewother\": 0, \"deleteother\": 0, \"editother\": 0, \"viewall\": 0}, \"role\": {\"deleteown\": 0, \"menu\": 0, \"create\": 0, \"editown\": 0, \"viewown\": 0, \"viewother\": 0, \"deleteother\": 0, \"editother\": 0, \"viewall\": 0}, \"user\": {\"deleteown\": 0, \"menu\": 0, \"create\": 0, \"editown\": 0, \"viewown\": 0, \"viewother\": 0, \"deleteother\": 0, \"editother\": 0, \"viewall\": 0}, \"organization\": {\"deleteown\": 0, \"menu\": 0, \"create\": 0, \"editown\": 0, \"viewown\": 0, \"viewother\": 0, \"deleteother\": 0, \"editother\": 0, \"viewall\": 0}, \"document\": {\"deleteother\": 0, \"menu\": 0, \"create\": 0, \"editown\": 0, \"viewown\": 0, \"viewother\": 0, \"deleteown\": 0, \"editother\": 0, \"viewall\": 0}, \"customfield\": {\"deleteown\": 0, \"menu\": 0, \"create\": 0, \"editown\": 0, \"viewown\": 0, \"viewother\": 0, \"deleteother\": 0, \"editother\": 0, \"viewall\": 0}, \"dashboard\": {\"deleteown\": 0, \"menu\": 0, \"create\": 0, \"editown\": 0, \"viewown\": 0, \"viewother\": 0, \"deleteother\": 0, \"editother\": 0, \"viewall\": 0}}', '2019-08-18 00:00:00', '2019-08-23 15:43:31'),
(2, 'Root', 'root', 'Root user role', '{\"status\": {\"deleteother\": 1, \"menu\": 1, \"create\": 1, \"editown\": 1, \"viewown\": 1, \"viewother\": 1, \"deleteown\": 1, \"editother\": 1, \"viewall\": 1}, \"permission\": {\"deleteown\": 1, \"menu\": 1, \"create\": 1, \"editown\": 1, \"viewown\": 1, \"viewother\": 1, \"deleteother\": 1, \"editother\": 1, \"viewall\": 1}, \"mobile\": {\"deleteown\": 1, \"menu\": 1, \"create\": 1, \"editown\": 1, \"viewown\": 1, \"viewother\": 1, \"deleteother\": 1, \"editother\": 1, \"viewall\": 1}, \"media\": {\"deleteown\": 1, \"menu\": 1, \"create\": 1, \"editown\": 1, \"viewown\": 1, \"viewother\": 1, \"deleteother\": 1, \"editother\": 1, \"viewall\": 1}, \"history\": {\"deleteown\": 1, \"menu\": 1, \"create\": 1, \"editown\": 1, \"viewown\": 1, \"viewother\": 1, \"deleteother\": 1, \"editother\": 1, \"viewall\": 1}, \"role\": {\"deleteown\": 1, \"menu\": 1, \"create\": 1, \"editown\": 1, \"viewown\": 1, \"viewother\": 1, \"deleteother\": 1, \"editother\": 1, \"viewall\": 1}, \"user\": {\"deleteown\": 1, \"menu\": 1, \"create\": 1, \"editown\": 1, \"viewown\": 1, \"viewother\": 1, \"deleteother\": 1, \"editother\": 1, \"viewall\": 1}, \"organization\": {\"deleteown\": 1, \"menu\": 1, \"create\": 1, \"editown\": 1, \"viewown\": 1, \"viewother\": 1, \"deleteother\": 1, \"editother\": 1, \"viewall\": 1}, \"document\": {\"deleteother\": 1, \"menu\": 1, \"create\": 1, \"editown\": 1, \"viewown\": 1, \"viewother\": 1, \"deleteown\": 1, \"editother\": 1, \"viewall\": 1}, \"customfield\": {\"deleteown\": 1, \"menu\": 1, \"create\": 1, \"editown\": 1, \"viewown\": 1, \"viewother\": 1, \"deleteother\": 1, \"editother\": 1, \"viewall\": 1}, \"dashboard\": {\"deleteown\": 1, \"menu\": 1, \"create\": 1, \"editown\": 1, \"viewown\": 1, \"viewother\": 1, \"deleteother\": 1, \"editother\": 1, \"viewall\": 1}}', '2019-08-18 00:00:00', '2019-08-23 15:43:31'),
(3, 'Employee', 'employee', 'Employee user role', '{\"status\": {\"deleteother\": 0, \"menu\": 0, \"create\": 0, \"editown\": 0, \"viewown\": 0, \"viewother\": 0, \"deleteown\": 0, \"editother\": 0, \"viewall\": 0}, \"permission\": {\"deleteother\": 0, \"menu\": 1, \"create\": 1, \"editown\": 1, \"viewown\": 1, \"viewother\": 0, \"deleteown\": 1, \"editother\": 0, \"viewall\": 1}, \"mobile\": {\"deleteother\": 0, \"menu\": 1, \"create\": 1, \"editown\": 1, \"viewown\": 1, \"viewother\": 0, \"deleteown\": 1, \"editother\": 0, \"viewall\": 1}, \"media\": {\"deleteother\": 0, \"menu\": 1, \"create\": 1, \"editown\": 1, \"viewown\": 1, \"viewother\": 0, \"deleteown\": 1, \"editother\": 0, \"viewall\": 1}, \"history\": {\"deleteown\": 0, \"menu\": 0, \"create\": 0, \"editown\": 0, \"viewown\": 0, \"viewother\": 0, \"deleteother\": 0, \"editother\": 0, \"viewall\": 0}, \"role\": {\"deleteother\": 0, \"menu\": 1, \"create\": 1, \"editown\": 1, \"viewown\": 1, \"viewother\": 0, \"deleteown\": 1, \"editother\": 0, \"viewall\": 1}, \"user\": {\"deleteother\": 0, \"menu\": 1, \"create\": 1, \"editown\": 1, \"viewown\": 1, \"viewother\": 0, \"deleteown\": 1, \"editother\": 0, \"viewall\": 1}, \"organization\": {\"deleteother\": 0, \"menu\": 1, \"create\": 1, \"editown\": 1, \"viewown\": 1, \"viewother\": 0, \"deleteown\": 1, \"editother\": 0, \"viewall\": 1}, \"document\": {\"deleteother\": 0, \"menu\": 0, \"create\": 0, \"editown\": 0, \"viewown\": 0, \"viewother\": 0, \"deleteown\": 0, \"editother\": 0, \"viewall\": 0}, \"customfield\": {\"deleteown\": 0, \"menu\": 0, \"create\": 0, \"editown\": 0, \"viewown\": 0, \"viewother\": 0, \"deleteother\": 0, \"editother\": 0, \"viewall\": 0}, \"dashboard\": {\"deleteother\": 0, \"menu\": 1, \"create\": 1, \"editown\": 1, \"viewown\": 1, \"viewother\": 0, \"deleteown\": 1, \"editother\": 0, \"viewall\": 1}}', '2019-08-18 00:00:00', '2019-08-23 15:43:31'),
(4, 'Public', 'public', 'Public user role', '{\"status\": {\"deleteother\": 0, \"menu\": 0, \"create\": 0, \"editown\": 0, \"viewown\": 0, \"viewother\": 0, \"deleteown\": 0, \"editother\": 0, \"viewall\": 0}, \"permission\": {\"deleteother\": 0, \"menu\": 0, \"create\": 0, \"editown\": 0, \"viewown\": 0, \"viewother\": 0, \"deleteown\": 0, \"editother\": 0, \"viewall\": 0}, \"mobile\": {\"deleteother\": 0, \"menu\": 0, \"create\": 0, \"editown\": 0, \"viewown\": 0, \"viewother\": 0, \"deleteown\": 0, \"editother\": 0, \"viewall\": 0}, \"history\": {\"deleteother\": 0, \"menu\": 0, \"create\": 0, \"editown\": 0, \"viewown\": 0, \"viewother\": 0, \"deleteown\": 0, \"editother\": 0, \"viewall\": 0}, \"role\": {\"deleteother\": 0, \"menu\": 0, \"create\": 0, \"editown\": 0, \"viewown\": 0, \"viewother\": 0, \"deleteown\": 0, \"editother\": 0, \"viewall\": 0}, \"user\": {\"deleteother\": 0, \"menu\": 0, \"create\": 0, \"editown\": 0, \"viewown\": 0, \"viewother\": 0, \"deleteown\": 0, \"editother\": 0, \"viewall\": 0}, \"organization\": {\"deleteother\": 0, \"menu\": 0, \"create\": 0, \"editown\": 0, \"viewown\": 0, \"viewother\": 0, \"deleteown\": 0, \"editother\": 0, \"viewall\": 0}, \"document\": {\"deleteother\": 0, \"menu\": 0, \"create\": 0, \"editown\": 0, \"viewown\": 0, \"viewother\": 0, \"deleteown\": 0, \"editother\": 0, \"viewall\": 0}, \"customfield\": {\"deleteother\": 0, \"menu\": 0, \"create\": 0, \"editown\": 0, \"viewown\": 0, \"viewother\": 0, \"deleteown\": 0, \"editother\": 0, \"viewall\": 0}, \"dashboard\": {\"deleteother\": 0, \"menu\": 0, \"create\": 0, \"editown\": 0, \"viewown\": 0, \"viewother\": 0, \"deleteown\": 0, \"editother\": 0, \"viewall\": 0}}', '2019-08-18 00:00:00', '2019-08-23 17:15:14');

-- --------------------------------------------------------

--
-- Table structure for table `doctrin_status`
--

CREATE TABLE `doctrin_status` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ispublic` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `organization_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `doctrin_status`
--

INSERT INTO `doctrin_status` (`id`, `name`, `slug`, `description`, `ispublic`, `created`, `updated`, `organization_id`) VALUES
(9, 'Diterima Di Sekper', 'diterima-di-sekper', NULL, 0, '2019-08-24 19:18:36', '2019-08-24 19:18:36', 7),
(10, 'Diterima Direktur Utama', 'diterima-direktur-utama', NULL, 1, '2019-08-26 06:49:54', '2019-08-26 06:49:54', 1);

-- --------------------------------------------------------

--
-- Table structure for table `doctrin_status_tracking`
--

CREATE TABLE `doctrin_status_tracking` (
  `id` int(11) NOT NULL,
  `notified` tinyint(1) DEFAULT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `doctrin_status_tracking`
--

INSERT INTO `doctrin_status_tracking` (`id`, `notified`, `note`, `status_id`, `author_id`, `document_id`, `created`, `updated`) VALUES
(1, 1, 'Lorem ipsum dolor sit amet consectus paleo javanicus brutus cuculutus', 10, 1, 1, '2019-08-26 11:31:57', '2019-08-26 11:31:57'),
(2, 0, NULL, 10, 1, 2, '2019-08-26 14:18:09', '2019-08-26 14:18:09');

-- --------------------------------------------------------

--
-- Table structure for table `doctrin_users`
--

CREATE TABLE `doctrin_users` (
  `id` int(11) NOT NULL,
  `nid` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fullname` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `badge` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `passkey` varchar(6) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `passkey_expire` int(11) DEFAULT NULL,
  `verification_code` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('unverified','verified','banned') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `organization_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `doctrin_users`
--

INSERT INTO `doctrin_users` (`id`, `nid`, `fullname`, `badge`, `phone`, `passkey`, `passkey_expire`, `verification_code`, `status`, `created`, `updated`, `role_id`, `organization_id`) VALUES
(1, NULL, 'Erwin Yusrizal', '92500000', '6281353206485', NULL, NULL, NULL, 'verified', '2019-08-18 00:00:00', '2019-08-27 04:26:25', 2, 1),
(2, NULL, 'Habib', '66500000', '6285267625030', NULL, NULL, NULL, 'unverified', '2019-08-22 13:32:31', '2019-08-22 13:39:52', 1, 7),
(3, NULL, 'Cipta', '90123567', '6281234567890', NULL, NULL, NULL, 'verified', '2019-08-24 01:35:23', '2019-08-24 02:15:00', 3, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alembic_version`
--
ALTER TABLE `alembic_version`
  ADD PRIMARY KEY (`version_num`);

--
-- Indexes for table `doctrin_documents`
--
ALTER TABLE `doctrin_documents`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ix_doctrin_documents_id` (`id`),
  ADD KEY `ix_doctrin_documents_author_id` (`author_id`);

--
-- Indexes for table `doctrin_documents_fields`
--
ALTER TABLE `doctrin_documents_fields`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ix_doctrin_documents_fields_id` (`id`),
  ADD KEY `ix_doctrin_documents_fields_field_id` (`field_id`),
  ADD KEY `ix_doctrin_documents_fields_document_id` (`document_id`);

--
-- Indexes for table `doctrin_documents_person_involves`
--
ALTER TABLE `doctrin_documents_person_involves`
  ADD PRIMARY KEY (`document_id`,`person_involve_id`),
  ADD KEY `person_involve_id` (`person_involve_id`);

--
-- Indexes for table `doctrin_fields`
--
ALTER TABLE `doctrin_fields`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ix_doctrin_fields_id` (`id`),
  ADD KEY `ix_doctrin_fields_group_id` (`group_id`),
  ADD KEY `ix_doctrin_fields_field` (`field`);

--
-- Indexes for table `doctrin_fields_groups`
--
ALTER TABLE `doctrin_fields_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ix_doctrin_fields_groups_id` (`id`),
  ADD KEY `ix_doctrin_fields_groups_organization_id` (`organization_id`);

--
-- Indexes for table `doctrin_histories`
--
ALTER TABLE `doctrin_histories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ix_doctrin_histories_id` (`id`),
  ADD KEY `ix_doctrin_histories_user_id` (`user_id`);

--
-- Indexes for table `doctrin_media`
--
ALTER TABLE `doctrin_media`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ix_doctrin_media_id` (`id`),
  ADD KEY `ix_doctrin_media_document_id` (`document_id`);

--
-- Indexes for table `doctrin_organization`
--
ALTER TABLE `doctrin_organization`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ix_doctrin_organization_id` (`id`);

--
-- Indexes for table `doctrin_permissions`
--
ALTER TABLE `doctrin_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD UNIQUE KEY `ix_doctrin_permissions_id` (`id`);

--
-- Indexes for table `doctrin_person_involved`
--
ALTER TABLE `doctrin_person_involved`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ix_doctrin_person_involved_id` (`id`);

--
-- Indexes for table `doctrin_roles`
--
ALTER TABLE `doctrin_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD UNIQUE KEY `ix_doctrin_roles_id` (`id`);

--
-- Indexes for table `doctrin_status`
--
ALTER TABLE `doctrin_status`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ix_doctrin_status_id` (`id`),
  ADD KEY `ix_doctrin_status_organization_id` (`organization_id`);

--
-- Indexes for table `doctrin_status_tracking`
--
ALTER TABLE `doctrin_status_tracking`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ix_doctrin_status_tracking_id` (`id`),
  ADD KEY `ix_doctrin_status_tracking_document_id` (`document_id`),
  ADD KEY `ix_doctrin_status_tracking_status_id` (`status_id`),
  ADD KEY `ix_doctrin_status_tracking_author_id` (`author_id`);

--
-- Indexes for table `doctrin_users`
--
ALTER TABLE `doctrin_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ix_doctrin_users_id` (`id`),
  ADD KEY `ix_doctrin_users_role_id` (`role_id`),
  ADD KEY `ix_doctrin_users_organization_id` (`organization_id`),
  ADD KEY `ix_doctrin_users_status` (`status`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `doctrin_documents`
--
ALTER TABLE `doctrin_documents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `doctrin_documents_fields`
--
ALTER TABLE `doctrin_documents_fields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `doctrin_fields`
--
ALTER TABLE `doctrin_fields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `doctrin_fields_groups`
--
ALTER TABLE `doctrin_fields_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `doctrin_histories`
--
ALTER TABLE `doctrin_histories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
--
-- AUTO_INCREMENT for table `doctrin_media`
--
ALTER TABLE `doctrin_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `doctrin_organization`
--
ALTER TABLE `doctrin_organization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `doctrin_permissions`
--
ALTER TABLE `doctrin_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `doctrin_person_involved`
--
ALTER TABLE `doctrin_person_involved`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `doctrin_roles`
--
ALTER TABLE `doctrin_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `doctrin_status`
--
ALTER TABLE `doctrin_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `doctrin_status_tracking`
--
ALTER TABLE `doctrin_status_tracking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `doctrin_users`
--
ALTER TABLE `doctrin_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `doctrin_documents`
--
ALTER TABLE `doctrin_documents`
  ADD CONSTRAINT `doctrin_documents_ibfk_1` FOREIGN KEY (`author_id`) REFERENCES `doctrin_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `doctrin_documents_fields`
--
ALTER TABLE `doctrin_documents_fields`
  ADD CONSTRAINT `doctrin_documents_fields_ibfk_1` FOREIGN KEY (`document_id`) REFERENCES `doctrin_documents` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `doctrin_documents_fields_ibfk_2` FOREIGN KEY (`field_id`) REFERENCES `doctrin_fields` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `doctrin_documents_person_involves`
--
ALTER TABLE `doctrin_documents_person_involves`
  ADD CONSTRAINT `doctrin_documents_person_involves_ibfk_1` FOREIGN KEY (`document_id`) REFERENCES `doctrin_documents` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `doctrin_documents_person_involves_ibfk_2` FOREIGN KEY (`person_involve_id`) REFERENCES `doctrin_person_involved` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `doctrin_fields`
--
ALTER TABLE `doctrin_fields`
  ADD CONSTRAINT `doctrin_fields_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `doctrin_fields_groups` (`id`);

--
-- Constraints for table `doctrin_fields_groups`
--
ALTER TABLE `doctrin_fields_groups`
  ADD CONSTRAINT `doctrin_fields_groups_ibfk_1` FOREIGN KEY (`organization_id`) REFERENCES `doctrin_organization` (`id`);

--
-- Constraints for table `doctrin_histories`
--
ALTER TABLE `doctrin_histories`
  ADD CONSTRAINT `doctrin_histories_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `doctrin_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `doctrin_media`
--
ALTER TABLE `doctrin_media`
  ADD CONSTRAINT `doctrin_media_ibfk_1` FOREIGN KEY (`document_id`) REFERENCES `doctrin_documents` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `doctrin_status`
--
ALTER TABLE `doctrin_status`
  ADD CONSTRAINT `doctrin_status_ibfk_1` FOREIGN KEY (`organization_id`) REFERENCES `doctrin_organization` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `doctrin_status_tracking`
--
ALTER TABLE `doctrin_status_tracking`
  ADD CONSTRAINT `doctrin_status_tracking_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `doctrin_status` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `doctrin_status_tracking_ibfk_2` FOREIGN KEY (`author_id`) REFERENCES `doctrin_users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `doctrin_status_tracking_ibfk_3` FOREIGN KEY (`document_id`) REFERENCES `doctrin_documents` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `doctrin_users`
--
ALTER TABLE `doctrin_users`
  ADD CONSTRAINT `doctrin_users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `doctrin_roles` (`id`),
  ADD CONSTRAINT `doctrin_users_ibfk_2` FOREIGN KEY (`organization_id`) REFERENCES `doctrin_organization` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
